import unittest
from ComPyLL.flattener import Flattener
from ComPyLL.variable_generator import generate_variable
from ComPyLL import ast
import ComPyLL.compyll_parser as parser

class StmtFlattenerTest(unittest.TestCase):

    @classmethod
    def set_up_class(cls):
        cls.flattener = Flattener()
        cls.longMessage = True

    setUpClass = set_up_class  # Because even at 1 am I respect PEP 8!

    def set_up(self):
        generate_variable.counter = 0

    setUp = set_up  # Because even at 1 am I respect PEP 8!

    def test_empty_stmt(self):
        actual = self.flattener.flatten(parser.parse("")).node
        print "actual"
        print actual
        expected = ast.Stmt([])
        self.assertItemsEqual(actual.nodes, expected.nodes,
                              "".join(["\nExpected: ", str(expected), "\nGot: ", str(actual)]))

if __name__ == '__main__':
    unittest.main()
