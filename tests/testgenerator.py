import random

statements = ['x = ', 'print ', '']

operators = ['+', '-', '*', '/', '//', 
			'%', '>>', '<<', 
			'&', '|', '^']

variables = ['1', '2', '3', '-1', 'True', 'False', '42', '0']

def generate_expression():
	ret = random.choice(variables)
	for i in range(20):
		ret += ' '
		ret += random.choice(operators)
		ret += ' '
		ret += random.choice(variables)
	return ret


def generate_good_expression():
	good = False
	while not good:
		good = True
		exp = generate_expression()
		try:
			eval(exp)
		except:
			good = False
	return random.choice(statements) + exp
	

print generate_good_expression()