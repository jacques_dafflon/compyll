x = "hello"
y = True
z = False


print x < y
print x < z

print y < x
print z < x

print x <= y
print x <= z

print y <= x
print z <= x

print x == y
print x == z

print y == x
print z == x

print x != y
print x != z

print y != x
print z != x

print x > y
print x > z

print y > x
print z > x

print x >= y
print x >= z

print y >= x
print z >= x
