print None and 42
v2 = None and 42
print v2

print None and 0
v3 = None and 0
print v3

print 42 and None
v4 = 42 and None
print v4

print 0 and None
v5 = 0 and None
print v5

print None and None
v6 = None and None
print v6

print None or 42
v7 = None or 42
print v7

print None or 0
v8 = None or 0
print v8

print 42 or None
v9 = 42 or None
print v9

print 0 or None
v10 = 0 or None
print v10

print None or None
v11 = None or None
print v11