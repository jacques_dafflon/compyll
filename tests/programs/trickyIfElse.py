if True:
	if False:
		print 34
	else:
		print 35  # Should print

if False:
	if True:
		print 36
	else:
		print 37
elif False:
	if False:
		print 38
	elif False:
		print 39
	else:
		print 40
elif True:
	if False:
		print 41
	elif True:
		print 42  # Should print
	elif False:
		print 43
	else:
		print 44
elif False:
	print 45

if True:
	if False:
		print 46
	elif True:
		if False:
			print 47
		else:
			print 48  # Should print
			if True:
				if True:
					if True:
						if True:
							if True:
								print 49  # Should print

		if True:
			print 50  # Should print
	else:
		print 51