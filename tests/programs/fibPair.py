
def makePair(a, b):
    def get(t):
        if t:
            return a
        else:
            return b
    return get

def fibH(n):
    if n == 0:
        return makePair(0, 1)
    x = fibH(n - 1)
    return makePair(x(False), x(True) + x(False))

def fib(n):
    return fibH(n)(False)

n = 0
while n < 15:
    print fib(n)
    n += 1

