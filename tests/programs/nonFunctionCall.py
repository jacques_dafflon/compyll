
def f(x):
    def g(y):
        return y + 1
    if x > 5:
        return 0
    return g

t = f(3)
print t(5)
t1 = f(7)
print t1(3)
