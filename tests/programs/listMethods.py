def reset_l1():
    return [1,2,3, True, "twice", "once", False, "twice"]

def reset_l2():
    [1,2,3, "some string", False, 42, not True, 3*6]

l1 = reset_l1()
l2 = reset_l1()

print l1
print l2

# Concatenation
print "__add__"
print l1 + l2
print l1.__add__(l2)
l3 = l2 + l1
print l3
l4 = l2.__add__(l1)
print l4

# Append
print "append"
l1.append(1)
print l1
print l2.append(1)
print l2

l1 = reset_l1()
l2 = reset_l1()

#Extend
print "extend"
l5 = []
l5.extend(l1)
print l5
print l4.extend(l4)
print l4

# Insert
print "insert"
l3.insert(2, 42)
print l3[2]
print l4.insert(len(l4)//2, 42)
print l4.__getitem__((len(l4)-1)//2)

# Remove
print "remove"
print l3.remove(42)
print l3

print l4.remove(42)
print l4

# Pop
print "pop"
last = l3[len(l3)-1]
print l3.pop(-1)
print last
print l3[len(l3)-1]
print last != l3[len(l3)-1]
print l4.pop(3)
print l4
popped = l4.pop(-2)
print popped

# Index
print "index"
print l4
index = l4.index(True)
print index
print l3.index("twice")
l5.insert(-2*len(l5), "begin")
print l5[0] == "begin"
l5.insert(-1, "before last")
print l5.index("before last") == len(l5) - 1
l5.insert(len(l5)*5, "last")
print l5[-1] == l5.pop(-1)

# Count
print "count"
print l3.count("twice")
print l2.count(False)
print l1.count("Not in the list")

# Reverse
print "reverse"
print l5.reverse()
print l5
[].reverse()
print [].reverse()
same = [2, 2, 2]
print same
same.reverse()
print same
