

def cons(a, b):
    def get(t):
        if t:
            return a
        else:
            return b
    return get

def car(p):
    return p(True)

def cdr(p):
    return p(False)

nil = None

def singleton(x):
    return cons(x, nil)

def length(l):
    if l == nil:
        return 0
    return 1+ length(cdr(l))

def append(xs, ys):
    if xs == nil:
        return ys
    return cons(car(xs), append(cdr(xs), ys))

def reverse(xs):
    if xs == nil:
        return nil
    return append(cdr(xs), singleton(car(xs)))

def reverse_tail(xs):
    def helper(xs, ys):
        if xs == nil:
            return ys
        return helper(cdr(xs), cons(car(xs), ys))
    return helper(xs, nil)

def print_list(xs):
    def helper(xs):
        if xs != nil:
            print car(xs)
            helper(cdr(xs))
    print 8888
    helper(xs)
    print 7777

def get(xs, n):
    if n == 0:
        return car(xs)
    return get(cdr(xs), n - 1)

l = cons(1, cons(2, cons(3, cons(4, cons(5, cons(6, cons(7, nil)))))))
print_list(l)
print length(l)

l1 = reverse(l)
l2 = cdr(reverse_tail(l))

print_list(l1)
print length(l1)
print_list(l2)
print length(l2)

l3 = append(l2, l)
print_list(l3)
print length(l3)

i = 0
while i < length(l3):
    print get(l3, i)
    i += 1

