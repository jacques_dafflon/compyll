x = [4, 5, 6]
x[0] = "a"
print x[0]
x[1] = "b"
print x[1]
x[2] = "c"
print x[2]

x = [4, 5, 6]
x.__setitem__(0, "a")
print x[0]
x.__setitem__(1, "b")
print x[1]
x.__setitem__(2, "c")
print x[2]

x = [4, 5, 6]
x[-1] = "a"
print x[-1]
x[-2] = "b"
print x[-2]
x[-3] = "c"
print x[-3]

x = [4, 5, 6]
x.__setitem__(-1, "a")
print x[-1]
x.__setitem__(-2, "b")
print x[-2]
x.__setitem__(-3, "c")
print x[-3]