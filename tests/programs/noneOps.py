
def f(x):
    print x + 2

n = f(1)
print n and True
print True and n
print n or False
print False or n
print n or True
print True or n
print None == True
print 4 == None
print (lambda x : x + 1) == None
print False != None
print None != 90
print None != (lambda x : x * x)
