x = "a"
y = "b"
z = "c"

print x > y
print y > z
print x > z

print x < y
print y < z
print x < z

print x == y
print y == z
print x == z

print x <= y
print y <= z
print x <= z

print x >= y
print y >= z
print x >= z

print x != y
print y != z
print x != z
