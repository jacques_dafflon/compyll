
def curry(f):
    return lambda x : lambda y : f(x, y)

def uncurry(f):
    def h(x, y):
        t = f(x)
        return t(y)
    return h

def add(x,y):
    return x+y

t = curry(add)
f = t(3)

print f(4)

g = uncurry(curry(add))

print g(34, 1)
