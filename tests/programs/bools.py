
t = lambda x, y : x
f = lambda x, y : y

def printB(b):
    print b(True, False)

If = lambda c, t, f : c(t, f)

And = lambda x, y : If(x, y, x)

Or = lambda x, y : If(x, x, y)

printB(Or(And(f, t), Or(f, t)))
printB(And(And(t, t), Or(f, And(t, f))))

