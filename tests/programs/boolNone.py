print None

v1 = None
print v1

print None and False
v2 = None and False
print v2

print None and True
v3 = None and True
print v3

print False and None
v4 = False and None
print v4

print True and None
v5 = True and None
print v5

print None and None
v6 = None and None
print v6

print None or False
v7 = None or False
print v7

print None or True
v8 = None or True
print v8

print False or None
v9 = False or None
print v9

print True or None
v10 = True or None
print v10

print None or None
v11 = None or None
print v11