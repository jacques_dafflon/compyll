ref = [4,5,6]
less = [4,4,6]
more = [4,6,6]
longer = [4,5,6,7]
shorter = [4,5]
equal = [4,5,6]
diff = [1,2,3]

print "=="
print ref == less
print ref == more
print ref == longer
print ref == shorter
print ref == equal
print ref == diff

print "!="
print ref != less
print ref != more
print ref != longer
print ref != shorter
print ref != equal
print ref != diff

print "<="
print ref <= less
print ref <= more
print ref <= longer
print ref <= shorter
print ref <= equal
print ref <= diff

print ">="
print ref >= less
print ref >= more
print ref >= longer
print ref >= shorter
print ref >= equal
print ref >= diff

print "<"
print ref < less
print ref < more
print ref < longer
print ref < shorter
print ref < equal
print ref < diff

print ">"
print ref > less
print ref > more
print ref > longer
print ref > shorter
print ref > equal
print ref > diff