
z = lambda f, x : x
s = lambda n : lambda f, x : f(n(f, x))

toInt = lambda n : n(lambda x : x + 1, 0)

plus = lambda m, n : m(s, n)

curry = lambda f : lambda x : lambda y : f(x, y)

mult = lambda m, n : m(curry(plus)(n), z)

three = s(s(s(z)))
two = s(s(z))

print toInt(three)
print toInt(two)
print toInt(plus(three, two))
print toInt(mult(three, three))

