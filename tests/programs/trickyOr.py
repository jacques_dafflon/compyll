print 0 or False  # Should print 0
print False or 0  # Should print False
print 0 or False or True
print 0 or False or 7
print True or 3
print True or 0 or 2
print False or 0 or True
print False or 0 or False
