print not None

v1 = not None
print v1

print not (None and False)
v2 = not (None and False)
print v2

print not (None and True)
v3 = not (None and True)
print v3

print not (False and None)
v4 = not (False and None)
print v4

print not (True and None)
v5 = not (True and None)
print v5

print not (None and None)
v6 = not (None and None)
print v6

print not (None or False)
v7 = not (None or False)
print v7

print not (None or True)
v8 = not (None or True)
print v8

print not (False or None)
v9 = not (False or None)
print v9

print not (True or None)
v10 = not (True or None)
print v10

print not (None or None)
v11 = not (None or None)
print v11