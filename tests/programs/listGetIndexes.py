x = [4, 5, 6]
print x[0]
print x[1]
print x[2]

print [4, 5, 6][0]
print [4, 5, 6][1]
print [4, 5, 6][2]

a = x[0]
b = x[1]
c = x[2]

d = [4, 5, 6][0]
e = [4, 5, 6][1]
f = [4, 5, 6][2]

print a
print b
print c
print d
print e
print f

x = [4, 5, 6]
print x[-1]
print x[-2]
print x[-3]

print [4, 5, 6][-1]
print [4, 5, 6][-2]
print [4, 5, 6][-3]

a = x[-1]
b = x[-2]
c = x[-3]

d = [4, 5, 6][-1]
e = [4, 5, 6][-2]
f = [4, 5, 6][-3]

print a
print b
print c
print d
print e
print f

x = [4, 5, 6]
print x.__getitem__(0)
print x.__getitem__(1)
print x.__getitem__(2)

print [4, 5, 6].__getitem__(0)
print [4, 5, 6].__getitem__(1)
print [4, 5, 6].__getitem__(2)

a = x.__getitem__(0)
b = x.__getitem__(1)
c = x.__getitem__(2)

d = [4, 5, 6].__getitem__(0)
e = [4, 5, 6].__getitem__(1)
f = [4, 5, 6].__getitem__(2)

print a
print b
print c
print d
print e
print f

x = [4, 5, 6]
print x.__getitem__(-1)
print x.__getitem__(-2)
print x.__getitem__(-3)

print [4, 5, 6].__getitem__(-1)
print [4, 5, 6].__getitem__(-2)
print [4, 5, 6].__getitem__(-3)

a = x.__getitem__(-1)
b = x.__getitem__(-2)
c = x.__getitem__(-3)

d = [4, 5, 6].__getitem__(-1)
e = [4, 5, 6].__getitem__(-2)
f = [4, 5, 6].__getitem__(-3)

print a
print b
print c
print d
print e
print f