print not (None and 42)
v2 = not (None and 42)
print v2

print not (None and 0)
v3 = not (None and 0)
print v3

print not (42 and None)
v4 = not (42 and None)
print v4

print not (0 and None)
v5 = not (0 and None)
print v5

print not (None and None)
v6 = not (None and None)
print v6

print not (None or 42)
v7 = not (None or 42)
print v7

print not (None or 0)
v8 = not (None or 0)
print v8

print not (42 or None)
v9 = not (42 or None)
print v9

print not (0 or None)
v10 = not (0 or None)
print v10

print not (None or None)
v11 = not (None or None)
print v11