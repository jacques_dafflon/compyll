print "abc" * 3
print "abc" * 1
print "abc" * -1
print "abc" * -4
print "abc" * True
print "abc" * False

print 3 * "abc"
print 1 * "abc"
print -1 * "abc"
print -4 * "abc"
print True * "abc"

l = "def"
print l * 3
print l * 1
print l * -1
print l * -4
print l * True
print l * False

print 3 * l
print 1 * l
print -1 * l
print -4 * l
print True * l