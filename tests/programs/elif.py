if True:
	print 42  # print
elif False:
	print 43


if False:
	print 45
elif True:
	print 46  # print


if True:
	print 47
elif True:
	print 48  # print

if False:
	print 49
elif False:
	print 50
else:
	print 51  # print

if False:
    print False
elif False or True:
    print True  # print
else:
    print False
