
def cons(a, b):
    def get(t):
        if t:
            return a
        else:
            return b
    return get

def car(p):
    return p(True)

def cdr(p):
    return p(False)

nil = None

def singleton(x):
    return cons(x, nil)

def length(l):
    if l == nil:
        return 0
    return 1+ length(cdr(l))

def append(xs, ys):
    if xs == nil:
        return ys
    return cons(car(xs), append(cdr(xs), ys))

def reverse(xs):
    if xs == nil:
        return nil
    return append(cdr(xs), singleton(car(xs)))

def reverse_tail(xs):
    def helper(xs, ys):
        if xs == nil:
            return ys
        return helper(cdr(xs), cons(car(xs), ys))
    return helper(xs, nil)

def print_list(xs):
    def helper(xs):
        if xs != nil:
            print car(xs)
            helper(cdr(xs))
    print 8888
    helper(xs)
    print 7777

def get(xs, n):
    if n == 0:
        return car(xs)
    return get(cdr(xs), n - 1)

def map_(f, xs):
    if xs == nil:
        return nil
    return cons(f(car(xs)), map_(f, cdr(xs)))

def filter_(p, xs):
    if xs == nil:
        return nil
    if p(car(xs)):
        return cons(car(xs), filter_(p, cdr(xs)))
    return filter_(p, cdr(xs))

def fold(f, b, xs):
    if xs == nil:
        return b
    return f(car(xs), fold(f, b, cdr(xs)))

def foldl(f, b, xs):
    if xs == nil:
        return b
    return foldl(f, f(b, car(xs)), cdr(xs))

def unfold(f, x):
    t = f(x)
    if not car(t):
        return nil
    x_ = car(cdr(t))
    e = cdr(cdr(t))
    return cons(e, unfold(f, x_))

def curry(f):
    return lambda x : lambda y : f(x, y)

def uncurry(f):
    return lambda x, y : f(x)(y)

def print_fold(xs):
    def print_one(dummy, x):
        print x
    print 1111111111111
    foldl(print_one, None, xs)
    print 4444444444444

def map_fold(f, xs):
    return fold(lambda x, a : cons(f(x), a), nil, xs)

def filter_fold(p, xs):
    def h(x, a):
        if p(x):
            return cons(x, a)
        return a
    return fold(h, nil, xs)

def flip(f):
    return lambda x, y: f(y, x)

def reverse_fold(xs):
    return foldl(flip(cons), nil, xs)

def append_fold(xs, ys):
    return fold(cons, ys, xs)

def count_to(n):
    def f(x):
        if x > n:
            return cons(False, None)
        return cons(True, cons(x+1, x))
    return f

def add(x, y):
    return x+y

l = unfold(count_to(10), 1)
print_fold(l)

lp3 = map_(curry(add)(3), l)
print_fold(lp3)

print_fold(map_(lambda x : True, l))

lp10 = map_fold(curry(add)(10), l)
print_fold(lp10)

even = filter_(lambda x : x % 2 == 0, l)
print_fold(even)

odd = filter_fold(lambda x : x % 2 == 1, l)
print_fold(odd)

rev = reverse_fold(l)
print_fold(rev)

evenodd = append_fold(even, odd)
print_fold(evenodd)
