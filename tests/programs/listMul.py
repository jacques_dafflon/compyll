print [1,2,3] * 3
print [1,2,3] * 1
print [1,2,3] * -1
print [1,2,3] * -4
print [1,2,3] * True
print [1,2,3] * False

print 3 * [1,2,3]
print 1 * [1,2,3]
print -1 * [1,2,3]
print -4 * [1,2,3]
print True * [1,2,3]

l = [4,5,6]
print l * 3
print l * 1
print l * -1
print l * -4
print l * True
print l * False

print 3 * l
print 1 * l
print -1 * l
print -4 * l
print True * l