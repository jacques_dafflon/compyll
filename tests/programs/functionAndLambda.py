def f1():
    return lambda x:x

def f2(z):
    return (lambda y:y)(z)

print f2(f1())(4)
