
n = 2

while n < 100:
    k = 2
    p = True
    while k < n:
        if (n % k) == 0:
            p = False
        k += 1
    if p:
        print n
    n += 1
