x = "hello"
y = -1
z = 0
k = 1


print x < y
print x < z
print x < k

print y < x
print z < x
print k < x

print x <= y
print x <= z
print x <= k

print y <= x
print z <= x
print k <= x

print x == y
print x == z
print x == k

print y == x
print z == x
print k == x

print x != y
print x != z
print x != k

print y != x
print z != x
print k != x

print x > y
print x > z
print x > k

print y > x
print z > x
print k > x

print x >= y
print x >= z
print x >= k

print y >= x
print z >= x
print k >= x
