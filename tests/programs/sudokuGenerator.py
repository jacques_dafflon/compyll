n = 3
field = []
i = 0
while i < 9:
	field.append([])
	i+=1

i = 0
while i < n*n:
	j = 0
	while j < n*n:
		field[i].append((i*n + i/n + j) % (n*n) + 1)
		j += 1
	i += 1

print "+-------+-------+-------+"
i = 0
while i < n*n:
	line = "| "
	j = 0
	while j < n*n:
		line += str(field[i][j]) + " "
		if j % n == 2 and j < 8:
			line += "| "
		j += 1
	print line + "|"
	if i % n == 2:
		print "+-------+-------+-------+"
	i+=1