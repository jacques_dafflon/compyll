# brute-force sudoku solver
#
# based on https://code.google.com/p/brute-force-sudoku/
# adapted to work with compyll

def display(puzzle):
    print "+-------+-------+-------+"
    i = 0
    while i < 9:
        line = "| "
        j = 0
        while j < 9:
            line += str(puzzle[i*9+j]) + " "
            if j % 3 == 2 and j < 8:
                line += "| "
            j += 1
        print line + "|"
        if i % 3 == 2:
            print "+-------+-------+-------+"
        i+=1

def isValid(arr, pos):
    y = pos // 9
    x = pos % 9
    # check if gives pos is valid in arr

    # check vertical & horizontal
    v_tmp = []
    h_tmp = []
    i = 0
    while i < 9:
        nv = arr[(i*9)+x]
        nh = arr[(y*9)+i]

        if v_tmp.count(nv) > 0 or h_tmp.count(nh) > 0:
            return False
        if nv:
            v_tmp.append(nv)
        if nh:
            h_tmp.append(nh)

        i += 1

    # check sub-square (3x3)
    sub_y = (y/3)*3
    sub_x = (x/3)*3
    tmp  = []
    i = 0
    while i < 3:
        j = 0
        while j < 3:
            n = arr[(i*9)+(sub_y*9)+sub_x+j]
            if n and tmp.count(n) > 0:
                return False
            if n:
                tmp.append(n)

            j += 1

        i += 1

    # no problemos!
    return True

def notSolved(arr):
    i = 0
    while i < len(arr):
        if arr[i] == 0:
            return i
        i += 1
    return -1


def solve(arr):

    arr = arr[:] # working copy
    # recursive, backtracking sudoku solver

    pos = notSolved(arr)

    if pos == -1:
        return arr

    i = 1
    while i < 10:
        arr[pos] = i
        if isValid(arr, pos): # is valid move?
            sol = solve(arr)
            if sol:
                return sol
        i += 1

# MAIN

print "Brute Force sudoku Solver"
print ""
puzzle = [
    0, 6, 0, 3, 7, 2, 0, 0, 0,
    0, 5, 0, 0, 0, 0, 0, 3, 0,
    0, 2, 0, 5, 0, 0, 8, 7, 0,
    2, 0, 0, 0, 1, 5, 0, 0, 4,
    0, 0, 3, 0, 9, 0, 6, 0, 0,
    4, 0, 0, 6, 3, 0, 0, 0, 2,
    0, 9, 7, 0, 0, 1, 0, 6, 0,
    0, 3, 0, 0, 0, 0, 0, 4, 0,
    0, 0, 0, 7, 6, 3, 0, 2, 0
]
print "problem:"
display(puzzle)
print ""
print "solution"
display(solve(puzzle))