s1 = "abc"
s2 = "def"

print s1 + s2
print s1.__add__(s2)
s3 = s2 + s1
print s3
s4 = s2.__add__(s1)
print s4

print "abc" + "def"
print "abc".__add__("def")
s3 = "def" + "abc"
print s3
s4 = "def".__add__("abc")
print s4