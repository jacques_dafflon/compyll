def fact(x):
    if x == 0:
        return 1
    return x*fact(x - 1)

n = 10
while n > 0:
    print fact(n)
    n -= 1
