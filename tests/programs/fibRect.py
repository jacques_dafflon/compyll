
def fib(x):
    if x < 2:
        return 1
    return fib(x - 2) + fib(x - 1)

n = 1
while n < 12:
    print fib(n)
    n += 1
