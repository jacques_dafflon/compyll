#!/bin/bash
function trapdesc ()
{
   case $1 in
    0) echo "Bogus signal"
     ;;
    1) echo "Hangup"
     ;;
    2) echo "Interrupt"
     ;;
    3) echo "Quit"
     ;;
    4) echo "Illegal instruction"
     ;;
    5) echo "BPT trace/trap"
     ;;
    6) echo "ABORT instruction"
     ;;
    7) echo "Bus error"
     ;;
    8) echo "Floating point exception"
     ;;
    9) echo "Killed"
     ;;
    10) echo "User signal 1"
     ;;
    11) echo "Segmentation fault"
     ;;
    12) echo "User signal 2"
     ;;
    13) echo "Broken pipe"
     ;;
    14) echo "Alarm clock"
     ;;
    15) echo "Terminated"
     ;;
    17) echo "Child death or stop"
     ;;
    18) echo "Continue"
     ;;
    19) echo "Stopped (signal)"
     ;;
    20) echo "Stopped"
     ;;
    21) echo "Stopped (tty input)"
     ;;
    22) echo "Stopped (tty output)"
     ;;
    23) echo "Urgent IO condition"
     ;;
    24) echo "CPU limit"
     ;;
    25) echo "File limit"
     ;;
    26) echo "Alarm (virtual)"
     ;;
    27) echo "Alarm (profile)"
     ;;
    28) echo "Window changed"
     ;;
    29) echo "I/O ready"
     ;;
    30) echo "power failure imminent"
     ;;
    31) echo "Bad system call"
     ;;
    *) echo "Unknown exit code $1!"
     ;;
   esac
}
PKG="ComPyLL"
RUNTIME="$PKG/clib"
OLD_PATH="$PWD"
OPT="-O2"
TESTS_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#Go to the test directory
cd $( dirname "${BASH_SOURCE[0]}" )
cd ../..
echo "Cleaning Up for tests run..."
make clean-tests


FAILURES=0
ERRORS=0
PASSED=0
TOTAL=0
TEST_NAMES=()
STATUS=()
TEST_TIMES=()
TESTSUITE_TIME=0

TESTS=`ls $TESTS_PATH/*.py`

if [ $# -eq 1 ]; then
    TESTS=`ls $TESTS_PATH/$1`
fi

mkdir -p compiled
mkdir -p errors

echo "Generating libraries"
make -C $RUNTIME/ lib

XML="xunit-integration.xml"
printf "" > $XML

#TESTSUITE_START=$(python -c'import time; print repr(time.time())')
for TEST_FILE in $TESTS
do
    TEST_START=$(python -c'import time; print repr(time.time())')
    TEST=$(basename $TEST_FILE)
    TEST=${TEST%.*}
    TOTAL=$((TOTAL+1))
    NEW_LINE=false # For formatting
    printf "Running $TEST... "
    TEST_XML=""

    OUT_PY=`yes 42 2>/dev/null | python $TEST_FILE 2> errors/$TEST.ref.log`
    RET_PY=$?

    OUT_COMPILE=`coverage run --append --branch $PKG/compile.py $TEST_FILE \
            2> errors/$TEST.act.log > compiled/$TEST.ll`
    RET_COMPILE=$?

    if [[ ! -z "$OUT_COMPILE" ]]; then
        OUT_COMPILE=`echo "${OUT_COMPILE}\n"`
    fi

    ERRORS_REF=$(<errors/$TEST.ref.log)

    if [[ ( $RET_COMPILE == 0 ) ]]; then
        OUT_LLC=`llc $OPT -o compiled/$TEST.o -filetype=obj compiled/$TEST.ll 2>> errors/$TEST.act.log`
        RET_LLC=$?
        if [[ ! -z "$OUT_LLC" ]]; then
            OUT_LLC=`echo "${OUT_LLC}\n"`
        fi
    else
        OUT_LLC="ERROR: The file ${TEST}.ll was not generated.\n"
        RET_LLC=1
    fi

    if [[ ( $RET_LLC == 0 ) ]]; then
        OUT_LINK=`gcc $OPT -Wall -o compiled/$TEST compiled/$TEST.o $RUNTIME/compyll_lib.a -lgc 2>> errors/$TEST.act.log`
        RET_LINK=$?
        if [[ ! -z "$OUT_LINK" ]]; then
            OUT_LINK=`echo "${OUT_LINK}\n"`
        fi
    else
        OUT_LINK="ERROR: The executable $TEST was not generated.\n"
        RET_LINK=1
    fi

    if [[ ( $RET_LLC == 0 ) ]]; then
        OUT_RUN=`yes 42 2>/dev/null | compiled/$TEST 2>> errors/$TEST.act.log`
        RET_RUN=$?
        if [[ $RET_RUN -gt 128 ]]; then
            trapdesc `echo "$RET_RUN-128" | bc` >> errors/$TEST.act.log
        fi
    else
        OUT_RUN="ERROR: The executable $TEST was not run."
        RET_RUN=1
    fi

    TEST_END=$(python -c'import time; print repr(time.time())')
    TEST_TIME=$( echo "$TEST_END - $TEST_START" | bc | awk '{printf "%f", $0}' )
    TESTSUITE_TIME=$( echo "$TESTSUITE_TIME+$TEST_TIME" | bc )
    OUTPUT=`echo "${OUT_COMPILE}${OUT_LLC}${OUT_LINK}${OUT_RUN}"`

    ERRORS_ACTUAL=$(<errors/$TEST.act.log)
    ERRORS_ACTUAL_XML=$(sed -e 's~&~\&amp;~g' -e 's~<~\&lt;~g'  -e  's~>~\&gt;~g' < errors/$TEST.act.log)
    TEST_NAMES+=("$TEST")

    # (errors in both cases) OR
    # (success with warnings during compilation) OR
    # (success) OR
    # (expected failure)
    if [[ ( $RET_PY > 0 && $RET_RUN > 0 ) ]] || \
       [[ $TEST == warn_* && ! -z $ERRORS_ACTUAL && $ERRORS_ACTUAL == WARNING* && $RET_PY == 0 && $RET_RUN == 0 ]] || \
       [[ ( "$OUT_PY" == "$OUT_RUN" && $RET_PY == 0 && $RET_RUN == 0  ) ]] || \
       [[ ( $RET_PY == 0 && $RET_RUN > 0 && $TEST == fail_* ) ]];
    then
        STATUS+=("pass -")
        TEST_TIMES+=("$TEST_TIME")
        PASSED=$((PASSED+1))
        if [[ ( $RET_RUN -ne 0) ]] || [[ $TEST == warn_* ]] || [[ $TEST == fail_* ]];
        then
        echo ""
            echo "------------------->>> Expected  output <<<-------------------"
            if [[ ${#OUTPUT} -ne 0 ]]; then
                    echo "(No output)"
            else
                    echo -e "$OUTPUT"
            fi
            echo "------------------->>> Expected  errors <<<-------------------"
            if [[ -z $ERRORS_ACTUAL ]]; then
                    echo "(No errors)"
            else
                    echo -e "$ERRORS_ACTUAL"
            fi

            echo "--------------------------------------------------------------"
            printf " Ran $TEST...  -> pass"
            NEW_LINE=true
        else
            printf "  -> pass"
        fi
    else
        NEW_LINE=true
        if [[ $ERRORS_ACTUAL == *"Traceback (most recent call last):"* ]]; then # Compilation error
            STATUS+=("ERROR ")                                                  # Detected by the python stack trace.
            TEST_TIMES+=("$TEST_TIME")
            ERRORS=$((ERRORS+1))
            TEST_XML="\n\t\t<error type=\"$( echo "$ERROR_ACTUAL"| grep -ve "^  \|^Traceback\|^DEBUG\|^INFO\|^WARNING\|^ERROR\|^CRITICAL" | cut -d: -f1 )\">"
            TEST_XML="$TEST_XML\n$ERRORS_ACTUAL_XML\n\t\t</error>\n\t"
            echo "$TEST: ERRORED"
        elif [[ $ERRORS_ACTUAL == "lli:"* || $ERRORS_ACTUAL == *"\nlli:"* ]]; then #Invalid LLVM code
            STATUS+=("ERROR ")
            TEST_TIMES+=("$TEST_TIME")
            ERRORS=$((ERRORS+1))
            TEST_XML="$TEST_XML\n\t\t<error type=\"LLVM Error\" message=\"`grep -e \"lli\" <<< ${ERRORS_ACTUAL_XML} | sed -n 's/^lli:\(.*\): //p'`\">\n$ERRORS_ACTUAL_XML\n\t\t</error>\n\t"
            echo "$TEST: ERRORED (Compiled file is incorrect.)"
        else
            STATUS+=("FAIL -")
            TEST_TIMES+=("$TEST_TIME")
            FAILURES=$((FAILURES+1))
            TEST_XML="$TEST_XML\n\t\t<failure type=\"$ERRORS_ACTUAL_XML\">\n$ERRORS_ACTUAL_XML\n\t\t</failure>\n\t"
            echo "$TEST: FAILED"
        fi
        echo "------------------->>> Reference output <<<-------------------"
        if [[ ${#OUT_PY} -le 3 ]]; then
                echo "(No output)"
        else
                echo -e "$OUT_PY"
        fi
        echo "------------------->>> Reference errors <<<-------------------"
        if [[ -z $ERRORS_REF ]]; then
                echo "(No errors)"
        else
                echo -e "$ERRORS_REF"
        fi
        echo "-------------------->>> Actual  output <<<--------------------"
        if [[ ${#OUTPUT} -le 3 ]]; then
                echo "(No output)"
        else
                echo -e "$OUTPUT"
        fi
        echo "-------------------->>> Actual  errors <<<--------------------"
        if [[ -z $ERRORS_ACTUAL ]]; then
                echo "(No errors)"
        else
                echo -e "$ERRORS_ACTUAL"
        fi
    fi
    TEST_XML="\t<testcase name=\"$TEST\" classname=\"$TEST_FILE\" time=\"$TEST_TIME\">$TEST_XML</testcase>"
    echo -e "$TEST_XML" >> $XML
    printf "  (${TEST_TIME}s)\n"
    if $NEW_LINE ; then #Formatting
            echo ""
    fi
done

echo -e "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<testsuite name=\"$PKG Integration Tests\" timestamp=$(date +\"%Y-%m-%dT%H:%M:%S\") hostname=\"$( hostname )\" tests=\"$TOTAL\" failures=\"$FAILURES\" errors=\"$ERRORS\" time=\"$TESTSUITE_TIME\">" | cat - $XML > temp && mv temp $XML
# if [[ -f "$XML-e" ]]; then
# 	echo "Clean up"
# 	rm "$XML-e"
#fi
echo "</testsuite>" >> $XML
echo
echo "------------------->>> Coverage Details <<<-------------------"
# echo "          (potentialy including unit tests coverage)          "
# echo "--------------------------------------------------------------"
coverage report --include="$PKG/*"
echo -e "\n------------------------>>> Summary <<<------------------------"
for ((i=0; i<"${#TEST_NAMES[@]}"; i++)); do
    echo "${STATUS[$i]}-> ${TEST_NAMES[$i]} (${TEST_TIMES[$i]}s)"
done

echo -e "\n------------------------>>> Result <<<------------------------"
echo "$PASSED tests of $TOTAL passed (errors: $ERRORS, failures: $FAILURES)"
echo "Duration: ${TESTSUITE_TIME} seconds."

cd "$OLD_PATH"

if [[ $PASSED == $TOTAL ]]; then
    echo "TESTS: SUCCESS"
else
    echo "TESTS: FAILURE"
fi

exit 0
