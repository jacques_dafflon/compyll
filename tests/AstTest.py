import unittest
import random

import ComPyLL.ast as ast


class AstNodeTestCase(unittest.TestCase):

    def set_up(self):
        pass  # For now...

    def simple_operation(self):
        # Test specific fixture
        left = random.randint(1, 100)
        right = random.randint(1, 100)
        node = self.node_type(ast.Const(left), ast.Const(right))
        representation = "{0}(Const({1}), Const({2}))".format(node.__class__.__name__, left, right)

        self.assertEqual(representation, str(node), "".join(["\nExpected: ", representation,
                                                                       "\nReceived: ", str(node)]))
        # Check left child
        self.assertIsInstance(node.left, ast.Const)
        self.assertEqual(node.left.value, left,
                         "".join(["\nExpected: ", str(left), "\nReceived: ", str(node.left.value)]))
        #Check right child
        self.assertIsInstance(node.right, ast.Const)
        self.assertEqual(node.right.value, right,
                         "".join(["\nExpected: ", str(right), "\nReceived: ", str(node.right.value)]))
        self.assertIsNone(node.lineno)

class AstAddTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Add
        super(AstAddTestCase, self).set_up()

    def test_simple_add(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstBitandTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Bitand
        super(AstBitandTestCase, self).set_up()

    def test_simple_bitand(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstBitorTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Bitor
        super(AstBitorTestCase, self).set_up()

    def test_simple_bitor(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstBitxorTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Bitxor
        super(AstBitxorTestCase, self).set_up()

    def test_simple_bitxor(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstDivTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Div
        super(AstDivTestCase, self).set_up()

    def test_simple_div(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstFloorDivTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.FloorDiv
        super(AstFloorDivTestCase, self).set_up()

    def test_simple_floordiv(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstLeftShiftTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.LeftShift
        super(AstLeftShiftTestCase, self).set_up()

    def test_simple_leftshift(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstModTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Mod
        super(AstModTestCase, self).set_up()

    def test_simple_mod(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstMulTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Mul
        super(AstMulTestCase, self).set_up()

    def test_simple_mul(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstRightShiftTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.RightShift
        super(AstRightShiftTestCase, self).set_up()

    def test_simple_rightshift(self):
        self.simple_operation()

    setUp = set_up  # PEP 8


class AstSubTestCase(AstNodeTestCase):

    def set_up(self):
        self.node_type = ast.Sub
        super(AstSubTestCase, self).set_up()

    def test_simple_sub(self):
        self.simple_operation()

    setUp = set_up  # PEP 8

if __name__ == '__main__':
    unittest.main()
