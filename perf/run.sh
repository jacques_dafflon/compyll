#Save current working directory and go to script source
OLD_PATH="$PWD"
OPT="-O2"
ITER="$1"

if [[ -z "$1" ]]; then
	echo "$ ./run.sh <number of runs>"
	exit 1
fi

cd $( dirname "${BASH_SOURCE[0]}" )

# compile runtime.c
make -C ../ComPyLL/clib clean-all
make -C ../ComPyLL/clib lib

# use our compiler
echo "Compiling program"
START=$(python -c'import time; print repr(time.time())')
python ../ComPyLL/compile.py prog.py > prog.ll
END=$(python -c'import time; print repr(time.time())')
COMPILE_TIME=$( echo "$END-$START" | bc )
echo "$COMPILE_TIME" > compile_time.log

# ll to obj
llc $OPT -o prog.o -filetype=obj prog.ll

# link and compile
gcc -Wall $OPT -o prog ../ComPyLL/clib/compyll_lib.a prog.o -lgc

printf "" > python_times.log
TIME_PYTHON=0
echo "Running python programm $ITER times."
# run the program
for i in $(eval echo {1..$ITER}); do
	printf "."
	START=$(python -c'import time; print repr(time.time())')
	python prog.py > /dev/null
	END=$(python -c'import time; print repr(time.time())')
	RUN_TIME=$( echo "$END-$START" | bc)
	echo "$RUN_TIME" >> python_times.log
	TIME_PYTHON=$( echo "$TIME_PYTHON + $RUN_TIME" | bc )
done

echo ""
printf "" > compile_times.log
TIME_COMPYLL=0
echo "Running compiled programm $ITER times."
# run the program
for i in $(eval echo {1..$ITER}); do
	printf "."
	START=$(python -c'import time; print repr(time.time())')
	./prog > /dev/null
	END=$(python -c'import time; print repr(time.time())')
	RUN_TIME=$( echo "$END-$START" | bc)
	echo "$RUN_TIME" >> compile_times.log
	TIME_COMPYLL=$( echo "$TIME_COMPYLL + $RUN_TIME" | bc )
done

echo ""
echo "PYTHON:"
python compute.py python_times.log
echo ""
echo "PYTHON:"
python compute.py compile_times.log

# remove all the crap
if [ $# -eq 0 ]; then
	rm ../*.pyc
fi

# Cd back to current dir
cd "$OLD_PATH"
exit 0