#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import sys

if len(sys.argv) != 2:
	print "python compute.py <data_file>"
	exit(1)

data = []
with open(sys.argv[1], 'r') as f:
	for line in f.read().split():
		val = line.strip()
		data.append(float(val))

total = sum(data)
mean = total/len(data)
max_val = max(data)
min_val = min(data)
diffs = []
for val in data:
	diffs.append((val-mean)*(val-mean))
variance = sum(diffs)/(len(diffs)-1)
sd = variance**0.5
se = sd/(len(data)**0.5)

print "Total: {}s".format(total)
print "Mean: {}s".format(mean)
print "Variance: {}s".format(variance)
print (u"Standard deviation: {}s \u00B1 {}s (95%)".format(sd, 1.96*se)).encode('utf-8', 'replace')
print "Standard error: {}s".format(se)