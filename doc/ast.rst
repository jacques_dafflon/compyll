AST Nodes
---------
   	This page lists all the nodes for the abstract syntax tree (AST) generated from the parser (Work in progress):

List of nodes
=============
Only the concrete nodes should be instantiated.
Abstract nodes are the super classes of the concrete nodes and shuold not be used directly.

Abstract nodes
**************
.. autosummary::
        ComPyLL.ast.Node
        ComPyLL.ast.BinaryNode
        ComPyLL.ast.ExpNode

Concrete nodes
**************
.. autosummary::
        ComPyLL.ast.Add
        ComPyLL.ast.And
        ComPyLL.ast.AssName
        ComPyLL.ast.AssRegister
        ComPyLL.ast.Assign
        ComPyLL.ast.AugAssign
        ComPyLL.ast.Bitand
        ComPyLL.ast.Bitor
        ComPyLL.ast.Bitxor
        ComPyLL.ast.CallFunc
        ComPyLL.ast.Const
        ComPyLL.ast.Discard
        ComPyLL.ast.Div
        ComPyLL.ast.Equal
        ComPyLL.ast.FloorDiv
        ComPyLL.ast.GreaterEqual
        ComPyLL.ast.GreaterThan
        ComPyLL.ast.If
        ComPyLL.ast.Invert
        ComPyLL.ast.LeftShift
        ComPyLL.ast.LessEqual
        ComPyLL.ast.LessThan
        ComPyLL.ast.Mod
        ComPyLL.ast.Module
        ComPyLL.ast.Mul
        ComPyLL.ast.Name
        ComPyLL.ast.Not
        ComPyLL.ast.NotEqual
        ComPyLL.ast.Or
        ComPyLL.ast.Pass
        ComPyLL.ast.Printnl
        ComPyLL.ast.Register
        ComPyLL.ast.RightShift
        ComPyLL.ast.Stmt
        ComPyLL.ast.Sub
        ComPyLL.ast.UnaryAdd
        ComPyLL.ast.UnarySub
        ComPyLL.ast.While
        
.. automodule:: ComPyLL.ast
	:members:
	:show-inheritance:
        :member-order: bysource