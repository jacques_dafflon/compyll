.. ComPyLL documentation master file, created by
   sphinx-quickstart on Wed Oct  2 01:21:41 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ComPyLL's documentation!
###################################


ComPyLL is a Compiler from Python to LLVM, written in Python.

Credits
*******
* Jacques Dafflon <jacques.dafflon@usi.ch>
* Giorgio Gori <gorig@usi.ch>
* Giovanni Viviani <vivianig@usi.ch>


Contents
********

.. toctree::
   :maxdepth: 2

   license
   readme
   doc
   ast

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

