Documentation
-------------
   	Official Documentation  (Work in progress):
        
        This page contains the documentation for the recommended methods to use to compile code.
        
        For the :doc:`ast`, you can find the complete list :doc:`here </ast>`.

Available methods (API)
=======================

.. autosummary::
        ComPyLL.compile
        ComPyLL.find_variables
        ComPyLL.flatten
        ComPyLL.generate_code
        ComPyLL.parse
        

Details
=======

.. automodule:: ComPyLL
	:members:
	:undoc-members:
	:show-inheritance:
