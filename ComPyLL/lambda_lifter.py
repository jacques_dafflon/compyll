import logging
import ast
from variable_generator import generate_function


__all__ = ["lift_lambda"]


def lift_lambda(tree):
    lambda_lifter = LambdaLifter()
    return lambda_lifter.lift_lambda(tree)


class LambdaLifter():
    def __init__(self):
        self.lambdas = dict()
        self.functions = list()

    def lift_lambda(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def String(self, tree):  # pylint: disable=C0103
        return tree

    def MakeClosure(self, tree):  # pylint: disable=C0103
        tree.func = self.lift_lambda(tree.func)
        return tree

    def EnvRef(self, tree):  # pylint: disable=C0103
        return tree

    def ObjRef(self, tree):
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def ConvertedLambda(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)

        f_name = generate_function()
        self.functions.append(ast.Function(f_name, tree.params, tree.exp))
        return ast.Name(f_name)

    def Class(self, tree):  # pylint: disable=C0103

        # Look for init
        init = None
        for stmt in tree.body.nodes:
            if type(stmt) is ast.Assign:
                if type(stmt.exp) is ast.MakeClosure:
                    for ass_name in stmt.nodes:
                        if ass_name.name == '__init__':
                            init = stmt.exp

        assert init is not None  # make sure that there is an init
        init_lambda = init.func  # save the lambda, it will be replaced in the next line
        init_params = init_lambda.params

        tree.body = self.lift_lambda(tree.body)  # lambda lift

        statements = list()  # future body of the function
        statements.append(ast.Assign([ast.AssName('self')], ast.MakeObject()))

        # convert simple assignments to assignment to object fields
        for stmt in tree.body.nodes:
            statements.append(stmt)
            if type(stmt) is ast.Assign:
                # Convert assignments to name into assignments to object attributes
                nodes = list()
                for assnode in stmt.nodes:
                    assert type(assnode) is ast.AssName, "we don't support class body"
                    nodes.append(ast.AssObjRef(ast.Name('self'), assnode.name))
                stmt.nodes = nodes

        params = init_params[1:]  # first parameter is Name('self'), ignore it

        # Call __init__
        statements.append(ast.Discard(ast.CallFunc(ast.ObjRef(ast.Name('self'), '__init__'), params)))

        # Return the object
        statements.append(ast.Return(ast.Name('self')))

        f_name = generate_function()
        self.functions.append(ast.Function(f_name, params, ast.Stmt(statements)))

        return ast.Assign([ast.AssName(tree.name.name)],
                          ast.MakeClosure(ast.Name(f_name), len(params), [], tree.name.name))

    def Module(self, tree):  # pylint: disable=C0103
        tree.node = self.lift_lambda(tree.node)
        tree.funcs = ast.Stmt(self.functions)
        return tree

    def Stmt(self, tree):  # pylint: disable=C0103
        tree.nodes = map(self.lift_lambda, tree.nodes)
        return tree

    def Discard(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def If(self, tree):  # pylint: disable=C0103
        tree.cond = self.lift_lambda(tree.cond)
        tree.body = self.lift_lambda(tree.body)
        if tree.other:
            tree.other = self.lift_lambda(tree.other)
        return tree

    def Const(self, tree):  # pylint: disable=C0103
        return tree

    def NoneNode(self, tree):  # pylint: disable=C0103
        return tree

    def CallFunc(self, tree):  # pylint: disable=C0103
        tree.node = self.lift_lambda(tree.node)
        tree.args = map(self.lift_lambda, tree.args)
        return tree

    def Assign(self, tree):  # pylint: disable=C0103
        tree.nodes = map(self.lift_lambda, tree.nodes)
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def AugAssign(self, tree):  # pylint: disable=C0103
        tree.node = self.lift_lambda(tree.node)
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def List(self, tree):  # pylint: disable=C0103
        tree.items = [self.lift_lambda(item) for item in tree.items]
        return tree

    def Index(self, tree):
        tree.exp = self.lift_lambda(tree.exp)
        tree.index = self.lift_lambda(tree.index)
        return tree

    def AssIndex(self, tree):
        tree.exp = self.lift_lambda(tree.exp)
        tree.index = self.lift_lambda(tree.index)
        return tree

    def Splice(self, tree):
        if tree.left:
            tree.left = self.lift_lambda(tree.left)
        if tree.right:
            tree.right = self.lift_lambda(tree.right)
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def Printnl(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def Name(self, tree):  # pylint: disable=C0103
        return tree

    def Register(self, tree):  # pylint: disable=C0103
        return tree

    def AssName(self, tree):  # pylint: disable=C0103
        return tree

    def AssObjRef(self, tree):
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def AssRegister(self, tree):  # pylint: disable=C0103
        return tree

    # ==== Arithmetic operations ====
    def Add(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Sub(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Mul(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Div(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def FloorDiv(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Mod(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def LeftShift(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def RightShift(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    # ==== Bit operations ====
    def Bitand(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Bitor(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Bitxor(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Or(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def And(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def UnaryAdd(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def UnarySub(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def Invert(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def Not(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    # ==== Boolead Operations ====
    def Equal(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def LessEqual(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def GreaterEqual(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def NotEqual(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def LessThan(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def GreaterThan(self, tree):  # pylint: disable=C0103
        tree.left = self.lift_lambda(tree.left)
        tree.right = self.lift_lambda(tree.right)
        return tree

    def Pass(self, tree):  # pylint: disable=C0103
        return tree

    def Return(self, tree):  # pylint: disable=C0103
        tree.exp = self.lift_lambda(tree.exp)
        return tree

    def While(self, tree):  # pylint: disable=C0103
        tree.cond = self.lift_lambda(tree.cond)
        tree.body = self.lift_lambda(tree.body)
        return tree
