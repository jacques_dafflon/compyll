"""
.. currentmodule:: ComPyLL

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>


This package provides method to transform flatten subset of Python 2 code into a flatten AST and/or LLVM.
"""

from ComPyLL.code_generator import generate_code
from ComPyLL.compile import compile
from ComPyLL.compyll_parser import parse
from ComPyLL.flattener import flatten
from ComPyLL.string_finder import find_strings
from ComPyLL.variable_finder import find_variables

__all__ = ["ast", "compile", "find_variables", "flatten", "generate_code", "parse"]
