"""
.. module:: variable_generator
   :platform: Unix, OSX
   :synopsis: Generate variable names.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

The variable_generator module generates new unique variable names generate_variable each call.
It is used for temporary variables during the flattening process.
"""

__all__ = ["generate_register", "generate_label", "generate_variable", "generate_function"]


def generate_variable():
    generate_variable.counter += 1
    return '.v' + str(generate_variable.counter)
generate_variable.counter = 0


def generate_register():
    generate_register.counter += 1
    return '%.r' + str(generate_register.counter)
generate_register.counter = 0


def generate_label():
    generate_label.counter += 1
    return 'lab' + str(generate_label.counter)
generate_label.counter = 0


def generate_function():
    generate_function.counter += 1
    return '.f' + str(generate_function.counter)
generate_function.counter = 0
