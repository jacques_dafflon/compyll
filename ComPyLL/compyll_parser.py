"""
.. module:: compyll_parser
   :platform: Unix, OSX
   :synopsis: Flatten an AST.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>
"""
# pylint: disable=C0103

import sys
import logging
import lexer
import ply.yacc as yacc
from lexer import tokens
from ast import *

__all__ = ["parse"]

precedence = (
    ('left', 'ASSIGN'),
    ('left', 'AUGPLUS', 'AUGMINUS',
        'AUGTIMES', 'AUGDIVIDE',
        #'AUGPOWER',
        'AUGFLOORDIVIDE', 'AUGMOD',
        'AUGLEFTSHIFT', 'AUGRIGHTSHIFT',
        'AUGBITAND', 'AUGBITOR', 'AUGBITXOR'),
    ('right', 'COLON'),                                 # lambda
    ('left', 'IF', 'ELSE'),                             # if - else
    ('left', 'OR'),                                     # or
    ('left', 'AND'),                                    # and
    # missing                                           # not x
    ('nonassoc', 'NOT'),                                # in, is
    #('nonassoc', 'IN', 'IS'),                          # not
    ('left', 'LESSER', 'LESSEREQUAL',                   # <, <=, >, >=, <>, !=, ==
        'GREATER', 'GREATEREQUAL',
        'NOTEQUAL', 'EQUALS'),
    ('left', 'BITOR'),                                  # |
    ('left', 'BITXOR'),                                 # ^
    ('left', 'BITAND'),                                 # &
    ('left', 'LEFTSHIFT', 'RIGHTSHIFT'),                # <<, >>
    ('left', 'PLUS', 'MINUS'),                          # +, -
    ('left', 'TIMES', 'DIVIDE', 'FLOORDIVIDE', 'MOD'),  # *, /, //, %
    ('right', 'INVERT', 'UMINUS'),
    ('right', 'DOT'),
    #('right', 'POWER'),                                  # **
    ('left', 'OBRACKET', 'CBRACKET'),                   # x[index]
    ('left', 'OPAREN', 'CPAREN'),                       # (expression...)
    # ('OCURLY', 'CCURLY'),                             # {key:value}
)


# ====================== Module / lines ======================
def p_module(p):
    """module : lines"""
    p[0] = Module(p[1], None, p.lineno(1))


def p_module2(p):
    """module :
    | newlines"""
    p[0] = Module(Stmt([]))


def p_newlines(p):
    """newlines : newlines NEWLINE
    | NEWLINE"""
    p[0] = None


def p_newlines_or_empty(p):
    """space : newlines
    | """
    p[0] = None


def p_lines(p):
    """lines : space statement morelines"""
    p[0] = Stmt([p[2]] + p[3])


def p_morelines(p):
    """morelines : statement morelines"""
    p[0] = [p[1]] + p[2]


def p_morelines_empty(p):
    """morelines :  """
    p[0] = []


# ====================== Statements ======================
def p_statement(p):
    """statement : print_stmt newlines
    | assign_stmt newlines
    | augassign_stmt newlines
    | pass_stmt newlines
    | return_stmt newlines
    | if_stmt
    | while_stmt
    | function_def_stmt
    | class_def_stmt
    | discard_stmt newlines"""
    p[0] = p[1]


def p_pass_stmt(p):
    """pass_stmt : PASS"""
    p[0] = Pass(p.lineno(1))


def p_return_stmt(p):
    """return_stmt : RETURN expression"""
    p[0] = Return(p[2], p.lineno(1))


def p_return_empty(p):
    """return_stmt : RETURN """
    p[0] = Return(NoneNode(p.lineno(1)), p.lineno(1))


def p_print_stmt(p):
    """print_stmt : PRINT expression"""
    p[0] = Printnl(p[2], p.lineno(1))


def p_discard_stmt(p):
    """discard_stmt : expression"""
    p[0] = Discard(p[1], p.lineno(1))


# ====================== Assign Statement ======================
def p_assign_stmt(p):
    """assign_stmt : expression ASSIGN right_assign"""
    assvars = p[3][0]
    expression = p[3][1]
    p[0] = Assign([get_ass_node(p[1])] + assvars, expression, p.lineno(2))


def p_right_assign(p):
    """right_assign : expression ASSIGN right_assign"""
    assvars = p[3][0]
    expression = p[3][1]
    p[0] = ([get_ass_node(p[1])] + assvars, expression)


def p_right_assign_end(p):
    """right_assign : expression"""
    p[0] = ([], p[1])


def get_ass_node(node):
    if type(node) is Name:
        return AssName(node.name, node.lineno)
    elif type(node) is Index:
        return AssIndex(node.exp, node.index, node.lineno)
    elif type(node) is ObjRef:
        return AssObjRef(node.exp, node.name, node.lineno)
    else:
        logging.error("SyntaxError: invalid syntax at line {}".format(node.lineno))
        exit(1)

# ====================== AugAssign Statement ======================
def p_augassign_stmt(p):
    """augassign_stmt : expression AUGPLUS expression
    | expression AUGMINUS expression
    | expression AUGTIMES expression
    | expression AUGDIVIDE expression
    | expression AUGFLOORDIVIDE expression
    | expression AUGMOD expression
    | expression AUGLEFTSHIFT expression
    | expression AUGRIGHTSHIFT expression
    | expression AUGBITAND expression
    | expression AUGBITOR expression
    | expression AUGBITXOR expression"""
    p[0] = AugAssign(p[1], p[2], p[3], p.lineno(2))


# ====================== Expression: simple terminals ======================
def p_expression_INTEGER(p):
    """expression : INTEGER"""
    p[0] = Const(int(p[1]), p.lineno(1))


def p_expression_IDENTIFIER(p):
    """expression : IDENTIFIER"""
    p[0] = Name(p[1], p.lineno(1))


def p_expression_STRING(p):
    """expression : STRING"""
    p[0] = String(str(p[1][1:-1]), p.lineno(1))

    
def p_expression_NONE(p):
    """expression : NONE"""
    p[0] = NoneNode(p.lineno(1))


# ====================== Expression: Binary operators ======================
def p_expression_BinaryOperators(p):
    """expression : expression PLUS expression
    | expression MINUS expression
    | expression TIMES expression
    | expression DIVIDE expression
    | expression FLOORDIVIDE expression
    | expression MOD expression
    | expression LEFTSHIFT expression
    | expression RIGHTSHIFT expression
    | expression BITAND expression
    | expression BITOR expression
    | expression BITXOR expression
    | expression AND expression
    | expression OR expression
    | expression EQUALS expression
    | expression LESSEREQUAL expression
    | expression GREATEREQUAL expression
    | expression NOTEQUAL expression
    | expression LESSER expression
    | expression GREATER expression"""
    if p[2] == '+':     p[0] = Add(p[1], p[3], p.lineno(2))
    elif p[2] == '-':   p[0] = Sub(p[1], p[3], p.lineno(2))
    elif p[2] == '*':   p[0] = Mul(p[1], p[3], p.lineno(2))
    elif p[2] == '/':   p[0] = Div(p[1], p[3], p.lineno(2))
    elif p[2] == '//':  p[0] = FloorDiv(p[1], p[3], p.lineno(2))
    elif p[2] == '%':   p[0] = Mod(p[1], p[3], p.lineno(2))
    elif p[2] == '<<':  p[0] = LeftShift(p[1], p[3], p.lineno(2))
    elif p[2] == '>>':  p[0] = RightShift(p[1], p[3], p.lineno(2))
    elif p[2] == '&':   p[0] = Bitand(p[1], p[3], p.lineno(2))
    elif p[2] == '|':   p[0] = Bitor(p[1], p[3], p.lineno(2))
    elif p[2] == '^':   p[0] = Bitxor(p[1], p[3], p.lineno(2))

    elif p[2] == 'and': p[0]= And(p[1], p[3], p.lineno(2))
    elif p[2] == 'or' : p[0]= Or(p[1], p[3], p.lineno(2))

    elif p[2] == '==':  p[0] = Equal(p[1], p[3], p.lineno(2))
    elif p[2] == '<=':  p[0] = LessEqual(p[1], p[3], p.lineno(2))
    elif p[2] == '>=':  p[0] = GreaterEqual(p[1], p[3], p.lineno(2))
    elif p[2] == '!=':  p[0] = NotEqual(p[1], p[3], p.lineno(2))
    elif p[2] == '<>':  p[0] = NotEqual(p[1], p[3], p.lineno(2))
    elif p[2] == '<':   p[0] = LessThan(p[1], p[3], p.lineno(2))
    elif p[2] == '>':   p[0] = GreaterThan(p[1], p[3], p.lineno(2))
    else:
        logging.error('CANNOT FIND MATCHING RULE FOR BINARY OPERATOR - BAD')


# ====================== Expression: Unary operators ======================
def p_expression_UnaryOperators(p):
    """expression : PLUS expression
    | INVERT expression
    | NOT expression """
    if p[1] == '+': p[0] = UnaryAdd(p[2], p.lineno(1))
    elif p[1] == '~': p[0] = Invert(p[2], p.lineno(1))
    elif p[1] == 'not': p[0] = Not(p[2], p.lineno(1))
    else:
        logging.error("BAD - CANNOT FIND MATCHING RULE FOR UNARY OPERATOR - BAD")


def p_expression_unary_minus(p):
    """expression : MINUS expression %prec UMINUS"""
    p[0] = UnarySub(p[2], p.lineno(2))


def p_expression_Brackets(p):
    """expression : OPAREN expression CPAREN"""
    p[0] = p[2]


# ====================== CallFunc ======================
def p_expression_CallFunc(p):
    """expression : expression OPAREN function_param CPAREN"""
    p[0] = CallFunc(p[1], p[3], p.lineno(1))


def p_function_param(p):
    """function_param : expression more_params"""
    p[0] = [p[1]] + p[2]


def p_function_param_empty(p):
    """function_param : """
    p[0] = []


def p_function_more_params(p):
    """more_params : COMMA expression more_params"""
    p[0] = [p[2]] + p[3]


def p_function_more_params_empty(p):
    """more_params : """
    p[0] = []


# ====================== Function Definitions ==================
def p_function_def(p):
    """function_def_stmt : DEF IDENTIFIER OPAREN function_args CPAREN COLON NEWLINE INDENT lines DEDENT"""
    p[0] = Function(p[2], p[4], p[9], p.lineno(1))


def p_function_args(p):
    """function_args : IDENTIFIER more_args"""
    p[0] = [Name(p[1], p.lineno(1))] + p[2]


def p_function_args_empty(p):
    """function_args : """
    p[0] = []


def p_function_more_args(p):
    """more_args : COMMA IDENTIFIER more_args"""
    p[0] = [Name(p[2], p.lineno(2))] + p[3]


def p_function_more_args_empty(p):
    """more_args : """
    p[0] = []


# ====================== Lambdas ======================
def p_lambda(p):
    """expression : LAMBDA function_args COLON expression"""
    p[0] = Lambda(p[2], p[4], p.lineno(1))


# ====================== Class Definitions ==================
def p_class_def(p):
    """class_def_stmt : CLASS IDENTIFIER OPAREN superclass CPAREN COLON NEWLINE INDENT lines DEDENT"""
    p[0] = Class(Name(p[2], p.lineno(1)), p[4], p[9], p.lineno(1))


def p_superclass(p):
    """superclass : IDENTIFIER"""
    p[0] = Name(p[1], p.lineno(1))


def p_superclass_empty(p):
    """superclass : """
    p[0] = None


# ====================== Object Reference ==================
def p_obj_ref(p):
    """expression : expression DOT IDENTIFIER"""
    p[0] = ObjRef(p[1], p[3], p.lineno(2))


# ====================== Conditionals ======================
def p_if_stmt(p):
    """if_stmt : IF expression COLON NEWLINE INDENT lines DEDENT else_stmt"""
    p[0] = If(p[2], p[6], p[8], p.lineno(1))


def p_else_stmt(p):
    """else_stmt : ELSE COLON NEWLINE INDENT lines DEDENT"""
    p[0] = p[5]


def p_elif_stmt(p):
    """else_stmt : ELIF expression COLON NEWLINE INDENT lines DEDENT else_stmt"""
    p[0] = Stmt([If(p[2], p[6], p[8], p.lineno(1))])


def p_else_empty(p):
    """else_stmt : """
    p[0] = None


# ====================== Loops ======================
def p_while_stmt(p):
    """while_stmt : WHILE expression COLON NEWLINE INDENT lines DEDENT """
    p[0] = While(p[2], p[6], p.lineno(1))


# ======================== List Definitions ====================
def p_list_def(p):
    """expression : OBRACKET expression_list CBRACKET"""
    p[0] = List(p[2], p.lineno(1))


def p_expression_list(p):
    """expression_list : expression more_items"""
    p[0] = [p[1]] + p[2]


def p_expression_list_empty(p):
    """expression_list : """
    p[0] = []


def p_list_items(p):
    """more_items : COMMA expression more_items"""
    p[0] = [p[2]] + p[3]


def p_list_items_comma(p):
    """more_items : COMMA"""
    p[0] = []


def p_list_items_empty(p):
    """more_items : """
    p[0] = []


# ======================== Indexing ====================
def p_index_def(p):
    """expression : expression OBRACKET expression CBRACKET"""
    p[0] = Index(p[1], p[3], p.lineno(2))


def p_splice_def(p):
    """expression : expression OBRACKET splice_item COLON splice_item CBRACKET"""
    p[0] = Splice(p[1], p[3], p[5], p.lineno(2))


def p_splice_item(p):
    """splice_item : expression"""
    p[0] = p[1]


def p_splice_item_empty(p):
    """splice_item : """
    p[0] = None


def parse(string):
    """Parse python code.

    This function parses python code and return an AST representing the code.

    Args:
        string (str): The string representing the python code

    Returns:
        ast.Module. The root of the ast
    """

    if not string.endswith('\n'):
        assert isinstance(string, str)
        string += '\n'

    lexer.compyll_lex()
    #yacc.yacc(errorlog=yacc.NullLogger(), debug=True)
    yacc.yacc(errorlog=logging, debug=True)
    parsed = None
    try:
        parsed = yacc.parse(string)
    except IndentationError as e:
        last_cr = string.rfind('\n', 0, e.offset) + 1
        after_cr = string.find('\n', e.offset)
        if last_cr < 0:
            last_cr = 0
        line = string[last_cr:after_cr]
        column = (e.offset - last_cr)
        sys.stderr.write("ERROR: Indentation! Unexpected {0} on line {1} col {2}\n".format(e.text, e.lineno, column))
        sys.stderr.write(line + '\n')
        sys.stderr.write(" " * (column - 1))
        sys.stderr.write("^\n")
        exit(1)
    except SyntaxError as e:
        last_cr = string.rfind('\n', 0, e.offset) + 1
        after_cr = string.find('\n', e.offset)
        if last_cr < 0:
            last_cr = 0
        line = string[last_cr:after_cr]
        column = (e.offset - last_cr) + 1
        token = "\"" + (str(e.text) + "\"" if str(e.text) != '\n' else '\\\\n\" (newline)')  # display newline char
        sys.stderr.write("ERROR: Syntax! Unexpected token {0} on line {1} col {2}\n".format(token, e.lineno, column))
        sys.stderr.write(line + '\n')
        sys.stderr.write(" " * (column - 1))
        sys.stderr.write("^\n")
        exit(1)
    return parsed


def p_error(p):
    se = SyntaxError()
    if p is None:
        logging.error('PARSER ERROR - AND p IS NONE, I DONT KNOW WHAT TO DO')
        se.offset = 0
        se.lineno = 0
        se.text = ''
    elif p.type == "INDENT":
        ie = IndentationError()
        ie.text = "indent"
        ie.lineno = p.lineno
        ie.offset = p.lexpos
        raise ie
    else:
        se.text = p.value
        se.lineno = p.lineno
        se.offset = p.lexpos
    raise se


if __name__ == '__main__':
    string = """
def a (x):
    print 0
    print 1
"""
    Ast = parse(string)
    if Ast is None:
        logging.error('Ast is None :(')
    else:
        #print Ast
        for stmt in Ast.node.nodes:
            if stmt.lineno is not None:
                logging.error('#'+str(stmt.lineno), stmt)
            else:
                logging.error('#?', stmt)

