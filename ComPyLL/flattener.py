"""
.. module:: flattener
   :platform: Unix, OSX
   :synopsis: Flatten an AST.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

The flattener module accepts a valid AST and return ast similar but flattened AST from which it is possible to
generate valid LLVM code.
"""
import logging
import ast
from variable_generator import generate_label, generate_register, generate_variable
from constants import *

__all__ = ["flatten"]


def flatten(tree):
    """Flatten the statements in a module.

    Flattens the list fo expressions in all the statements of a module and return ast valid ast.Module instance.

    Args:
        tree (ast.Module): The module to flatten.

    Returns:
        ast.Module The module with flatten statements.

    """
    flattener = Flattener()
    return flattener.flatten(tree)


class Flattener(object):
    """
    The top level class, this class handle the flattening of modules directly.

    """
    def __init__(self):
        self.exp_flattener = ExpFlattener()
        self.stmt_flattener = StmtFlattener(self, self.exp_flattener)

    def flatten(self, tree):
        if isinstance(tree, ast.Module):
            tree.node = self.flatten(tree.node)
            tree.funcs = self.flatten(tree.funcs)
            return tree
        elif isinstance(tree, ast.Stmt):
            nodes = list()
            for node in tree.nodes:
                nodes += self.stmt_flattener.flatten(node)
            tree.nodes = nodes
            return tree
        elif tree is None:
            return ast.Stmt([])
        else:
            logging.error('Unrecognized! ' + repr(tree))
            exit(1)


class StmtFlattener(object):

    def __init__(self, flattener, exp_flattener):
        self._flattener = flattener
        self.exp_flattener = exp_flattener

    def flatten(self, tree):  # pylint: disable=C0103
        return getattr(self, tree.__class__.__name__)(tree)

    def Assign(self, assign):  # pylint: disable=C0103
        flat_var, flattened = self.exp_flattener.flatten(assign.exp)
        nodes_flat = []
        nodes_vars = []
        for i in range(len(assign.nodes)-1, -1, -1):
            node_var, node_flat = self.exp_flattener.flatten(assign.nodes[i])
            flattened.extend(node_flat)
            nodes_vars.append(node_var)

        flattened.append(ast.Assign([nodes_vars[0]], flat_var))
        for i in range(len(nodes_vars)-1):
            flattened.append(ast.Assign([nodes_vars[i+1]], self.exp_flattener._get_node_from_ass(nodes_vars[i])))
        return flattened

    def AugAssign(self, augassign):  # pylint: disable=C0103
        flat_var, flattened = self.exp_flattener.flatten(augassign.exp)
        name, flat_node = self.exp_flattener.flatten(augassign.node)
        flattened.extend(flat_node)

        node_type = None
        if augassign.opr == "+=":
            node_type = ast.Add
        elif augassign.opr == "-=":
            node_type = ast.Sub
        elif augassign.opr == "*=":
            node_type = ast.Mul
        elif augassign.opr == "/=":
            node_type = ast.Div
        elif augassign.opr == "//=":
            node_type = ast.FloorDiv
        elif augassign.opr == "%=":
            node_type = ast.Mod
        elif augassign.opr == "&=":
            node_type = ast.Bitand
        elif augassign.opr == "|=":
            node_type = ast.Bitor
        elif augassign.opr == "^=":
            node_type = ast.Bitxor
        elif augassign.opr == ">>=":
            node_type = ast.RightShift
        elif augassign.opr == "<<=":
            node_type = ast.LeftShift
        else:
            logging.error("Unrecognized operation!")
            exit(1)

        tmp = generate_register()
        flattened.append(ast.Assign([ast.AssRegister(tmp)], node_type(name, flat_var)))

        flattened.append(ast.Assign([self.exp_flattener._get_ass_from_node(augassign.node)], ast.Register(tmp), augassign.lineno))
        return flattened

    def Function(self, tree):  # pylint: disable=C0103
        tree.body = self._flattener.flatten(tree.body)
        if len(tree.body.nodes)==0 or type(tree.body.nodes[-1]) is not ast.Return:
            tree.body.nodes.append(ast.Return(ast.Const(NONE_VALUE)))  # add default return None
        return [tree]

    def Discard(self, discard):  # pylint: disable=C0103
        flat_var, flattened = self.exp_flattener.flatten(discard.exp)
        flattened.append(ast.Discard(flat_var, discard.lineno))
        return flattened

    def If(self, tree):  # pylint: disable=C0103
        if_var, if_flat = self.exp_flattener.flatten(tree.cond)
        if_body = self._flattener.flatten(tree.body)
        else_body = self._flattener.flatten(tree.other) if tree.other else None
        if_flat.append(ast.If(if_var, if_body, else_body, tree.lineno))
        return if_flat

    def Printnl(self, printnl):  # pylint: disable=C0103
        flat_var, flattened = self.exp_flattener.flatten(printnl.exp)
        flattened.append(ast.Printnl(flat_var, printnl.lineno))
        return flattened

    def Pass(self, tree):
        return []

    def Return(self, tree):
        flat_var, flattened = self.exp_flattener.flatten(tree.exp)
        tree.exp = flat_var
        flattened.append(tree)
        return flattened

    def While(self, tree):
        cond_var, cond_flat = self.exp_flattener.flatten(tree.cond)
        while_body = self._flattener.flatten(tree.body)
        while_flat = ast.While(ast.Stmt(cond_flat), while_body)
        while_flat.cond_var = cond_var
        return [while_flat]

    def MakeEnv(self, tree):
        return [tree]


class ExpFlattener(object):  # pylint: disable=R0904

    def __init__(self):
        pass

    def MakeClosure(self, tree):
        return tree, []

    def MakeObject(self, tree):
        return tree, []

    def flatten(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def String(self, string):
        return string, []

    def Add(self, add):  # pylint: disable=C0103
        return self._binary_node(add, ast.Add)

    def And(self, tree):  # pylint: disable=C0103
        left_var, left_flatten = self.flatten(tree.left)
        right_var, right_flatten = self.flatten(tree.right)

        negated_var = generate_register()
        left_flatten.append(ast.Assign([ast.AssRegister(negated_var)], ast.Not(left_var)))

        ret_var = generate_variable()
        right_flatten.append(ast.Assign([ast.AssName(ret_var)], right_var, tree.lineno))
        left_flatten.append(
            ast.If(
                ast.Register(negated_var),
                ast.Stmt([ast.Assign([ast.AssName(ret_var)], left_var, tree.lineno)], tree.lineno),
                ast.Stmt(right_flatten, tree.lineno)
            )
        )
        return ast.Name(ret_var), left_flatten

    def AssIndex(self, tree):
        tree.exp, exp_flat = self.flatten(tree.exp)
        tree.index, index_flat = self.flatten(tree.index)
        exp_flat.extend(index_flat)
        #ret_var = generate_register()
        #assign = ast.Assign([ast.AssRegister(ret_var)], self._get_node_from_ass(tree), tree.lineno)
        return ast.AssIndex(tree.exp, tree.index, tree.lineno), exp_flat

    def Splice(self, tree):
        if tree.left:
            tree.left, left_flatten = self.flatten(tree.left)
        else:
            left_flatten = []

        if tree.right:
            tree.right, right_flatten = self.flatten(tree.right)
        else:
            right_flatten = []

        exp_var, exp_flat = self.flatten(tree.exp)
        tree.exp = exp_var

        ret_var = generate_register()
        assign = ast.Assign([ast.AssRegister(ret_var)], tree, tree.lineno)
        return ast.Register(ret_var), left_flatten + right_flatten + exp_flat + [assign]

    def AssName(self, ass_name):
        return ass_name, []

    def AssObjRef(self, tree):
        return tree, []

    def Bitand(self, bit_and):  # pylint: disable=C0103
        return self._binary_node(bit_and, ast.Bitand)

    def Bitor(self, bit_or):  # pylint: disable=C0103
        return self._binary_node(bit_or, ast.Bitor)

    def Bitxor(self, bit_xor):  # pylint: disable=C0103
        return self._binary_node(bit_xor, ast.Bitxor)

    def CallFunc(self, func):  # pylint: disable=C0103
        args_var = []
        flattened = []
        for arg in func.args:
            arg_var, arg_flat = self.flatten(arg)
            args_var.append(arg_var)
            flattened.extend(arg_flat)

        ret_var = None
        if type(func.node) is ast.ObjRef:
            obj_var, obj_flat = self.flatten(func.node)
            args_var.insert(0, func.node.exp)
            func.node = obj_var
            flattened.extend(obj_flat)

        if type(func.node) in [ast.Name, ast.EnvRef, ast.ObjRef, ast.Register]:
            ret_var = generate_register()
            assign = ast.Assign([ast.AssRegister(ret_var)], ast.CallFunc(func.node, args_var))
            flattened.append(assign)

        elif type(func.node) in [ast.CallFunc, ast.MakeClosure]:
            func_var, func_flat = self.flatten(func.node)
            ret_var = generate_register()
            assign = ast.Assign([ast.AssRegister(ret_var)], ast.CallFunc(func_var, args_var))
            flattened.extend(func_flat)
            flattened.append(assign)

        else:
            logging.error('TypeError: ' + str(type(func.node)) + ' is not callable')
            exit(1)

        return ast.Register(ret_var), flattened

    def Const(self, const):  # pylint: disable=C0103
        return const, []

    def Div(self, div):  # pylint: disable=C0103
        return self._binary_node(div, ast.Div)

    def Equal(self, tree):  # pylint: disable=C0103
        return self._binary_node(tree, ast.Equal)

    def FloorDiv(self, floor_div):  # pylint: disable=C0103
        return self._binary_node(floor_div, ast.FloorDiv)

    def GreaterEqual(self, tree):  # pylint: disable=C0103
        return self._binary_node(tree, ast.GreaterEqual)

    def GreaterThan(self, tree):  # pylint: disable=C0103
        return self._binary_node(tree, ast.GreaterThan)

    def Index(self, tree):
        tree.exp, exp_flat = self.flatten(tree.exp)
        tree.index, index_flat = self.flatten(tree.index)
        exp_flat.extend(index_flat)
        ret_var = generate_register()
        assign = ast.Assign([ast.AssRegister(ret_var)], tree, tree.lineno)
        return ast.Register(ret_var), exp_flat + [assign]

    def Invert(self, invert):  # pylint: disable=C0103
        return self._unary_node(invert, ast.Invert)

    def LeftShift(self, left_shift):  # pylint: disable=C0103
        return self._binary_node(left_shift, ast.LeftShift)

    def LessEqual(self, tree):  # pylint: disable=C0103
        return self._binary_node(tree, ast.LessEqual)

    def LessThan(self, tree):  # pylint: disable=C0103
        return self._binary_node(tree, ast.LessThan)

    def List(self, tree):
        flattened = []
        flat_list = []
        for item in tree.items:
            var_item, flat_item = self.flatten(item)
            flattened.extend(flat_item)
            flat_list.append(var_item)
        tree.items = flat_list

        ret_var = generate_register()
        assign = ast.Assign([ast.AssRegister(ret_var)], tree, tree.lineno)
        flattened.append(assign)
        return ast.Register(ret_var), flattened

    def Mod(self, mod):  # pylint: disable=C0103
        return self._binary_node(mod, ast.Mod)

    def Mul(self, mul):  # pylint: disable=C0103
        return self._binary_node(mul, ast.Mul)

    def Name(self, name):  # pylint: disable=C0103
        return name, []

    def EnvRef(self, envref):  # pylint: disable=C0103
        return envref, []

    def ObjRef(self, objref):
        var, flat = self.flatten(objref.exp)
        objref.exp = var
        ret_var = generate_register()
        assign = ast.Assign([ast.AssRegister(ret_var)], objref)
        flat.append(assign)
        return ast.Register(ret_var), flat

    def Not(self, tree):
        return self._unary_node(tree, ast.Not)

    def NotEqual(self, tree):
        return self._binary_node(tree, ast.NotEqual)

    def Or(self, tree):
        left_var, left_flatten = self.flatten(tree.left)
        right_var, right_flatten = self.flatten(tree.right)

        ret_var = generate_variable()
        right_flatten.append(ast.Assign([ast.AssName(ret_var)], right_var, tree.lineno))
        left_flatten.append(
            ast.If(
                left_var,
                ast.Stmt([ast.Assign([ast.AssName(ret_var)], left_var, tree.lineno)], tree.lineno),
                ast.Stmt(right_flatten, tree.lineno)
            )
        )
        return ast.Name(ret_var), left_flatten

    def RightShift(self, right_shift):  # pylint: disable=C0103
        return self._binary_node(right_shift, ast.RightShift)

    def Sub(self, sub):  # pylint: disable=C0103
        return self._binary_node(sub, ast.Sub)

    def UnaryAdd(self, unary_add):  # pylint: disable=C0103
        return self._unary_node(unary_add, ast.UnaryAdd)

    def UnarySub(self, unary_sub):  # pylint: disable=C0103
        return self._unary_node(unary_sub, ast.UnarySub)

    def _binary_node(self, binary_node, node_type):
        left_var, left_flatten = self.flatten(binary_node.left)
        right_var, right_flatten = self.flatten(binary_node.right)
        result = node_type(left_var, right_var, binary_node.lineno)
        ret_var = generate_register()
        assign = ast.Assign([ast.AssRegister(ret_var)], result, binary_node.lineno)
        return ast.Register(ret_var), left_flatten + right_flatten + [assign]

    def _unary_node(self, unary_node, node_type):
        flat_var, flattened = self.flatten(unary_node.exp)
        if isinstance(flat_var, ast.Const):
            # Optimization for constants.
            if node_type is ast.UnarySub:
                ret_var = ast.Const(-flat_var.value)
            elif node_type is ast.UnaryAdd:
                ret_var = ast.Const(flat_var.value)
            elif node_type is ast.Not:
                ret_var = ast.Const(1 if flat_var.value == 0 else 0)
            else:
                ret_var = node_type(flat_var)
            return ret_var, flattened

        u_node = node_type(flat_var)
        ret_var = generate_register()
        assign = ast.Assign([ast.AssRegister(ret_var)], u_node, unary_node.lineno)
        flattened.append(assign)
        return ast.Register(ret_var), flattened

    def _get_node_from_ass(self, node):
        if type(node) is ast.AssName:
            return ast.Name(node.name, node.lineno)
        elif type(node) is ast.AssIndex:
            return ast.Index(node.exp, node.index, node.lineno)
        elif type(node) is ast.AssObjRef:
            return ast.ObjRef(node.exp, node.name, node.lineno)
        elif type(node) is ast.AssRegister:
            return ast.Register(node.name, node.lineno)
        else:
            logging.error("SyntaxError: Assign to invalid variable {}".format(node))
            exit(1)

    def _get_ass_from_node(self, node):
        if type(node) is ast.Name:
            return ast.AssName(node.name, node.lineno)
        elif type(node) is ast.Index:
            return ast.AssIndex(node.exp, node.index, node.lineno)
        elif type(node) is ast.ObjRef:
            return ast.AssObjRef(node.exp, node.name, node.lineno)
        elif type(node) is ast.Register:
            return ast.AssRegister(node.name, node.lineno)
        else:
            logging.error("SyntaxError: Assign to invalid variable {}".format(node))
            exit(1)
