#include <stdio.h>

int hashmap_hash(char * key) {
    unsigned long hash = 0;
    int c;

    while ((c = *key++)) {
    	hash = hash ^ c ^ (c>>2);
    }
    return hash;
}

int table_size = 4;
int count[] = {0, 0, 0, 0};

void print_hash(char * key)
{
	int hash = hashmap_hash(key) & (table_size-1);
	// printf("%d mod %d = %d\n", hashmap_hash(key), table_size, hash);
	// printf("%s -> %x\n", key, hash);
	count[hash]++;
}

void print_stats() {
	int max = 0;
	int min = count[0];
	for (int i = 0; i < table_size; ++i) {
		if (max < count[i]) max = count[i];
		if (min > count[i]) min = count[i];
		printf("%d -> %d\n", i, count[i]);
	}
	printf("Max: %d, Min: %d, Range: %d\n", max, min, max - min);
}

int main(int argc, char const *argv[])
{
	print_hash("hello");
	print_hash("x");
	print_hash("y");
	print_hash("a");
	print_hash("b");
	print_hash("c");
	print_hash("d");
	print_hash("asd");
	print_hash("makeup");
	print_hash("dens");
	print_hash("sutler");
	print_hash("dynasty");
	print_hash("unowing");
	print_hash("mlr");
	print_hash("bernice");
	print_hash("gael");
	print_hash("muttra");
	print_hash("valvate");
	print_hash("humeri");
	print_hash("coft");
	print_hash("peloric");
	print_hash("ozs");
	print_hash("deathy");
	print_hash("loosest");
	print_hash("sedile");
	print_hash("hulking");
	print_hash("dares");
	print_hash("retack");
	print_hash("placate");
	print_hash("rimose");
	print_hash("sweet");
	print_hash("dimply");
	print_hash("gambir");
	print_hash("garth");
	print_hash("gala");
	print_hash("salta");
	print_hash("wilfrid");
	print_hash("firn");
	print_hash("ensoul");
	print_hash("garnett");
	print_hash("tressed");
	print_hash("borshch");
	print_hash("faustus");
	print_hash("eild");
	print_hash("quahaug");
	print_hash("gomberg");
	print_hash("rotate");
	print_hash("phasis");
	print_hash("pisay");
	print_hash("blether");
	print_hash("ironer");
	print_hash("ago");
	print_hash("palest");
	print_hash("barrs");
	print_hash("dunoon");
	print_hash("tuxedo");
	print_hash("lungi");
	print_hash("rcmp");
	print_hash("reverso");
	print_hash("beano");
	print_hash("riata");
	print_hash("reggy");
	print_hash("wind");
	print_hash("ischia");
	print_hash("megger");
	print_hash("nayarit");
	print_hash("dimming");
	print_hash("sulu");
	print_hash("excudit");
	print_hash("dormate");
	print_hash("rowted");
	print_hash("dwell");
	print_hash("waling");
	print_hash("preyer");
	print_hash("pelike");
	print_hash("solvate");
	print_hash("caid");
	print_hash("eager");
	print_hash("mooring");
	print_hash("winch");
	print_hash("itemize");
	print_hash("cattier");
	print_hash("roe");
	print_hash("sambre");
	print_hash("conchy");
	print_hash("unfrost");
	print_hash("ripper");
	print_hash("arsino");
	print_hash("loyal");
	print_hash("fremont");
	print_hash("daly");
	print_hash("hath");
	print_hash("maduro");
	print_hash("ricotta");
	print_hash("zest");
	print_hash("ismaili");
	print_hash("moper");
	print_hash("cager");
	print_hash("bedless");
	print_hash("sudeten");
	print_hash("fullom");
	print_hash("jackies");
	print_hash("landing");
	print_hash("goblet");
	print_hash("gnostic");
	print_hash("marron");

	print_stats();
	return 0;
}