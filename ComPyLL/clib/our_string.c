#include <string.h>
#include <gc.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "our_string.h"

#include "tags.h"
#include "init.h"


long get_string_pointer(char * const string) {
    init();
    char* pointer  = GC_MALLOC(sizeof(string)*strlen(string));
    strcpy(pointer, string);
    long tagged = (long) pointer;
    ensure_aligned(tagged);
    return tagged + TAG_STRING;
}


long tcapitalize(void * unused, long pointer) {
    ensure_tagged(pointer, TAG_STRING);
    char * string =(char*) untag(pointer);
    char * newpointer  = GC_MALLOC(sizeof(string)*strlen(string));
    strcpy(newpointer, string);
    *newpointer = toupper((unsigned char)*newpointer);
    //   fprintf(stderr,"%s\n", newpointer);
    long tagged = (long) newpointer;
    ensure_aligned(tagged);
    return tagged + TAG_STRING;
}

long check_lower_case(void * unused, long pointer) {
    ensure_tagged(pointer, TAG_STRING);
    char * string =(char*) untag(pointer);
    while (*string !='\0') {
        if(isupper(*string++)) {
            return TAG_FALSE_VALUE;
        }
    }
    return TAG_TRUE_VALUE;
}

long check_upper_case(void * unused, long pointer) {
    ensure_tagged(pointer, TAG_STRING);
    char * string = (char*) untag(pointer);
    while (*string != '\0') {
        if (islower(*string++)) {
            return TAG_FALSE_VALUE;
        }
    }
    return TAG_TRUE_VALUE;
}

long lower_case(void * unused, long pointer) {
    ensure_tagged(pointer, TAG_STRING);
    char * string = (char*) untag(pointer);
    char * newpointer  = GC_MALLOC(sizeof(string) * strlen(string));
    strcpy(newpointer, string);
    char *tmp = newpointer;
    while (*tmp != '\0') {
        *tmp = tolower(*tmp);
        tmp++;
    }
    long tagged = (long) newpointer;
    ensure_aligned(tagged);
    return tagged + TAG_STRING;
}


long upper_case(void * unused, long pointer) {
    ensure_tagged(pointer, TAG_STRING);
    char * string = (char*) untag(pointer);
    char * newpointer  = GC_MALLOC(sizeof(string)*strlen(string));
    strcpy(newpointer, string);
    char *tmp = newpointer;
    while (*tmp != '\0') {
        *tmp = toupper(*tmp);
        tmp++;
    }
    long tagged = (long) newpointer;
    ensure_aligned(tagged);
    return tagged + TAG_STRING;
}

long index_string(void *unused, long tagged_string, long tagged_index) {
    ensure_tagged(tagged_string, TAG_STRING);
    ensure_tagged(tagged_index, TAG_INT);
    char * string = (char*) untag(tagged_string);
    long index = untag(tagged_index);
    char * newpointer  = GC_MALLOC(sizeof(char *));
    if(!((int)strlen(string)>index && index >= -(int)strlen(string))) {
        fprintf(stderr, "%s\n", "ERROR: Index out of range");
        exit(1);
    }
    if(index < 0)
        index = ((int)strlen(string))+index;
    newpointer[0] = string[index];
    long tagged = (long)newpointer;
    ensure_aligned(tagged);
    return tagged + TAG_STRING;
}


long starts_with(void * unused, long left_pointer, long right_pointer) {
    ensure_tagged(left_pointer, TAG_STRING);
    ensure_tagged(right_pointer, TAG_STRING);
    char * left_string = (char*) untag(left_pointer);
    char * right_string = (char*) untag(right_pointer);
    int left_len = strlen(left_string);
    int right_len = strlen(right_string);
    return left_len < right_len ? TAG_FALSE_VALUE : strncmp(left_string, right_string, right_len) == 0 ? TAG_TRUE_VALUE: TAG_FALSE_VALUE;
}

long ends_with(void * unused, long left_pointer, long right_pointer) {
    ensure_tagged(left_pointer, TAG_STRING);
    ensure_tagged(right_pointer, TAG_STRING);
    char * left_string = (char*) untag(left_pointer);
    char * right_string = (char*) untag(right_pointer);
    int left_len = strlen(left_string);
    int right_len = strlen(right_string);
    left_string += (left_len - right_len);
    return left_len < right_len ? TAG_FALSE_VALUE : strncmp(left_string, right_string, right_len) == 0 ? TAG_TRUE_VALUE: TAG_FALSE_VALUE;
}

long tstring_mul(void* unused, long string, long factor) {
    ensure_tagged(string, TAG_STRING);
    int tag = factor & TAG_MASK;
    if (!(tag == TAG_INT || tag == TAG_BOOL)) {
        fprintf(stderr, "ERROR: Incompatible type 'str' and %s!\n",
            tag_to_string(tag));
        exit(1);
    }
    return string_mul((char*)untag(string), untag(factor));
}
long string_mul(char* string, long factor) {
    factor = MAX(factor, 0);
    char* new_string = GC_MALLOC(sizeof(string)*(strlen(string)*factor));
    for(long i =0; i < (factor); ++i) {
        strcat(new_string, string);
    }
    long pointer = (long) new_string;
    return tag(pointer, TAG_STRING);
}

long tstring_concat(void *unused, long string_a, long string_b) {
    ensure_tagged(string_a, TAG_STRING);
    ensure_tagged(string_b, TAG_STRING);
    return string_concat((char*) untag(string_a), (char*) untag(string_b));
}
long string_concat(char* string_a, char* string_b) {
    char* new_string = GC_MALLOC(sizeof(string_a)*(strlen(string_a)+strlen(string_b)));
    strcpy(new_string, string_a);
    strcat(new_string, string_b);
    return tag((long)new_string, TAG_STRING);
}

long tstring_get_size(void * unused, long string) {
    ensure_tagged(string, TAG_STRING);
    return string_get_size((char*)untag(string));
}
long string_get_size(char* string) {
    return tag(strlen(string), TAG_INT);
}

long tstring_get_attr(long str, char * attr) {
    static closure_t getitem = {index_string,NULL, 2};
    static closure_t concat = {tstring_concat,NULL, 2};
    static closure_t mul = {tstring_mul,NULL, 2};
    static closure_t len = {tstring_get_size,NULL, 1};
    static closure_t capitalize = {tcapitalize,NULL, 1};
    static closure_t islower= {check_lower_case,NULL, 1};
    static closure_t isupper = {check_upper_case,NULL, 1};
    static closure_t lower = {lower_case,NULL, 1};
    static closure_t upper = {upper_case,NULL, 1};
    static closure_t startswith= {starts_with,NULL, 2};
    static closure_t endswith = {ends_with,NULL, 2};
    static closure_t index = {index_string,NULL, 2};
    if(strcmp("__getitem__", attr) == 0) {
        return ((long) (&getitem))+TAG_FUNCTION;
    } else if(strcmp("__add__", attr) == 0) {
        return ((long) (&concat))+TAG_FUNCTION;
    } else if(strcmp("__mul__", attr) == 0) {
        return ((long) (&mul))+TAG_FUNCTION;
    } else if(strcmp("__len__", attr) == 0) {
        return ((long) (&len))+TAG_FUNCTION;
    } else if(strcmp("islower", attr) == 0) {
        return ((long) (&islower))+TAG_FUNCTION;
    } else if(strcmp("capitalize", attr) == 0) {
        return ((long) (&capitalize))+TAG_FUNCTION;
    } else if(strcmp("isupper", attr) == 0) {
        return ((long) (&isupper))+TAG_FUNCTION;
    } else if(strcmp("lower", attr) == 0) {
        return ((long) (&lower))+TAG_FUNCTION;
    } else if(strcmp("upper", attr) == 0) {
        return ((long) (&upper))+TAG_FUNCTION;
    } else if(strcmp("startswith", attr) == 0) {
        return ((long) (&startswith))+TAG_FUNCTION;
    } else if(strcmp("endswith", attr) == 0) {
        return ((long) (&endswith))+TAG_FUNCTION;
    } else if(strcmp("index", attr) == 0) {
        return ((long) (&index))+TAG_FUNCTION;
    } else {
        return TAG_NONE_VALUE;
    }
}
