#include <stdio.h>
#include <stdlib.h>
#include <gc.h>

#include "init.h"

#include "tags.h"

void init(void) {
    // Initialize the garbage collector
    static int initialized = 0;
    if (!initialized) {
        GC_INIT();
        ++initialized;
    }
}

long make_closure(void * func, long nparams) {
    init();
    closure_t * closure = GC_MALLOC(sizeof(closure_t));
    closure->function = func;
    closure->environment = hashmap_create(8);
    closure->nparams = nparams;
    long tagged = (long) closure;
    ensure_aligned(tagged);
    return tagged + TAG_FUNCTION;
}


long make_object(void) {
    init();
    hashmap_t * map = hashmap_create(8);
    long tagged = (long) map;
    ensure_aligned(tagged);
    return tagged + TAG_OBJECT;
}
