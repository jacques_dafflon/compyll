#include "tags.h"

#include "init.h"
#include <stdio.h>
#include <stdlib.h>

// Returns the tag
int get_tag(long x) { 
    return x & TAG_MASK; 
}

// Returns the tagged value
long tag(long value, int tag) {
    if (tag > 2) {
        return value | tag;
    } else {
        return value << TAG_SIZE;
    }
}

// Return a long with the untagged value
long untag(long tagged) {
    if (get_tag(tagged) > 2) {
        return tagged & VALUE_MASK;
    } else {
        return tagged >> TAG_SIZE;
    }
}

// Fails if the tag does not match
void ensure_tagged(long ptr, int tag) {
    if ((ptr & TAG_MASK) != tag) {
        fprintf(stderr, "ERROR: Incompatible type!\n\tExpected %s, got %s\n",
            tag_to_string(tag), tag_to_string(ptr & TAG_MASK));
        exit(1);
    }
    return;
}

long untag_or_null(long ptr, int tag, long null_val) {
    long value = untag(ptr);
    if ((ptr & TAG_MASK ) == TAG_FUNCTION && value == 0) {
        return null_val;
    }
    ensure_tagged(ptr, tag);
    return value;
}

// Fails if the tag is not a function tag
void assert_tag_func(long x, long nparams) {
    switch (x & TAG_MASK) {
        case TAG_INT:
            fprintf(stderr, "ERROR: 'int' object is not callable!\n");
            exit(1);
        case TAG_BOOL:
            fprintf(stderr, "ERROR: 'bool' object is not callable!\n");
            exit(1);
        case TAG_UNDEFINED:
            fprintf(stderr, "ERROR: Undefined variable referenced!\n");
            exit(1);
        case TAG_OBJECT:
            fprintf(stderr, "ERROR: object is not callable!\n");
            exit(1);
        case TAG_STRING:
            fprintf(stderr, "ERROR: 'str' object is not callable!\n");
            exit(1);
        case TAG_LIST:
            fprintf(stderr, "ERROR: 'list' object is not callable!\n");
            exit(1);
    }

    closure_t * closure = (closure_t*) untag(x);
    if (closure->nparams != nparams) {
        fprintf(stderr, "ERROR: function takes exactly %ld argument(s) (%ld given)\n", closure->nparams, nparams);
        exit(1);
    }
}

// Fails if the tag is TAG_UNDEFINED
void ensure_defined(long x) {
    if ((x & TAG_MASK) == TAG_UNDEFINED) {
        error_undefined_variable();
    }
}

// Fails if the last bits are not 0
void ensure_aligned(long x) {
    if ((x & TAG_MASK) != 0) {
        fprintf(stderr, "ERROR: pointer %ld not aligned!\n", x);
    }
}

// Prints a message and fails
void error_undefined_variable() {
    fprintf(stderr, "ERROR: Undefined variable referenced!\n");
    exit(1);
}

// Returns a string representation of the given tag
char * tag_to_string(int tag) {
    switch (tag) {
        case 0: return "'int'";
        case 1: return "'bool'";
        case 2: return "'undeclared'";
        case 3: return "'function'";
        case 4: return "'object'";
        case 5: return "'str'";
        case 6: return "'list'";
        default: return "<type error>";
    }
}
