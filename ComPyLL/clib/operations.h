#ifndef OPERATIONS_H
#define OPERATIONS_H

long len(long pointer);
long add(long left, long right);
long sub(long left, long right);
long mul(long left, long right);
long _div(long left, long right);
long floordiv(long left, long right);
long mod(long left, long right);
long leftshift(long left, long right);
long rightshift(long left, long right);
long bitand(long left, long right);
long bitor(long left, long right);
long bitxor(long left, long right);
long equal(long left, long right);
long lesserqual(long left, long right);
long greaterequal(long left, long right);
long notequal(long left, long right);
long lessthan(long left, long right);
long greaterthan(long left, long right);
long not(long exp);

#endif // OPERATIONS_H
