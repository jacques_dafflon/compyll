#ifndef LIST_H
#define LIST_H

#include <gc.h>
#include <stdbool.h>
#include "init.h"

typedef struct list_entry list_entry;

// List struct, contains the length of the list (size), space allocated and the
// array containing the elements of the list
typedef struct _list_t {
	long allocated;			// amount of space allocated for elements
	long size;				// value returned by len() function
	long* items;		// array pointer (list of values as longs)
} list_t;

// Create a list and return it as a tagged pointer.
long list_create(long allocate);

// Resize the list.
void list_resize(list_t* list, const long new_size);

// Get the number of elements in the list.
// Returns the value as a tagged integer (long).
long tlist_get_size(void * unused, long list);
long list_get_size(list_t* list);

// Get the element at the given index
// Error if the index is out of range.
long tlist_get_elem(void * unused, const long list, long index);
long list_get_elem(list_t* list, long index);

// Set the given element at the given index
// Error if the index is out of range.
long tlist_set_elem(void * unused, const long list, long index, long elem);
long list_set_elem(list_t* list, long index, long elem);

// Return a concatenation of two lists.
long tlist_concat(void * unused, const long list_a, long list_b);
long list_concat(list_t* list_a, list_t* list_b);

// Return a slice of a list as a new list
long tlist_get_splice(void * unused, const long list, long lo, long hi);
long list_get_splice(list_t* list, long lo, long hi);

// Append an element ot the end of the list.
long tlist_append(void * unused, const long list, const long elem);
long list_append(list_t* list, const long elem);

// Extend the list with the content of another list.
long tlist_extend(void * unused, const long list, const long extension);
long list_extend(list_t* list, list_t* extension);

// Insert an element in the list at a specifc location.
long tlist_insert(void * unused, const long list, long index, const long elem);
long list_insert(list_t* list, long index, const long elem);

// Remove the first occurence of the element from the list.
// Error if the element is not in the list.
long tlist_remove(void * unused, const long list, const long elem);
long list_remove(list_t* list, const long elem);

// Remove the element at the given index from the list and return it.
long tlist_pop(void * unused, const long list, const long index);
long list_pop(list_t* list, const long index);

// Get the index of the first occurence of the element in the list.
// Error if the element is not in the list.
long tlist_index(void * unused, const long list, const long elem);
long list_index(list_t* list, const long elem);

// Count the number of occurences an element is in the list.
long tlist_count(void * unused, const long list, const long elem);
long list_count(list_t* list, const long elem);

// Reverse the elements of the list in place
long tlist_reverse(void * unused, const long list);
long list_reverse(list_t* list);

// Return true or false wether the lists are equal based on lexicographic order.
long tlist_compare(
	void * unused,
	long list_a,
	long list_b,
	long cmp(long left, long right)
);
long list_compare(
	list_t* list_a,
	list_t* list_b,
	long cmp(long left, long right)
);

// Return a concatenation of factor times list.
long tlist_mul(void* unused, long list, long factor);
long list_mul(list_t* list, long factor);

//Get closure with the attribute
long tlist_get_attr(long list, char * attr);

#endif // LIST_H