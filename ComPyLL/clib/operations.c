#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gc.h>

#include "operations.h"

#include "tags.h"
#include "init.h"
#include "list.h"
#include "our_string.h"

int error_unsupported_binary(int left_tag, int right_tag, char * operand) {
    fprintf(stderr, "ERROR: Unsupported operand '%s' for type '%s' and '%s'\n", operand, tag_to_string(left_tag), tag_to_string(right_tag));
    exit(1);
    return 0;
}

int error_unsupported_unary(int exp_tag, char * operand) {
    fprintf(stderr, "ERROR: Unsupported operand '%s' for type '%s'\n", operand, tag_to_string(exp_tag));
    exit(1);
    return 0;
}

long add(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) + untag(right), TAG_INT);
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_concat(NULL, left, right);
    }

    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        return tstring_concat(NULL, left, right);
    }

    return error_unsupported_binary(left_tag, right_tag, "+");
}

long sub(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) - untag(right), TAG_INT);
    }

    return error_unsupported_binary(left_tag, right_tag, "-");
}

long mul(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) * untag(right), TAG_INT);
    }

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL)
            && right_tag == TAG_STRING) {
        return tstring_mul(NULL, right, left);
    }

    if (left_tag == TAG_STRING
            && (right_tag == TAG_INT || right_tag == TAG_BOOL)) {
        return tstring_mul(NULL, left, right);
    }

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL)
            && right_tag == TAG_LIST) {
        return tlist_mul(NULL, right, left);
    }

    if (left_tag == TAG_LIST
            && (right_tag == TAG_INT || right_tag == TAG_BOOL)) {
        return tlist_mul(NULL, left, right);
    }

    return error_unsupported_binary(left_tag, right_tag, "*");
}

long _div(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) / untag(right), TAG_INT);
    }

    return error_unsupported_binary(left_tag, right_tag, "/");
}

long floordiv(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) / untag(right), TAG_INT);
    }

    return error_unsupported_binary(left_tag, right_tag, "//");
}

long mod(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) % untag(right), TAG_INT);
    }

    return error_unsupported_binary(left_tag, right_tag, "%");
}

long leftshift(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) << untag(right), TAG_INT);
    }

    return error_unsupported_binary(left_tag, right_tag, "<<");
}

long rightshift(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return tag(untag(left) >> untag(right), TAG_INT);
    }

    return error_unsupported_binary(left_tag, right_tag, ">>");
}

long bitand(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return left & right;
    }

    return error_unsupported_binary(left_tag, right_tag, "&");
}

long bitor(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {
        return ((left | right) & VALUE_MASK) | (left_tag & right_tag);
    }

    return error_unsupported_binary(left_tag, right_tag, "|");
}

long bitxor(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return ((left ^ right) & VALUE_MASK) | (left_tag & right_tag);
    }

    return error_unsupported_binary(left_tag, right_tag, "^");
}

long equal(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {
        return (untag(left) == untag(right)) << TAG_SIZE | TAG_BOOL ;
    }

    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        char* leftString = (char*)left;
        leftString-=TAG_STRING;
        char* rightString = (char*)right;
        rightString-=TAG_STRING;
        int result = strcmp(leftString, rightString)==0;
        long pointer = (long)result;
        return (pointer<<TAG_SIZE)+TAG_BOOL;
    }

    if (left_tag == TAG_STRING && right_tag == (TAG_INT | TAG_BOOL)) {
        return TAG_FALSE_VALUE;
    }

    if (left_tag == (TAG_INT | TAG_BOOL) && right_tag == TAG_STRING) {
        return TAG_FALSE_VALUE;
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_compare(NULL, left, right, equal);
    }

    return (left == right) << TAG_SIZE | TAG_BOOL;
}

long lesserequal(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return (untag(left) <= untag(right)) << TAG_SIZE | TAG_BOOL ;
    }


    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        char* leftString = (char*)left;
        leftString-=TAG_STRING;
        char* rightString = (char*)right;
        rightString-=TAG_STRING;
        int result = strcmp(leftString, rightString)<=0;
        long pointer =  (long)result;
        return (pointer<<TAG_SIZE)+TAG_BOOL;
    }

    if (left_tag == TAG_STRING && right_tag == (TAG_INT | TAG_BOOL)) {
        return TAG_FALSE_VALUE;
    }

    if (left_tag == (TAG_INT | TAG_BOOL) && right_tag == TAG_STRING) {
        return TAG_TRUE_VALUE;
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_compare(NULL, left, right, lesserequal);
    }

    return (left <= right) << TAG_SIZE | TAG_BOOL;
}

long greaterequal(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return (untag(left) >= untag(right)) << TAG_SIZE | TAG_BOOL ;
    }

    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        char* leftString = (char*)left;
        leftString-=TAG_STRING;
        char* rightString = (char*)right;
        rightString-=TAG_STRING;
        int result = strcmp(leftString, rightString)>=0;
        long pointer =  (long)result;
        return (pointer<<TAG_SIZE)+TAG_BOOL;
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_compare(NULL, left, right, greaterequal);
    }

    if (left_tag == TAG_STRING && right_tag == (TAG_INT | TAG_BOOL)) {
        return TAG_TRUE_VALUE;
    }

    if (left_tag == (TAG_INT | TAG_BOOL) && right_tag == TAG_STRING) {
        return TAG_FALSE_VALUE;
    }

    return (left >= right) << TAG_SIZE | TAG_BOOL;
}

long notequal(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return (untag(left) != untag(right)) << TAG_SIZE | TAG_BOOL ;
    }

    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        char* leftString = (char*)left;
        leftString-=TAG_STRING;
        char* rightString = (char*)right;
        rightString-=TAG_STRING;
        int result = strcmp(leftString, rightString)!=0;
        long pointer =  (long)result;
        return (pointer<<TAG_SIZE)+TAG_BOOL;
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_compare(NULL, left, right, notequal);
    }

    if (left_tag == TAG_STRING && right_tag == (TAG_INT | TAG_BOOL)) {
        return TAG_TRUE_VALUE;
    }

    if (left_tag == (TAG_INT | TAG_BOOL) && right_tag == TAG_STRING) {
        return TAG_TRUE_VALUE;
    }

    return (left != right) << TAG_SIZE | TAG_BOOL;;
}

long lessthan(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return (untag(left) < untag(right)) << TAG_SIZE | TAG_BOOL ;
    }

    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        char* leftString = (char*)left;
        leftString-=TAG_STRING;
        char* rightString = (char*)right;
        rightString-=TAG_STRING;
        int result = strcmp(leftString, rightString)<0;
        long pointer =  (long)result;
        return (pointer<<TAG_SIZE)+TAG_BOOL;
    }

    if (left_tag == TAG_STRING && right_tag == (TAG_INT | TAG_BOOL)) {
        return TAG_FALSE_VALUE;
    }

    if (left_tag == (TAG_INT | TAG_BOOL) && right_tag == TAG_STRING) {
        return TAG_TRUE_VALUE;
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_compare(NULL, left, right, lessthan);
    }


    return (left < right) << TAG_SIZE | TAG_BOOL;;
}

long greaterthan(long left, long right) {
    ensure_defined(left);
    ensure_defined(right);
    int left_tag = get_tag(left);
    int right_tag = get_tag(right);

    if ((left_tag == TAG_INT || left_tag == TAG_BOOL) &&
        (right_tag == TAG_INT || right_tag == TAG_BOOL)) {

        return (untag(left) > untag(right)) << TAG_SIZE | TAG_BOOL ;
    }

    if (left_tag == TAG_STRING && right_tag == TAG_STRING) {
        char* leftString = (char*)left;
        leftString -= TAG_STRING;
        char* rightString = (char*)right;
        rightString -= TAG_STRING;
        int result = strcmp(leftString, rightString) > 0;
        long pointer = (long)result;
        return (pointer << TAG_SIZE) + TAG_BOOL;
    }

    if (left_tag == TAG_STRING && right_tag == (TAG_INT | TAG_BOOL)) {
        return TAG_TRUE_VALUE;
    }

    if (left_tag == (TAG_INT | TAG_BOOL) && right_tag == TAG_STRING) {
        return TAG_FALSE_VALUE;
    }

    if (left_tag == TAG_LIST && right_tag == TAG_LIST) {
        return tlist_compare(NULL, left, right, greaterthan);
    }

    return (left > right) << TAG_SIZE | TAG_BOOL;
}

long not(long exp) {
    ensure_defined(exp);

    return (exp >> TAG_SIZE) ? TAG_FALSE_VALUE : TAG_TRUE_VALUE;
}
