#ifndef INITI_H
#define INITI_H

#include "hashmap.h"

#define MAX(a,b) \
   	({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define MIN(a,b) \
   	({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

void init(void);

typedef struct {
    void * function;
    hashmap_t * environment;
    long nparams;
} closure_t;

long make_closure(void * func, long nparams);
long make_object(void);

#endif // INITI_H
