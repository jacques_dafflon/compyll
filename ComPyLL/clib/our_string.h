#ifndef OUR_STRING_H
#define OUR_STRING_H
long get_string_pointer(char * const string);
long tcapitalize(void * unused, long pointer);
long check_lower_case(void * unused, long pointer);
long check_upper_case(void *unused, long pointer);
long lower_case(void * unused, long pointer);
long upper_case(void * unused, long pointer);
long starts_with(void * unused, long left_pointer, long right_pointer);
long ends_with(void * unused, long left_pointer, long right_pointer);
long index_string(void * unused, long left_pointer, long right_pointer);
long tstring_get_attr(long str, char * attr);
long tstring_mul(void* unused, long string, long factor);
long string_mul(char* string, long factor);
long tstring_concat(void *unused, long string_a, long string_b);
long string_concat(char* string_a, char* string_b);
long tstring_get_size(void * unused, long string);
long string_get_size(char* string);
#endif // OUR_STRING_H
