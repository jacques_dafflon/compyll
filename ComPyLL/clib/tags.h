#ifndef TAGS_H
#define TAGS_H

#define TAG_SIZE 3
#define TAG_MASK 7
#define VALUE_MASK -8

#define TAG_INT 0
#define TAG_BOOL 1
#define TAG_UNDEFINED 2
#define TAG_FUNCTION 3
#define TAG_OBJECT 4
#define TAG_STRING 5
#define TAG_LIST 6
// #define TAG_??? 7

#define TAG_TRUE_VALUE 9
#define TAG_FALSE_VALUE 1
#define TAG_NONE_VALUE 4

// Returns the tag
int get_tag(long x);

// Return the untagged value
long untag(long tagged);

// Returns the tagged value
long tag(long value, int tag);

// Fails if the tag does not match
void ensure_tagged(long ptr, int tag);

// Return untagged value or null_val if ptr is tagged Null.
long untag_or_null(long ptr, int tag, long null_val);

// Fails if the tag is not a function tag
void assert_tag_func(long x, long nparams);

// Fails if the tag is TAG_UNDECLARED
void ensure_defined(long x);

// Fails if the last bits are not 0
void ensure_aligned(long x);

// Prints a message and fails
void error_undefined_variable();

// Returns a string representation of the given tag
char * tag_to_string(int tag);

#endif
