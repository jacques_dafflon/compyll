#include <stdio.h>
#include <stdlib.h>
#include <gc.h>
#include <string.h>

#include "list.h"

#include "tags.h"
#include "init.h"
#include "operations.h"

#define L_DEBUG 0 // Disable := 0 | Enable := 1
#define PY_LIST_SIZE_MAX 42000

list_t* _list_create(const long allocate) {
	if (allocate < 0) {
        fprintf(stderr, "ERROR: Size of the list must be greater than zero!\n");
        exit(1);
    }
    init();
    list_t * list = GC_MALLOC(sizeof(list_t));
    list->size = 0;
    list->items = GC_MALLOC(allocate * sizeof(long));

    return list;
}

long list_create(const long allocate) {
    long tagged = (long) _list_create(allocate);
    ensure_aligned(tagged);
    return tagged + TAG_LIST;
}

void list_resize(list_t* list, const long new_size) {
	// printf("Resize %ld -> %ld (+%ld)\n", list->size, new_size, new_size-list->size);
	/* Bypass realloc() when a previous overallocation is large enough
       to accommodate the new_size.  If the new_size falls lower than half
       the allocated size, then proceed with the realloc() to shrink the list.
       From CPython's list implementation.
    */
    if (list->allocated >= new_size && new_size >= (list->allocated >> 1)) {
        return;
    }

    /* This over-allocates proportional to the list size, making room
     * for additional growth.  The over-allocation is mild, but is
     * enough to give linear-time amortized behavior over a long
     * sequence of appends() in the presence of a poorly-performing
     * system realloc().
     * The growth pattern is:  0, 4, 8, 16, 25, 35, 46, 58, 72, 88, ...
     */
    long new_allocated = (new_size >> 3) + (new_size < 9 ? 3 : 6);

    /* check for integer overflow */
    if (new_allocated > PY_LIST_SIZE_MAX - new_size) {
    	if (PY_LIST_SIZE_MAX > new_size) {
    		printf("Cant alloc to %ld extend to max: %d\n", new_allocated+new_size, PY_LIST_SIZE_MAX);
    		new_allocated = PY_LIST_SIZE_MAX;
    	} else {
    	printf("Cant alloc to %ld (%ld + %ld) > %d\n", new_allocated+new_size, new_size, new_allocated, PY_LIST_SIZE_MAX);
        fprintf(stderr, "ERROR: Out of memory!\n");
        exit(1);
    	}
    } else {
        new_allocated += new_size;
    }

    if (new_size == 0) {
        new_allocated = 0;
    }

    if (new_allocated < list->size) {
    	return;
    }

    list->items = GC_REALLOC(list->items, new_allocated * sizeof(long));
    list->allocated = new_allocated;
}

long tlist_get_size(void * unused, long list) {
	ensure_tagged(list, TAG_LIST);
	return list_get_size((list_t*) untag(list));
}
long list_get_size(list_t* list) {
	long tagged = (long) list->size;
	return tagged << TAG_SIZE;
}

long tlist_get_elem(void * unused, const long list, long index) {
	ensure_tagged(list, TAG_LIST);
	ensure_tagged(index, TAG_INT);
	return list_get_elem((list_t*) untag(list), untag(index));
}
long list_get_elem(list_t* list, long index) {
	if (index >= list->size) {
		fprintf(stderr, "ERROR: Index %ld out of range! (%ld)\n", index,
			list->size);
		exit(0);
	}
	if (index < 0) {
		index += list->size;
		if (index < 0) {
			fprintf(stderr, "ERROR: Index %ld out of range! (%ld)\n",
				index - list->size, list->size);
			exit(0);
		}
	}
	long tagged = list->items[index];
	return tagged;

}

long tlist_set_elem(void * unused, const long list, long index, long elem) {
	ensure_tagged(list, TAG_LIST);
	ensure_tagged(index, TAG_INT);
	return list_set_elem((list_t*) untag(list), untag(index), elem);
}
long list_set_elem(list_t* list, long index, long elem) {
	if (index >= list->size) {
		fprintf(stderr, "ERROR: Index %ld out of range! (%ld)\n", index,
			list->size);
		exit(0);
	}
	if (index < 0) {
		index += list->size;
		if (index < 0) {
			fprintf(stderr, "ERROR: Index %ld out of range! (%ld)\n",
				index - list->size, list->size);
		exit(0);
		}
	}
	list->items[index] = elem;
	return TAG_NONE_VALUE;
}

long tlist_concat(void * unused, const long list_a, long list_b) {
	ensure_tagged(list_a, TAG_LIST);
	ensure_tagged(list_b, TAG_LIST);
	return list_concat((list_t*) untag(list_a), (list_t*) untag(list_b));
}
long list_concat(list_t* list_a, list_t* list_b) {
	list_t* concat = _list_create(list_a->size + list_b->size);
	list_extend(concat, list_a);
	list_extend(concat, list_b);
	long tagged = (long) concat;
    ensure_aligned(tagged);
    return tagged + TAG_LIST; // Tag for lists: 6 = b110
}

long tlist_get_splice(void * unused, const long list, long lo, long hi) {
	ensure_tagged(list, TAG_LIST);
	list_t* ulist = (list_t*) untag(list);
	return list_get_splice(
		ulist,
		untag_or_null(lo, TAG_INT, 0),
		untag_or_null(hi, TAG_INT, ulist->size)
	);
}
long list_get_splice(list_t* list, long lo, long hi) {
	if (hi < 0) {
		hi += list->size;
		if (hi < 0) {
			hi = 0;
		}
	}
	if (lo < 0) {
		lo += list->size;
		if (lo < 0) {
			lo = 0;
		}
	}

	list_t* slice = _list_create(MAX(hi-lo, 0));
	for(int i = lo; i<hi; i++) {
		list_append(slice, list_get_elem(list, i));
	}

	long tagged = (long) slice;
    ensure_aligned(tagged);
    return tagged + TAG_LIST;
}

long tlist_append(void * unused, const long list, const long elem) {
	ensure_tagged(list, TAG_LIST);
	return list_append((list_t*) untag(list), elem);
}
long list_append(list_t* list, const long elem) {
	list_resize(list, list->size + 1);
	list->items[list->size++] = elem;
	return TAG_NONE_VALUE;

}

long tlist_extend(void * unused, const long list, const long extension) {
	ensure_tagged(list, TAG_LIST);
	ensure_tagged(extension, TAG_LIST);
	return list_extend((list_t*) untag(list), (list_t*) untag(extension));
}
long list_extend(list_t* list, list_t* extension) {
	list_resize(list, list->size + extension->size);

	for (int i = 0; i < extension->size; i++) {
		list->items[list->size + i] = extension->items[i];
	}
	list->size += extension->size;
	return TAG_NONE_VALUE;
}

long tlist_insert(void * unused, const long list, long index, const long elem) {
	ensure_tagged(list, TAG_LIST);
	ensure_tagged(index, TAG_INT);
	return list_insert((list_t*) untag(list), untag(index), elem);
}
long list_insert(list_t* list, long index, const long elem) {
	list_resize(list, list->size + 1);

	if (index < 0) {
		index += list->size;
		if (index < 0) {
			index = 0;
		}
	}
	if (index > list->size) {
		index = list->size;
	}

	for(int i = list->size; --i >= index; ) {
		list->items[i+1] = list->items[i];
	}

	list->items[index] = elem;
	list->size++;
	return TAG_NONE_VALUE;
}

long tlist_remove(void * unused, const long list, const long elem) {
	ensure_tagged(list, TAG_LIST);
	return list_remove((list_t*) untag(list), elem);
}
long list_remove(list_t* list, const long elem) {
	int i = 0;
	while (i < list->size && equal(list->items[i], elem) != TAG_TRUE_VALUE) i++;

	if (i >= list->size) {
		fprintf(stderr, "ERROR: list.remove(x): x not in list!\n");
		exit(1);
	}

	for (; i < list->size-1; i++) {
		list->items[i] = list->items[i+1];
	}
	list->size--;
	list_resize(list, list->size);
	return TAG_NONE_VALUE;
}

long tlist_pop(void * unused, const long list, long index) {
	ensure_tagged(list, TAG_LIST);
	index = untag_or_null(index, TAG_INT, -1);
	return list_pop((list_t*) untag(list), index);
}
long list_pop(list_t* list, long index) {
	if (index >= list->size) {
		fprintf(stderr, "ERROR: Index %ld out of range (%ld)!\n", index,
			list->size);
		exit(0);
		return -1; // for the form
	}

	if (index < 0) {
		index += list->size;
		if (index < 0) {
			fprintf(stderr, "ERROR: Index %ld out of range! (%ld)\n",
				index - list->size, list->size);
			exit(0);
		}
	}

	const long ret = list->items[index];
	for (int i = index; i < list->size-1; i++) {
		list->items[i] = list->items[i+1];
	}
	list->size--;
	list_resize(list, list->size);
	return ret;
}

long tlist_index(void * unused, const long list, const long elem) {
	ensure_tagged(list, TAG_LIST);
	return list_index((list_t*) untag(list), elem);
}
long list_index(list_t* list, const long elem) {
	for (int i = 0; i < list->size; i++) {
		if (equal(list->items[i], elem) == TAG_TRUE_VALUE) {
			return i << TAG_SIZE; //tagged as an int.
		}
	}
	fprintf(stderr, "ERROR: %ld was not found in the list!\n", elem >> TAG_SIZE);
	exit(1);

}

long tlist_count(void * unused, const long list, const long elem) {
	ensure_tagged(list, TAG_LIST);
	return list_count((list_t*) untag(list), elem);
}
long list_count(list_t* list, const long elem) {
	long count = 0; // Avoid casting later
	for (int i = 0; i < list->size; i++) {
		if (equal(list->items[i], elem) == TAG_TRUE_VALUE) {
			count++;
		}
	}
	return count << TAG_SIZE; //tagged as an int.
}

long tlist_reverse(void * unused, const long list) {
	ensure_tagged(list, TAG_LIST);
	return list_reverse((list_t*) untag(list));
}
long list_reverse(list_t* list) {
	long* lo = list->items;
	long* hi = list->items + list->size - 1;
	long* t = GC_MALLOC(sizeof(lo));
	while (lo < hi) {
		*t = *lo;
		*lo = *hi;
		*hi = *t;
		lo++;
		hi--;
	}
	return TAG_NONE_VALUE;
}

long tlist_compare(
	void * unused,
	long list_a,
	long list_b,
	long cmp(long left, long right))
{
	ensure_tagged(list_a, TAG_LIST);
	ensure_tagged(list_b, TAG_LIST);
	return list_compare((list_t*)untag(list_a), (list_t*)untag(list_b), cmp);
}
long list_compare(
	list_t* list_a,
	list_t* list_b,
	long cmp(long left, long right))
{
	long size = MIN(list_a->size, list_b->size);
	for (int i = 0; i < size; i++) {
		if (equal(list_a->items[i], list_b->items[i]) != TAG_TRUE_VALUE) {
			return cmp(list_a->items[i], list_b->items[i]);
		}
	}
	return cmp(tag(list_a->size, TAG_INT), tag(list_b->size, TAG_INT));
}

long tlist_mul(void* unused, long list, long factor) {
	ensure_tagged(list, TAG_LIST);
	int tag = factor & TAG_MASK;
	if (!(tag == TAG_INT || tag == TAG_BOOL)) {
		fprintf(stderr, "ERROR: Incompatible type 'list' and %s!\n",
			tag_to_string(tag));
		exit(1);
	}
	return list_mul((list_t*)untag(list), untag(factor));
}

long list_mul(list_t* list, long factor) {
	list_t* new_list = _list_create(MAX(list->size*factor, 0));
	for (int i = 0; i < factor; i++) {
		list_extend(new_list, list);
	}
	long tagged = (long) new_list;
    ensure_aligned(tagged);
    return tagged + TAG_LIST; // Tag for lists: 6 = b110
}

long tlist_get_attr(long list, char * attr) {
	static closure_t len = {tlist_get_size, NULL, 1};
	static closure_t get_item = {tlist_get_elem, NULL, 2};
	static closure_t set_item = {tlist_set_elem, NULL, 3};
	static closure_t concat = {tlist_concat, NULL, 2};
	static closure_t mul = {tlist_mul, NULL, 2};
	static closure_t append = {tlist_append, NULL, 2};
	static closure_t extend = {tlist_extend, NULL, 2};
	static closure_t insert = {tlist_insert, NULL, 3};
	static closure_t remove = {tlist_remove, NULL, 2};
	static closure_t pop = {tlist_pop, NULL, 2};
	static closure_t index = {tlist_index, NULL, 2};
	static closure_t count = {tlist_count, NULL, 2};
	static closure_t reverse = {tlist_reverse, NULL, 1};
	if (strcmp("__len__", attr) == 0) {
		return ((long) (&len)) + TAG_FUNCTION;
	} else if (strcmp("__getitem__", attr) == 0) {
		return ((long) (&get_item)) + TAG_FUNCTION;
	} else if (strcmp("__setitem__", attr) == 0) {
		return ((long) (&set_item)) + TAG_FUNCTION;
	} else if (strcmp("__add__", attr) == 0) {
		return ((long) (&concat)) + TAG_FUNCTION;
	} else if (strcmp("__mul__", attr) == 0) {
		return ((long) (&mul)) + TAG_FUNCTION;
	} else if (strcmp("append", attr) == 0) {
		return ((long) (&append)) + TAG_FUNCTION;
	} else if (strcmp("extend", attr) == 0) {
		return ((long) (&extend)) + TAG_FUNCTION;
	} else if (strcmp("insert", attr) == 0) {
		return ((long) (&insert)) + TAG_FUNCTION;
	} else if (strcmp("remove", attr) == 0) {
		return ((long) (&remove)) + TAG_FUNCTION;
	} else if (strcmp("pop", attr) == 0) {
		return ((long) (&pop)) + TAG_FUNCTION;
	} else if (strcmp("index", attr) == 0) {
		return ((long) (&index)) + TAG_FUNCTION;
	} else if (strcmp("count", attr) == 0) {
		return ((long) (&count)) + TAG_FUNCTION;
	} else if (strcmp("reverse", attr) == 0) {
		return ((long) (&reverse)) + TAG_FUNCTION;
	} else {
		return TAG_NONE_VALUE;
	}
}
