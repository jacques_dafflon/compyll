#include <stdio.h>
#include <gc.h>
#include "hashmap.h"

#define TESTS_SIZE 5

int test_create(void);
int test_insert(void);
int test_lookup_integers(void);
int test_lookup_booleans(void);
int test_index_overflow(void);

int main(int argv, char** argc) {
    int (*tests[TESTS_SIZE])() = {
        test_create,
        test_insert,
        test_lookup_integers,
        test_lookup_booleans,
        test_index_overflow
    };

    char* test_names[TESTS_SIZE] = {
        "test creation",
        "test insertion",
        "test lookup integers",
        "test lookup booleans",
        "test index overflow"
    };
    GC_INIT();
    int status = 0;
    for(int i = 0; i < TESTS_SIZE; i++) {
        printf("Running %s:\n", test_names[i]);
	    if(tests[i]() != 0) {
            printf("FAIL --> %s\n\n", test_names[i]);
            status = 1;
        } else {
            printf("pass --> %s\n\n", test_names[i]);
        }
    }
    return status;
}

int test_create(void) {
    hashmap_t * hashmap;
    hashmap = hashmap_create(1);
    printf("Created hashmap of size %d\n", 1);

    hashmap = hashmap_create(100);
    printf("Created hashmap of size %d\n", 100);

    hashmap = hashmap_create(10000);
    printf("Created hashmap of size %d\n", 10000);

    return 0;
}

int test_insert(void) {
    hashmap_t * hashmap;
    hashmap = hashmap_create(9);

    hashmap_insert(hashmap, "x", 4);
    hashmap_insert(hashmap, "y", 8);
    hashmap_insert(hashmap, "z", 12);
    hashmap_insert(hashmap, "k", 16);
    hashmap_insert(hashmap, "j",  5);

    return 0;
 }

int test_lookup_integers(void) {
    hashmap_t * hashmap;
    hashmap = hashmap_create(20);
    hashmap_insert(hashmap, "x", 4);
    hashmap_insert(hashmap, "y", 8);
    hashmap_insert(hashmap, "z", 12);
    hashmap_insert(hashmap, "k", 16);
    hashmap_insert(hashmap, "t", 160);
    hashmap_insert(hashmap, "r", 25000);
    hashmap_insert(hashmap, "v", 1048576);

    if(hashmap_lookup(hashmap, "x") != 4) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "x"), 4, "x");
	return 1;
    }
    if(hashmap_lookup(hashmap, "y") != 8) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "y"), 8, "y");
	return 1;
    }
    if(hashmap_lookup(hashmap, "z") != 12) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "z"), 12, "z");
	return 1;
    }
    if(hashmap_lookup(hashmap, "k") != 16) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "k"), 16, "k");
	return 1;
    }
    if(hashmap_lookup(hashmap, "t") != 160) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "t"), 160, "t");
	return 1;
    }
    if(hashmap_lookup(hashmap, "r") != 25000) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "r"), 25000, "r");
	return 1;
    }
    if(hashmap_lookup(hashmap, "v") != 1048576) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "v"), 1048576, "v");
	return 1;
    }
    return 0;
}

int test_lookup_booleans(void) {
    hashmap_t * hashmap;
    hashmap = hashmap_create(2);
    hashmap_insert(hashmap, "x", 5);
    hashmap_insert(hashmap, "y", 1);

    if(hashmap_lookup(hashmap, "x") != 5) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "x"), 5, "x");
	return 1;
    }
    if(hashmap_lookup(hashmap, "y") != 1) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "y"), 1, "y");
	return 1;
    }
    return 0;
}

int test_index_overflow(void) {
   hashmap_t * hashmap;
   hashmap = hashmap_create(2);
   hashmap_insert(hashmap, "x", 4);
   hashmap_insert(hashmap, "y", 8);
   hashmap_insert(hashmap, "z", 12);
   hashmap_insert(hashmap, "k", 16);
 
 if(hashmap_lookup(hashmap, "x") != 4) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "x"), 4, "x");
	return 1;
    }
    if(hashmap_lookup(hashmap, "y") != 8) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "y"), 8, "y");
	return 1;
    }
    if(hashmap_lookup(hashmap, "z") != 12) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "z"), 12, "z");
	return 1;
    }
    if(hashmap_lookup(hashmap, "k") != 16) {
        printf("Got wrong value %ld, expected %d for key %s\n", hashmap_lookup(hashmap, "k"), 16, "k");
	return 1;
    }
    return 0;  
}
