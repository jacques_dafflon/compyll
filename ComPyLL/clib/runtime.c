#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <gc.h>

#include "tags.h"
#include "our_string.h"
#include "init.h"
#include "hashmap.h"
#include "list.h"

// Prints the raw value, along with tag and the value untagged as integer
void print_raw(long x) {
    printf("%ld (tag=%ld, int value=%ld)\n", x, x & TAG_MASK, x >> TAG_SIZE);
}

// Returns the string representation
char* to_str(long x) {
    init();
    long tag = x & TAG_MASK;
    int out_len;
    int max_len = 16;
    char * string= NULL;
    switch (tag) {
        case TAG_INT: //int
            max_len = 5;
            string = GC_MALLOC(max_len);
            out_len = snprintf(string, max_len, "%ld", x >> TAG_SIZE);
            if (out_len > max_len) {
                string = GC_MALLOC(out_len);
                snprintf(string, out_len, "%ld", x >> TAG_SIZE);
            }
            break;

        case TAG_BOOL: //boolean
            if (x == TAG_TRUE_VALUE) {
                max_len = 6;
                string = GC_MALLOC(max_len);
                out_len = snprintf(string, max_len, "True");
                if (out_len > max_len) {
                    string = GC_MALLOC(out_len);
                    snprintf(string, out_len, "True");
                }
            } else if (x == TAG_FALSE_VALUE) {
                max_len = 7;
                string = GC_MALLOC(max_len);
                out_len = snprintf(string, max_len, "False");
                if (out_len > max_len) {
                    string = GC_MALLOC(out_len);
                    snprintf(string, out_len, "False");
                }
            } else {
                fprintf(stderr, "ERROR: Printing invalid value (tag = %ld, value = %ld)\n", tag, x);
                exit(1);
            }
            break;

        case TAG_UNDEFINED: //not declared
            error_undefined_variable();
            break;

        case TAG_FUNCTION: //function pointer
            if (x == TAG_FUNCTION) {
                max_len = 6;
                string = GC_MALLOC(max_len);
                out_len = snprintf(string, max_len, "None");
                if (out_len > max_len) {
                    string = GC_MALLOC(out_len);
                    snprintf(string, out_len, "None");
                }
            } else {
                max_len = 34;
                string = GC_MALLOC(max_len);
                out_len = snprintf(string, max_len, "<Function Pointer at %ld>", x);
                if (out_len > max_len) {
                    string = GC_MALLOC(out_len);
                    snprintf(string, out_len, "<Function Pointer at %ld>", x);
                }
            }
            break;

        case TAG_OBJECT:
            if (x == TAG_OBJECT) {
                max_len = 6;
                string = GC_MALLOC(max_len);
                out_len = snprintf(string, max_len, "None");
                if (out_len > max_len) {
                    string = GC_MALLOC(out_len);
                    snprintf(string, out_len, "None");
                }
            } else {
                max_len = 24;
                string = GC_MALLOC(max_len);
                out_len = snprintf(string, max_len, "<Object at %ld>", x);
                if (out_len > max_len) {
                    string = GC_MALLOC(out_len);
                    snprintf(string, out_len, "<Object at %ld>", x);
                }
            }
            break;

        case TAG_STRING:
            string = GC_MALLOC(max_len);
            out_len = snprintf(string, max_len, "'%s'", (char*)(x-5));
            if (out_len > max_len) {
                string = GC_MALLOC(out_len);
                snprintf(string, out_len, "'%s'", (char*)(x-5));
            }
            break;

        case TAG_LIST: {
            list_t* list = (list_t*) untag(x);
            int list_len = (int) list->size;
            char* items[list_len];
            for (int i = 0; i < list_len; i++) {
                items[i] = to_str(list->items[i]);
            }
            int length = 0;
            for (int i = 0; i < list_len; ++i) {
                length += strlen(items[i]);
            }
            char *output = (char*)GC_MALLOC((length + 1 + (list_len) * 2)* sizeof(char));
            char* dest = output;
            *dest++ = '[';
            for (int i = 0; i < list_len-1; i++) {
                char *src = items[i];
                while (*src) {
                    *dest++ = *src++;
                }
                *dest++ = ',';
                *dest++ = ' ';
            } if (list_len > 0) {
                char *src = items[list_len-1];
                while (*src) {
                    *dest++ = *src++;
                }
            }
            *dest++ = ']';
            *dest = '\0';
            string = output;
            break;
        }
        default:
            max_len = 64;
            string = GC_MALLOC(max_len);
            out_len = snprintf(string, max_len, "ERROR: Printing invalid value (tag = %ld, value = %ld)", tag, x);
            if (out_len > max_len) {
                string = GC_MALLOC(out_len);
                snprintf(string, out_len, "ERROR: Printing invalid value (tag = %ld, value = %ld)", tag, x);
            }
            break;
    }
    return string;
}

long str(void * unused, long x) {
    return (long)(to_str(x)) + TAG_STRING;
}


void print(long x, bool quote) {
    long tag = x & TAG_MASK;
    switch (tag) {
        case TAG_INT: //int
            printf("%ld", x >> TAG_SIZE);
            break;

        case TAG_BOOL: //boolean
            if (x == TAG_TRUE_VALUE) {
                printf("True");
            } else if (x == TAG_FALSE_VALUE) {
                printf("False");
            } else {
                fprintf(stderr, "ERROR: Printing invalid value (tag = %ld, value = %ld)\n", tag, x);
                exit(1);
            }
            break;

        case TAG_UNDEFINED: //not declared
            error_undefined_variable();
            break;

        case TAG_FUNCTION: //function pointer
            if (x == TAG_FUNCTION) {
                printf("None");
            } else {
                printf("<Function Pointer at %ld>", x);
            }
            break;

        case TAG_OBJECT:
            if (x == TAG_OBJECT) {
                printf("None");
            } else {
                printf("<Object at %ld>", x);
            }
            break;

        case TAG_STRING: {
            if (quote) {
                printf("'%s'", (char*)(x - TAG_STRING));
            } else {
                printf("%s", (char*)(x - TAG_STRING));
            }
            break;
        }
        case TAG_LIST:{
            list_t* list = (list_t*) untag(x);
            printf("[");
            int i = 0;
            while(i < list->size -1) {
                print(list->items[i++], true);
                printf(", ");
            }
            if (i < list->size) print(list->items[i], true);
            printf("]");
            break;
            }

        default:
            fprintf(stderr, "ERROR: Printing invalid value (tag = %ld, value = %ld)\n", tag, x);
            break;
    }
}

void printnl(long x) {
    print(x, false);
    printf("\n");
}

long input(void * unused) {
    char * x = malloc(sizeof(char*)*10) ;
    scanf("%s", x);
    char* end;
    long result = strtol(x, &end, 10);
    if(*end) {
		//boolean
		if (strcmp(x, "True") == 0) {
			return TAG_TRUE_VALUE;
		} else if (strcmp(x, "False") == 0) {
			return TAG_FALSE_VALUE;
		} else {
		    return get_string_pointer(x);
		}
    } else {
    	//integer
    	result <<= TAG_SIZE;
    	return result;
    }
}

// Returns the length of the object (like built-in python method 'len')
long len(void * unused, long x) {
    int tag = get_tag(x);
    switch(tag) {
        case TAG_OBJECT:
            fprintf(stderr, "len(object) not implemented!\n");
            exit(1); // return tobject_get_size(x);

        case TAG_STRING:
            return tstring_get_size(NULL, x);
        case TAG_LIST:
            return tlist_get_size(NULL, x);

        default:
            fprintf(stderr, "ERROR: Incompatible type %s has no len()\n", tag_to_string(tag));
            exit(1);
    }
    return -1;
}

// Returns the nth element of the object (like built-in python [ ] operator)
long get_elem(long seq, long idx) {
    int tag = get_tag(seq);
    switch(tag) {
        case TAG_OBJECT:
            if (seq == TAG_NONE_VALUE) {
                fprintf(stderr, "ERROR: Type 'NoneType' has no attribute \'__getitem__'\n");
                exit(1);
            } else {
                fprintf(stderr, "get_elem(object) not implemented!\n");
                exit(1); // return tobject_get_elem(seq, idx);
            }

        case TAG_STRING:
            return index_string(NULL, seq, idx);

        case TAG_LIST:
            return tlist_get_elem(NULL, seq, idx);

        default:
            fprintf(stderr, "ERROR: Type %s has no attribute \'__getitem__'\n", tag_to_string(tag));
            exit(1);
    }
    return -1;
}

// Sets the nth element of the object (like built-in python [ ] operator)
void set_elem(long seq, long idx, long elem) {
    int tag = get_tag(seq);
    switch(tag) {
        case TAG_OBJECT:
            fprintf(stderr, "set_elem(object) not implemented!\n");
            exit(1); // return tobject_set_elem(seq, idx);

        case TAG_LIST:
            tlist_set_elem(NULL, seq, idx, elem);
            break;

        default:
            fprintf(stderr, "ERROR: Type %s has no attribute \'__getitem__'\n", tag_to_string(tag));
            exit(0);
    }
}

// Returns the attribute of the object
long get_attr(long obj, char * attr) {
    int tag = get_tag(obj);
    switch(tag) {
        case TAG_OBJECT: {
            hashmap_t * map = (hashmap_t*) untag(obj);
            return hashmap_lookup(map, attr);
        }

        case TAG_STRING:
            return tstring_get_attr(obj, attr);

        case TAG_LIST:
            return tlist_get_attr(obj, attr);

        default:
            fprintf(stderr, "ERROR: Type %s has no attribute \'%s'\n", tag_to_string(tag), attr);
            exit(0);
    }
    return -1;
}

long get_splice(long obj , long left, long right) {
    int tag = get_tag(obj);
    switch(tag) {
        case TAG_OBJECT:
            fprintf(stderr, "tobject_get_splice(object) not implemented!\n");
            exit(1); // return tobject_get_splice(NULL, obj, left, right);

        case TAG_STRING:
            fprintf(stderr, "tstring_get_splice(object) not implemented!\n");
            exit(1); // return tstring_get_splice(NULL, obj, left, right);

        case TAG_LIST:
            return tlist_get_splice(NULL, obj, left, right);

        default:
            fprintf(stderr, "ERROR: Type %s has no attribute \'__getslice__'\n", tag_to_string(tag));
            exit(0);
    }
    return -1;
}

// Sets the attribute of the object
void set_attr(long obj, char * attr, long value) {
    int tag = get_tag(obj);
    switch(tag) {
        case TAG_OBJECT: {
            hashmap_t * map = (hashmap_t*) untag(obj);
            hashmap_insert(map, attr, value);
            break;
        }

        case TAG_LIST:
            fprintf(stderr, "ERROR: Attribute \'%s\' of list is read-only\n", attr);
            exit(0);

        default:
            fprintf(stderr, "ERROR: Cannot set attribute of type %s\n", tag_to_string(tag));
            exit(0);
    }
}
