#ifndef HASHMAP_H
#define HASHMAP_H

// Entry struct
struct hashmap_entry {
	char *key;		// the map key (array of characters)
	int hash;		// the hashed key as computed by hashmap_compute_hash
	long value;		// the value of the entry
	struct hashmap_entry *next;	// pointer to the next struct (if any, otherwise NULL)
};
typedef struct hashmap_entry hashmap_entry;

// Hashmap struct, contains the table size and the table, 
typedef struct _hashmap_t {
	int table_size;			// table_size
	int used;			// number of cells in the array used. (not num of elems)
	struct hashmap_entry **table;	// table (list of pointers)
} hashmap_t;

hashmap_t * hashmap_create(int size);
void hashmap_insert(hashmap_t * const hashmap, char * const key, const long value);
long hashmap_lookup(hashmap_t * const hashmap, char * const key);
int hashmap_compute_hash(char * key);
#endif // HASHMAP_H
