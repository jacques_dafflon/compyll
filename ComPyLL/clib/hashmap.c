#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gc.h>

#include "hashmap.h"

#include "tags.h"
#include "init.h"


#define H_DEBUG 0 // Disable := 0 | Enable := 1

hashmap_t * hashmap_create(int size) {

    if (size <= 0) {
        fprintf(stderr, "ERROR: The size of the hasmap must be positive!\n");
        return NULL;
    }
    init();

	// allocate the hashmap
	hashmap_t * hashmap = GC_MALLOC(sizeof(hashmap_t));

    // roundup the table size to the nearse value of 2
    if (H_DEBUG) printf("rounding %d -->", size);
    size--;
    size |= size >> 1;
    size |= size >> 2;
    size |= size >> 4;
    size |= size >> 8;
    size |= size >> 16;
    size++;
    if (H_DEBUG) printf(" %d\n", size);

	// set the size
	hashmap->table_size = size;
    hashmap->used = 0;

	// allocate the table
	hashmap->table = GC_MALLOC(size * sizeof(hashmap_entry *));
	
	// populate the table with zeros
	for (int i = 0; i < size; ++i) {
		hashmap->table[i] = NULL;
	}
	return hashmap;
}

int hashmap_hash(char * key) {
    unsigned long hash = 0;
    int c;
    while ((c = *key++)) {
        hash = hash ^ c ^ (c>>2);
    }
    return hash;
}

void hashmap_insert(hashmap_t * const hashmap, char * const key, const long value) {
    int hash = hashmap_hash(key);
    int i = hash & (hashmap->table_size - 1);
    
    hashmap_entry * prev = hashmap->table[i];
    
    // Insert new entry
    if ( hashmap->table[i] == NULL) {
        if (H_DEBUG) printf("Null, inserting %ld\n", value);
        hashmap->table[i] = GC_MALLOC(sizeof(hashmap_entry));
        hashmap->table[i]->key = key;
        hashmap->table[i]->value = value;
        hashmap->table[i]->hash = hash;
        hashmap->table[i]->next = NULL;
        hashmap->used++;
        return;
    }
    
    // Update first value at index
    if  ( prev != NULL && (prev->hash == hash && strcmp(prev->key, key) == 0 )) {
        if (H_DEBUG) printf("Update first elem at pos %d with same keys %s, %s, %d\n", i, prev->key, key, strcmp(prev->key, key));
        hashmap->table[i]->value = value;
        return;
    }
    
    // Goes down the list of entries for the hash to find the correct key. 
    while (prev->next != NULL && (prev->next->hash != hash || strcmp(prev->next->key, key) != 0)) {
        if (H_DEBUG) printf("looking...\n");
        prev = prev->next;
    }
    if (prev->next != NULL) { // Update value in the list
        if (H_DEBUG) printf("Replace value %ld by value %ld in list\n", prev->next->value, value);
        if (H_DEBUG) printf("since %d != %d or %s != %s (%d)\n", prev->next->hash, hash, prev->next->key, key, strcmp(prev->next->key, key));
        prev->next->value = value;
        return;
    }
    
    if (H_DEBUG) printf("Null, appending %ld after %ld\n", value, prev->value);
    // Key not found, append a new entry
    prev->next = GC_MALLOC(sizeof(hashmap_entry));
    prev->next->value = value;
    prev->next->key = key;
    prev->next->hash = hash;
    prev->next->next=NULL;
    
    return;
}

long hashmap_lookup(hashmap_t * const hashmap, char * const key) {
    int hash = hashmap_hash(key);
    hashmap_entry * e = *(hashmap->table + (hash & (hashmap->table_size - 1)));
    
    if (e == NULL) { // Key not in map
        if (H_DEBUG) printf("%s not found\n", key);
        return TAG_NONE_VALUE;
    }
    
    if ( e->hash == hash && strcmp(e->key, key) == 0 ) { // Key is first elem in hash
        if (H_DEBUG) printf("found (%s, %ld), at first!\n", key, e->value);
        return e->value;
    }
    
    // Look for correct elem with hash
    while ( e != NULL && (e->hash != hash || strcmp(e->key, key) != 0) ) {
        if (H_DEBUG) printf("looking...\n");
        e = e->next;
    }
    if (e == NULL) { // not found
        if (H_DEBUG) printf("%s not found\n", key);
        return TAG_NONE_VALUE;
    }
    if (H_DEBUG) printf("found (%s, %ld), in the end!\n", key, e->value);
    return e->value; // found
}
