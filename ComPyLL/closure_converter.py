import logging
from variable_generator import generate_function
import ast
from constants import *

__all__ = ['convert_closure']


def convert_closure(tree):
    closure_converter = ClosureConverter()
    return closure_converter.convert_closure(tree)


class ClosureConverter():

    def __init__(self):
        self.can_return = False

    def convert_closure(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def String(self, tree):
        return tree

    def Function(self, tree):  # : disable=C0103

        old_level = self.can_return
        self.can_return = True
        tree.body = self.convert_closure(tree.body)
        self.can_return = old_level

        func_name = generate_function()

        parameters = map(lambda x: x.name, tree.params)
        variable_finder = VariableFinder(parameters)
        variable_finder.find_variables(tree.body)
        free_variables = variable_finder.get_free_vars()
        local_variables = variable_finder.get_local_vars()

        variable_replacer = VariableReplacer(func_name, free_variables, local_variables)
        tree.body = variable_replacer.replace_variables(tree.body)

        closure = ast.MakeClosure(ast.ConvertedLambda(func_name, tree.params, tree.body),
                                  len(tree.params),
                                  map(lambda v: ast.Name(v), free_variables),
                                  tree.name)

        return ast.Assign([ast.AssName(tree.name)], closure)

    def Lambda(self, tree):  # : disable=C0103

        tree.exp = self.convert_closure(tree.exp)

        func_name = generate_function()

        parameters = map(lambda x: x.name, tree.params)
        variable_finder = VariableFinder(parameters)
        variable_finder.find_variables(tree.exp)
        free_variables = variable_finder.get_free_vars()
        local_variables = variable_finder.get_local_vars()

        variable_replacer = VariableReplacer(func_name, free_variables, local_variables)
        tree.exp = variable_replacer.replace_variables(tree.exp)

        lambda_body = ast.Stmt([ast.Return(tree.exp)])

        return ast.MakeClosure(ast.ConvertedLambda(func_name, tree.params, lambda_body),
                               len(tree.params),
                               map(lambda v: ast.Name(v), free_variables),
                               '')

    def Return(self, tree):  # : disable=C0103
        if self.can_return:
            tree.exp = self.convert_closure(tree.exp)
            return tree
        else:
            logging.error('SyntaxError: invalid return statement')
            exit(1)

    def Module(self, tree):  # : disable=C0103
        tree.node = self.convert_closure(tree.node)
        return tree

    def Stmt(self, tree):  # : disable=C0103
        tree.nodes = map(self.convert_closure, tree.nodes)
        return tree

    def Const(self, tree):  # : disable=C0103
        return tree

    def CallFunc(self, tree):  # pylint: disable=C0103
        tree.node = self.convert_closure(tree.node)
        tree.args = map(self.convert_closure, tree.args)
        return tree

    def Class(self, tree):  # pylint: disable=C0103
        tree.body = self.convert_closure(tree.body)

        # Look for __init__
        init = None
        for stmt in tree.body.nodes:
            if type(stmt) is ast.Assign:
                if type(stmt.exp) is ast.MakeClosure:
                    for ass_name in stmt.nodes:
                        if ass_name.name == '__init__':
                            init = stmt.exp

        # create __init__ if it does not exist
        if not init:
            func_name = generate_function()
            params = [ast.Name('self')]
            init = ast.MakeClosure(ast.ConvertedLambda(func_name, params, ast.Stmt([])), len(params), [], '__init__')
            tree.body.nodes.append(ast.Assign([ast.AssName('__init__')], init))

        # make sure everything is right
        assert type(init) is ast.MakeClosure
        assert type(init.func) is ast.ConvertedLambda
        assert init.nparams == len(init.func.params)
        assert init.assname == '__init__'

        return tree

    def Discard(self, tree):  # : disable=C0103
        tree.exp = self.convert_closure(tree.exp)
        return tree

    def Assign(self, tree):  # : disable=C0103
        tree.exp = self.convert_closure(tree.exp)
        if type(tree.exp) is ast.ConvertedLambda:
            tree.exp.assname = tree.nodes[0]
        tree.nodes = map(self.convert_closure, tree.nodes)
        return tree

    def AugAssign(self, tree):  # : disable=C0103
        tree.node = self.convert_closure(tree.node)
        tree.exp = self.convert_closure(tree.exp)
        return tree

    def List(self, tree):
        tree.items = [self.convert_closure(item) for item in tree.items]
        return tree

    def Index(self, tree):
        tree.exp = self.convert_closure(tree.exp)
        tree.index = self.convert_closure(tree.index)
        return tree

    def AssIndex(self, tree):
        tree.exp = self.convert_closure(tree.exp)
        tree.index = self.convert_closure(tree.index)
        return tree

    def Splice(self, tree):
        if tree.left:
            tree.left = self.convert_closure(tree.left)
        if tree.right:
            tree.right = self.convert_closure(tree.right)
        tree.exp = self.convert_closure(tree.exp)
        return tree

    def Pass(self, tree):  # : disable=C0103
        return tree

    def NoneNode(self, tree):  # : disable=C0103
        return tree

    def And(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Or(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def ObjRef(self, tree):
        tree.exp = self.convert_closure(tree.exp)
        return tree

    def Xor(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Printnl(self, tree):  # : disable=C0103
        tree.exp = self.convert_closure(tree.exp)
        return tree

    def Name(self, tree):  # : disable=C0103
        return tree

    def Register(self, tree):  # : disable=C0103
        return tree

    def AssName(self, tree):  # : disable=C0103
        return tree

    def AssObjRef(self, tree):
        tree.exp = self.convert_closure(tree.exp)
        return tree

    def AssRegister(self, tree):  # : disable=C0103
        return tree

    def Add(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Sub(self, tree):  # : disable=C0103
        tree = self._convert_closure_binary_node(tree)
        return tree

    def Mul(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Div(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def UnaryAdd(self, tree):  # : disable=C0103
        return self._convert_closure_unary_node(tree)

    def UnarySub(self, tree):  # : disable=C0103
        return self._convert_closure_unary_node(tree)

    def FloorDiv(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Mod(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def If(self, tree):  # : disable=C0103
        tree.cond = self.convert_closure(tree.cond)
        tree.body = self.convert_closure(tree.body)
        if tree.other:
            tree.other = self.convert_closure(tree.other)
        return tree

    def Invert(self, tree):  # : disable=C0103
        return self._convert_closure_unary_node(tree)

    def Not(self, tree):
        return self._convert_closure_unary_node(tree)

    def Bitand(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Bitor(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Bitxor(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def LeftShift(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def RightShift(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def Equal(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def LessEqual(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def GreaterEqual(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def NotEqual(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def LessThan(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def GreaterThan(self, tree):  # : disable=C0103
        return self._convert_closure_binary_node(tree)

    def While(self, tree):  # : disable=C0103
        tree.cond = self.convert_closure(tree.cond)
        tree.body = self.convert_closure(tree.body)
        return tree

    def _convert_closure_binary_node(self, node):
        node.left = self.convert_closure(node.left)
        node.right = self.convert_closure(node.right)
        return node

    def _convert_closure_unary_node(self, node):
        node.exp = self.convert_closure(node.exp)
        return node


class VariableFinder():

    def __init__(self, params):
        self.free_vars = set()
        self.params = params
        self.local_vars = set()

    def get_free_vars(self):
        return self.free_vars

    def get_local_vars(self):
        return self.local_vars

    def find_variables(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def defined(self, var):
        return var in self.params or var in self.local_vars

    def define(self, var):
        if var in self.free_vars:
            self.free_vars.remove(var)
        self.local_vars.add(var)

    def String(self, tree):
        pass

    def Function(self, tree):  # : disable=C0103
        if not self.defined(tree.name):
            self.define(tree.name)

    def Lambda(self, tree):  # : disable=C0103
        pass

    def MakeClosure(self, tree):  # : disable=C0103
        self.find_variables(tree.func)
        for free_var in tree.env:
            self.find_variables(free_var)

    def ConvertedLambda(self, tree):
        added = set()
        for param in tree.params:
            if param.name not in self.local_vars:
                self.local_vars.add(param.name)
                added.add(param.name)
        self.find_variables(tree.exp)
        for param_str in added:
            self.local_vars.remove(param_str)

    def EnvRef(self, tree):
        pass

    def ObjRef(self, tree):
        self.find_variables(tree.exp)

    def Return(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)

    def Stmt(self, tree):  # : disable=C0103
        for stmt in tree.nodes:
            self.find_variables(stmt)

    def Discard(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)

    def Const(self, tree):  # : disable=C0103
        pass

    def NoneNode(self, tree):
        pass

    def CallFunc(self, tree):  # : disable=C0103
        self.find_variables(tree.node)
        for arg in tree.args:
            self.find_variables(arg)

    def Assign(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)
        for node in tree.nodes:
            self.find_variables(node)

    def AugAssign(self, tree):   # : disable=C0103
        self.find_variables(tree.node)
        self.find_variables(tree.exp)

    def List(self, tree):
        for item in tree.items:
            self.find_variables(item)

    def Index(self, tree):
        self.find_variables(tree.exp)
        self.find_variables(tree.index)

    def AssIndex(self, tree):
        self.find_variables(tree.exp)
        self.find_variables(tree.index)

    def Splice(self, tree):
        if tree.left:
            self.find_variables(tree.left)
        if tree.right:
            self.find_variables(tree.right)
        self.find_variables(tree.exp)

    def Pass(self, tree):  # : disable=C0103
        pass

    def And(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Or(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Xor(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Printnl(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)

    def Name(self, tree):  # : disable=C0103
        if not self.defined(tree.name):
            self.free_vars.add(tree.name)

    def AssName(self, tree):  # : disable=C0103
        self.define(tree.name)

    def AssObjRef(self, tree):
        self.find_variables(tree.exp)

    def Add(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Sub(self, tree):   # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Mul(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Div(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def UnaryAdd(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def UnarySub(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)

    def FloorDiv(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Mod(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def If(self, tree):  # : disable=C0103
        self.find_variables(tree.cond)
        self.find_variables(tree.body)
        if tree.other:
            self.find_variables(tree.other)

    def Invert(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)

    def Not(self, tree):  # : disable=C0103
        self.find_variables(tree.exp)

    def Bitand(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Bitor(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Bitxor(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def LeftShift(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def RightShift(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def Equal(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def LessEqual(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def GreaterEqual(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def NotEqual(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def LessThan(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def GreaterThan(self, tree):  # : disable=C0103
        self.find_variables(tree.left)
        self.find_variables(tree.right)

    def While(self, tree):  # : disable=C0103
        self.find_variables(tree.cond)
        self.find_variables(tree.body)


class VariableReplacer():

    def __init__(self, func_name, free_vars, local_vars):
        self.func_name = func_name
        self.env_name = '.env'
        self.free_vars = free_vars
        self.local_vars = local_vars

    def replace_variables(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def Function(self, tree):  # : disable=C0103
        logging.error('This should not happen')
        exit(1)

    def Lambda(self, tree):  # : disable=C0103
        logging.error('This should not happen')
        exit(1)

    def MakeClosure(self, tree):
        tree.func = self.replace_variables(tree.func)
        tree.env = map(self.replace_variables, tree.env)
        return tree

    def String(self, tree):
        return tree

    def ConvertedLambda(self, tree):
        added = set()
        for param in tree.params:
            if param.name not in self.local_vars:
                self.local_vars.add(param.name)
                added.add(param.name)
        tree.exp = self.replace_variables(tree.exp)
        for param_str in added:
            self.local_vars.remove(param_str)
        return tree

    def EnvRef(self, tree):
        return tree

    def ObjRef(self, tree):
        self.replace_variables(tree.exp)
        return tree

    def Return(self, tree):  # : disable=C0103
        tree.exp = self.replace_variables(tree.exp)
        return tree

    def Stmt(self, tree):  # : disable=C0103
        tree.nodes = map(self.replace_variables, tree.nodes)
        return tree

    def Discard(self, tree):  # : disable=C0103
        tree.exp = self.replace_variables(tree.exp)
        return tree

    def Const(self, tree):  # : disable=C0103
        return tree

    def NoneNode(self, tree):
        return tree

    def CallFunc(self, tree):  # : disable=C0103
        tree.node = self.replace_variables(tree.node)
        tree.args = map(self.replace_variables, tree.args)
        return tree

    def Assign(self, tree):  # : disable=C0103
        tree.exp = self.replace_variables(tree.exp)
        tree.nodes = map(self.replace_variables, tree.nodes)
        return tree

    def AugAssign(self, tree):  # : disable=C0103
        tree.node = self.replace_variables(tree.node)
        tree.exp = self.replace_variables(tree.exp)
        return tree

    def List(self, tree):
        tree.items = [self.replace_variables(item) for item in tree.items]
        return tree

    def Index(self, tree):
        tree.exp = self.replace_variables(tree.exp)
        tree.index = self.replace_variables(tree.index)
        return tree

    def AssIndex(self, tree):
        tree.exp = self.replace_variables(tree.exp)
        tree.index = self.replace_variables(tree.index)
        return tree

    def Splice(self, tree):
        if tree.left:
            tree.left = self.replace_variables(tree.left)
        if tree.right:
            tree.right = self.replace_variables(tree.right)
        tree.exp = self.replace_variables(tree.exp)
        return tree

    def Pass(self, tree):  # : disable=C0103
        return tree

    def And(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Or(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Xor(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Printnl(self, tree):  # : disable=C0103
        tree.exp = self.replace_variables(tree.exp)
        return tree

    def Name(self, tree):  # : disable=C0103
        if tree.name in self.free_vars:
            return ast.EnvRef(self.env_name, tree.name)
        return tree

    def AssName(self, tree):  # : disable=C0103
        if tree.name in self.free_vars:
            logging.error('Bad: Assignment to a free variable!')
            logging.error('Free vars: ' + str(self.free_vars))
            logging.error('Name: ' + tree.name)
            exit(1)
        return tree

    def AssObjRef(self, tree):
        tree.exp = self.replace_variables(tree.exp)
        return tree

    def Add(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Sub(self, tree):   # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Mul(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Div(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def UnaryAdd(self, tree):  # : disable=C0103
        return self.replace_variables_unary_node(tree)

    def UnarySub(self, tree):  # : disable=C0103
        return self.replace_variables_unary_node(tree)

    def FloorDiv(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Mod(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def If(self, tree):  # : disable=C0103
        tree.cond = self.replace_variables(tree.cond)
        tree.body = self.replace_variables(tree.body)
        if tree.other:
            tree.other = self.replace_variables(tree.other)
        return tree

    def Invert(self, tree):  # : disable=C0103
        return self.replace_variables_unary_node(tree)

    def Not(self, tree):
        return self.replace_variables_unary_node(tree)

    def Bitand(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Bitor(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Bitxor(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def LeftShift(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def RightShift(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def Equal(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def LessEqual(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def GreaterEqual(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def NotEqual(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def LessThan(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    def GreaterThan(self, tree):  # : disable=C0103
        return self.replace_variables_binary_node(tree)

    #def Tag(self, tree):  # : disable=C0103
    #    return self.replace_variables_unary_node(tree)

    #def Untag(self, tree):  # : disable=C0103
    #    return self.replace_variables_unary_node(tree)

    def While(self, tree):  # : disable=C0103
        tree.cond = self.replace_variables(tree.cond)
        tree.body = self.replace_variables(tree.body)
        return tree

    def replace_variables_binary_node(self, node):
        node.left = self.replace_variables(node.left)
        node.right = self.replace_variables(node.right)
        return node

    def replace_variables_unary_node(self, node):
        node.exp = self.replace_variables(node.exp)
        return node
