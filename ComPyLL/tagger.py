import logging
import ast
from constants import *


__all__ = ["tag"]


def tag(tree):
    tagger = Tagger()
    return tagger.tag(tree)


class Tagger():
    def __init__(self):
        pass

    def tag(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def String(self, tree):  # pylint: disable=C0103
        return tree

    def Module(self, tree):  # pylint: disable=C0103
        tree.node = self.tag(tree.node)
        tree.funcs = self.tag(tree.funcs)
        return tree

    def Stmt(self, tree):  # pylint: disable=C0103
        tree.nodes = map(self.tag, tree.nodes)
        return tree

    def Discard(self, tree):  # pylint: disable=C0103
        tree.exp = self.tag(tree.exp)
        return tree

    def If(self, tree):  # pylint: disable=C0103
        tree.cond = self.tag(tree.cond)
        tree.body = self.tag(tree.body)
        if tree.other:
            tree.other = self.tag(tree.other)
        return tree

    def Const(self, tree):  # pylint: disable=C0103
        tree.value <<= TAG_SIZE
        return tree

    def NoneNode(self, tree):  # pylint: disable=C0103
        return ast.Const(NONE_VALUE)  # Tagged Null pointer

    def Bool(self, tree):  # pylint: disable=C0103
        return ast.Const(TRUE_VALUE if tree.value else FALSE_VALUE)

    def CallFunc(self, tree):  # pylint: disable=C0103
        tree.node = self.tag(tree.node)
        tree.args = map(self.tag, tree.args)
        return tree

    def Function(self, tree):  # pylint: disable=C0103
        tree.body = self.tag(tree.body)
        return tree

    def MakeClosure(self, tree):  # pylint: disable=C0103
        tree.func = self.tag(tree.func)
        return tree

    def MakeEnv(self, tree):  # pylint: disable=C0103
        return tree

    def MakeObject(self, tree):
        return tree

    def Assign(self, tree):  # pylint: disable=C0103
        tree.nodes = map(self.tag, tree.nodes)
        tree.exp = self.tag(tree.exp)
        return tree

    def AugAssign(self, tree):  # pylint: disable=C0103
        tree.node = self.tag(tree.node)
        tree.exp = self.tag(tree.exp)
        return tree

    def List(self, tree):
        tree.items = [self.tag(item) for item in tree.items]
        return tree

    def Index(self, tree):
        tree.exp = self.tag(tree.exp)
        tree.index = self.tag(tree.index)
        return tree

    def AssIndex(self, tree):
        tree.exp = self.tag(tree.exp)
        tree.index = self.tag(tree.index)
        return tree

    def Splice(self, tree):
        if tree.left:
            tree.left = self.tag(tree.left)
        if tree.right:
            tree.right = self.tag(tree.right)
        tree.exp = self.tag(tree.exp)
        return tree

    def Printnl(self, tree):  # pylint: disable=C0103
        tree.exp = self.tag(tree.exp)
        return tree

    def Name(self, tree):  # pylint: disable=C0103
        return tree

    def EnvRef(self, tree):  # pylint: disable=C0103
        return tree

    def ObjRef(self, tree):
        tree.exp = self.tag(tree.exp)
        return tree

    def Register(self, tree):  # pylint: disable=C0103
        return tree

    def AssName(self, tree):  # pylint: disable=C0103
        return tree

    def AssRegister(self, tree):  # pylint: disable=C0103
        return tree

    def AssObjRef(self, tree):
        self.tag(tree.exp)
        return tree

    def _tag_binary(self, tree):
        tree.left = self.tag(tree.left)
        tree.right = self.tag(tree.right)

    # ==== Arithmetic operations ====
    def Add(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Sub(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Mul(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Div(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def FloorDiv(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Mod(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def LeftShift(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def RightShift(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    # ==== Bit operations ====
    # tags are handled by the LLVM code, as result tag depends on operands tag.
    def Bitand(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Bitor(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Bitxor(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Or(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def And(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def UnaryAdd(self, tree):  # pylint: disable=C0103
        tree.exp = self.tag(tree.exp)
        return tree

    def UnarySub(self, tree):  # pylint: disable=C0103
        tree.exp = self.tag(tree.exp)
        return tree

    def Invert(self, tree):  # pylint: disable=C0103
        tree.exp = self.tag(tree.exp)
        return tree

    def Not(self, tree):  # pylint: disable=C0103
        tree.exp = self.tag(tree.exp)
        return tree

    # ==== Boolead Operations ====
    # parameters are just untagged
    # result is casted from i1 to i32 and then tagged as boolean
    def Equal(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def LessEqual(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def GreaterEqual(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def NotEqual(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def LessThan(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def GreaterThan(self, tree):  # pylint: disable=C0103
        self._tag_binary(tree)
        return tree

    def Pass(self, tree):
        return tree

    def Return(self, tree):
        tree.exp = self.tag(tree.exp)
        return tree

    def While(self, tree):
        tree.cond = self.tag(tree.cond)
        tree.body = self.tag(tree.body)
        return tree
