"""
.. module:: string_finder
   :platform: Unix, OSX
   :synopsis: Look for valid strings.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

Something here.
"""
import logging
import ast

__all__ = ["find_strings"]


def find_strings(tree):
    string_finder = StringFinder()
    strings = string_finder.walk(tree)
    return strings


class StringFinder():  # pylint: disable=R0904

    def __init__(self):
        self.strings = set()

    def walk(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def _walk_binary_node(self, node):
        self.walk(node.left)
        self.walk(node.right)

    def _walk_unary_node(self, node):
        self.walk(node.exp)

    def String(self, tree):
        self.strings.add(tree.value)

    def Return(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)

    def Module(self, tree):  # pylint: disable=C0103
        self.walk(tree.node)
        self.walk(tree.funcs)
        return self.strings

    def Stmt(self, tree):  # pylint: disable=C0103
        for stmt in tree.nodes:
            self.walk(stmt)

    def Discard(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)

    def Const(self, tree):  # pylint: disable=C0103
        pass

    def CallFunc(self, tree):  # pylint: disable=C0103
        self.walk(tree.node)
        for arg in tree.args:
            self.walk(arg)

    def MakeClosure(self, tree):  # pylint: disable=C0103
        pass

    def MakeObject(self, tree):
        pass

    def Function(self, tree):  # pylint: disable=C0103
        self.walk(tree.body)

    def Assign(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)
        for stmt in tree.nodes:
            self.walk(stmt)

    def List(self, tree):
        for item in tree.items:
            self.walk(item)

    def Index(self, tree):
        self.walk(tree.exp)
        self.walk(tree.index)

    def AssIndex(self, tree):
        self.walk(tree.exp)
        self.walk(tree.index)

    def Splice(self, tree):
        if tree.left:
            self.walk(tree.left)
        if tree.right:
            self.walk(tree.right)
        self.walk(tree.exp)

    def Printnl(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)

    def Name(self, tree):  # pylint: disable=C0103
        pass

    def EnvRef(self, tree):  # pylint: disable=C0103
        self.strings.add(tree.name)

    def ObjRef(self, tree):
        self.walk(tree.exp)
        self.strings.add(tree.name)

    def Register(self, tree):  # pylint: disable=C0103
        pass

    def AssName(self, tree):  # pylint: disable=C0103
        pass

    def AssRegister(self, tree):  # pylint: disable=C0103
        pass

    def AssObjRef(self, tree):
        self.walk(tree.exp)
        self.strings.add(tree.name)

    def Add(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Sub(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Mul(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Div(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def UnaryAdd(self, tree):  # pylint: disable=C0103
        self._walk_unary_node(tree)

    def UnarySub(self, tree):  # pylint: disable=C0103
        self._walk_unary_node(tree)

    def FloorDiv(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Mod(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def If(self, tree):  # pylint: disable=C0103
        self.walk(tree.cond)
        self.walk(tree.body)
        if tree.other:
            self.walk(tree.other)

    def Invert(self, tree):  # pylint: disable=C0103
        self._walk_unary_node(tree)

    def Not(self, tree):
        self._walk_unary_node(tree)

    def Bitand(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Bitor(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Bitxor(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def LeftShift(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def RightShift(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Equal(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def LessEqual(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def GreaterEqual(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def NotEqual(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def LessThan(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def GreaterThan(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def While(self, tree):  # pylint: disable=C0103
        self.walk(tree.cond)
        self.walk(tree.body)
