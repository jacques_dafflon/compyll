#!/bin/bash

set -e

#Save current working directory and go to script source
OLD_PATH="$PWD"
OPT="-O2"
cd $( dirname "${BASH_SOURCE[0]}" )

# compile runtime.c
make -C ../clib lib > /dev/null

# use our compiler
if [ $# -eq 0 ]; then
	python ../compile.py test.py > test.ll
fi

# ll to obj
llc $OPT -o test.o -filetype=obj test.ll

# link and compile
gcc -Wall $OPT -o test ../clib/compyll_lib.a test.o -lgc

# run the program
./test

# remove all the crap
if [ $# -eq 0 ]; then
	rm ../*.pyc
fi

# Cd back to current dir
cd "$OLD_PATH"
exit 0
