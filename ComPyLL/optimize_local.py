import logging
import ast
import math

__all__ = ["optimize_local"]


def optimize_local(tree):
    optimizer = Optimizer()
    return optimizer.optimize(tree)


class Optimizer():
    def __init__(self):
        pass

    def optimize(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def Module(self, tree):  # pylint: disable=C0103
        tree.node = self.optimize(tree.node)
        tree.funcs = self.optimize(tree.funcs)
        return tree

    def String(self, tree):
        return tree

    def Stmt(self, tree):  # pylint: disable=C0103
        ret = []
        for stmt in tree.nodes:
            tmp = self.optimize(stmt)
            if type(tmp) is list:
                ret += tmp
            else:
                ret.append(tmp)
        tree.nodes = ret
        return tree

    def Discard(self, tree):  # pylint: disable=C0103
        # TODO: Kill dead code: remove the whole thing if there is no callfunc in the subtree
        tree.exp = self.optimize(tree.exp)
        return tree

    def If(self, tree):  # pylint: disable=C0103
        tree.cond = self.optimize(tree.cond)
        tree.body = self.optimize(tree.body)
        if tree.other:
            tree.other = self.optimize(tree.other)

        if type(tree.cond) is ast.Const:  # if 1: something; else: something_else
            if tree.cond.value:
                return tree.body.nodes
            else:
                if tree.other:
                    return tree.other.nodes
                else:
                    return []

        if type(tree.cond) is ast.Bool and tree.cond:  # if 1==1: something
            return tree.body.nodes

        if type(tree.cond) is ast.Bool and not tree.cond:  # if 1==2: something
            if tree.other:
                return tree.other.nodes
            else:
                return []

        return tree

    def Const(self, tree):  # pylint: disable=C0103
        return tree

    def NoneNode(self, tree):
        return tree

    def CallFunc(self, tree):  # pylint: disable=C0103
        tree.args = map(self.optimize, tree.args)
        return tree

    def Assign(self, tree):  # pylint: disable=C0103
        tree.exp = self.optimize(tree.exp)
        tree.nodes = [self.optimize(node) for node in tree.nodes]
        return tree

    def AugAssign(self, tree):
        tree.node = self.optimize(tree.node)
        tree.exp = self.optimize(tree.exp)
        return tree

    def List(self, tree):
        tree.items = [self.optimize(item) for item in tree.items]
        return tree

    def Index(self, tree):
        tree.exp = self.optimize(tree.exp)
        tree.index = self.optimize(tree.index)
        return tree

    def AssIndex(self, tree):
        tree.exp = self.optimize(tree.exp)
        tree.index = self.optimize(tree.index)
        return tree

    def Splice(self, tree):
        if tree.left:
            tree.left = self.optimize(tree.left)
        if tree.right:
            tree.right = self.optimize(tree.right)
        tree.exp = self.optimize(tree.exp)
        return tree

    def Printnl(self, tree):  # pylint: disable=C0103
        tree.exp = self.optimize(tree.exp)
        return tree

    def Name(self, tree):  # pylint: disable=C0103
        return tree

    def EnvRef(self, tree):  # pylint: disable=C0103
        return tree

    def ObjRef(self, tree):
        return tree

    def Register(self, tree):  # pylint: disable=C0103
        return tree

    def AssName(self, tree):  # pylint: disable=C0103
        return tree

    def AssObjRef(self, tree):
        self.optimize(tree.exp)
        return tree

    def AssRegister(self, tree):  # pylint: disable=C0103
        return tree

    # ==== Arithmetic operations ====
    def Add(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 1 + 2 -> 3
            return ast.Const(tree.left.value + tree.right.value, tree.lineno)

        if type(tree.left) is ast.Const and tree.left.value == 0:  # 0 + x -> x
            return tree.right

        if type(tree.right) is ast.Const and tree.right.value == 0:  # x + 0 -> x
            return tree.left

        return tree

    def Sub(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 - 1 -> 2
            return ast.Const(tree.left.value - tree.right.value, tree.lineno)

        if type(tree.left) is ast.Const and tree.left.value == 0:  # 0 - x -> -x
            return ast.UnarySub(tree.right, tree.lineno)

        if type(tree.right) is ast.Const and tree.right.value == 0:  # x - 0 -> x
            return tree.left

        return tree

    def Mul(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 2 * 3 -> 6
            return ast.Const(tree.left.value * tree.right.value)

        if type(tree.left) is ast.Const:  # 2 * x -> x * 2
            tree.left, tree.right = tree.right, tree.left

        if type(tree.right) is ast.Const:
            n = tree.right.value
            if n == 1:  # x * 1 -> x
                return tree.left

            elif n == 0:
                pass  # TODO handle x * 0 -> 0

        return tree

    def Div(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.right) is ast.Const and tree.right.value == 0:
            logging.error('ZeroDivisionError: division by zero')
            exit(1)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 / 1 -> 2
            return ast.Const(tree.left.value / tree.right.value, tree.lineno)

        if type(tree.right) is ast.Const:
            n = tree.right.value
            if n == 1:  # x / 1 -> x
                return tree.left

            elif (n > 0) and (n & (n - 1) == 0):  # x / 4 -> x >> 2
                return ast.RightShift(tree.left, ast.Const(int(math.log(n, 2))))

        # TODO: handle '0 / x'

        return tree

    def FloorDiv(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.right) is ast.Const and tree.right.value == 0:
            logging.error('ZeroDivisionError: integer division by zero')
            exit(1)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 / 1 -> 2
            return ast.Const(tree.left.value / tree.right.value, tree.lineno)

        if type(tree.right) is ast.Const:
            n = tree.right.value
            if n == 1:  # x / 1 -> x
                return tree.left

            elif (n > 0) and (n & (n - 1) == 0):  # x / 4 -> x >> 2
                return ast.RightShift(tree.left, ast.Const(int(math.log(n, 2))))

        # TODO: handle '0 / x'

        return tree

    def Mod(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.right) is ast.Const and tree.right.value == 0:
            logging.error('ZeroDivisionError: modulo by zero')
            exit(1)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 % 2 -> 1
            return ast.Const(tree.left.value % tree.right.value, tree.lineno)

        # TODO: handle 'x % 1'

        return tree

    def LeftShift(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 << 1 -> 6
            if tree.right.value < 0:
                logging.error('ValueError: negative shift count')
                exit(1)
            return ast.Const(tree.left.value << tree.right.value, tree.lineno)

        if type(tree.right) is ast.Const and tree.right.value == 0:  # x << 0 -> x
            return tree.left

        # TODO: handle '0 << x'

        return tree

    def RightShift(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 >> 1 -> 1
            if tree.right.value < 0:
                logging.error('ValueError: negative shift count')
                exit(1)
            return ast.Const(tree.left.value >> tree.right.value, tree.lineno)

        if type(tree.right) is ast.Const and tree.right.value == 0:  # x >> 0 -> x
            return tree.left

        # TODO: handle '0 >> x'

        return tree

    # ==== Bit operations ====
    # optimizes are handled by the LLVM code, as result optimize depends on operands optimize.
    def Bitand(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 & 1 -> 1
            return ast.Const(tree.left.value & tree.right.value, tree.lineno)

        # TODO: handle 'x & 0'
        # TODO: handle '0 & x'

        return tree

    def Bitor(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 2 | 1 -> 3
            return ast.Const(tree.left.value | tree.right.value, tree.lineno)

        # TODO: handle 'x | -1'
        # TODO: handle '-1 | x'

        return tree

    def Bitxor(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 2 ^ 1 -> 3
            return ast.Const(tree.left.value ^ tree.right.value, tree.lineno)

        if type(tree.right) is ast.Const and tree.right.value == 0:  # x ^ 0 -> x
            return tree.left

        if type(tree.left) is ast.Const and tree.left.value == 0:  # 0 ^ x -> x
            return tree.right

        return tree

    def Or(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 0 or 2 -> 2
            return tree.left if tree.left.value else tree.right

        return tree

    def And(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 0 or 2 -> 2
            return tree.left if not tree.left.value else tree.right

        return tree

    def UnaryAdd(self, tree):  # pylint: disable=C0103
        tree.exp = self.optimize(tree.exp)
        if type(tree.exp) is ast.Const:
            return tree.exp
        return tree

    def UnarySub(self, tree):  # pylint: disable=C0103
        tree.exp = self.optimize(tree.exp)
        if type(tree.exp) is ast.Const:
            return ast.Const(-tree.exp.value, tree.lineno)
        return tree

    def Invert(self, tree):  # pylint: disable=C0103
        tree.exp = self.optimize(tree.exp)
        if type(tree.exp) is ast.Const:
            return ast.Const(~tree.exp.value, tree.lineno)
        return tree

    def Not(self, tree):  # pylint: disable=C0103
        tree.exp = self.optimize(tree.exp)
        if type(tree.exp) is ast.Const:  # inverted because it's a not node
            return ast.Bool(False, tree.lineno) if tree.exp.value else ast.Bool(True, tree.lineno)
        if type(tree.exp) is ast.NoneNode:
            return ast.Bool(True, tree.lineno)
        return tree

    # ==== Boolean Operations ====
    def Equal(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 == 1 -> False
            return ast.Bool(True, tree.lineno) if tree.left.value == tree.right.value else ast.Bool(False, tree.lineno)

        return tree

    def LessEqual(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 == 1 -> False
            return ast.Bool(True, tree.lineno) if tree.left.value <= tree.right.value else ast.Bool(False, tree.lineno)

        return tree

    def GreaterEqual(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 == 1 -> False
            return ast.Bool(True, tree.lineno) if tree.left.value >= tree.right.value else ast.Bool(False, tree.lineno)

        return tree

    def NotEqual(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 == 1 -> False
            return ast.Bool(True, tree.lineno) if tree.left.value != tree.right.value else ast.Bool(False, tree.lineno)

        return tree

    def LessThan(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 == 1 -> False
            return ast.Bool(True, tree.lineno) if tree.left.value < tree.right.value else ast.Bool(False, tree.lineno)

        return tree

    def GreaterThan(self, tree):  # pylint: disable=C0103
        tree.left = self.optimize(tree.left)  # because its bottom-up
        tree.right = self.optimize(tree.right)

        if type(tree.left) is ast.Const and type(tree.right) is ast.Const:  # 3 == 1 -> False
            return ast.Bool(True, tree.lineno) if tree.left.value > tree.right.value else ast.Bool(False, tree.lineno)

        return tree

    def Pass(self, tree):
        return tree

    def Return(self, tree):
        tree.exp = self.optimize(tree.exp)
        return tree

    def While(self, tree):
        tree.cond = self.optimize(tree.cond)
        tree.body = self.optimize(tree.body)
        return tree

    def Function(self, tree):
        tree.body = self.optimize(tree.body)
        return tree

    def MakeClosure(self, tree):
        tree.func = self.optimize(tree.func)
        return tree

    def MakeEnv(self, tree):
        return tree

    def MakeObject(self, tree):
        return tree
