#!/usr/bin/env python
#
# Python lexer.
# pylint: disable=C0103

import logging
import ply.lex as lex

states = (
    ('indent', 'exclusive'),
    ('dedent', 'exclusive'),
    ('comment', 'exclusive'),
    ('main', 'exclusive')
)

reserved = {
    #'del': 'DEL',
    #'for': 'FOR',
    'while': 'WHILE',
    #'continue': 'CONTINUE',
    #'break': 'BREAK',
    #'in': 'IN',
    'if': 'IF',
    'elif': 'ELIF',
    'else': 'ELSE',
    #'import': 'IMPORT',
    #'from': 'FROM',
    #'as': 'AS',
    'and': 'AND',
    'or': 'OR',
    'not': 'NOT',
    #'is': 'IS',
    'class': 'CLASS',
    'def': 'DEF',
    #'global': 'GLOBAL',
    #'try': 'TRY',
    #'except': 'EXCEPT',
    #'finally': 'FINALLY',
    #'raise': 'RAISE',
    'return': 'RETURN',
    #'yield': 'YIELD',
    'pass': 'PASS',
    #'assert': 'ASSERT',
    #'exec': 'EXEC',
    'lambda': 'LAMBDA',
    'print': 'PRINT',
    #'with': 'WITH',
    'None': 'NONE',
}

tokens = [
    'INDENT', 'DEDENT', 'IDENTIFIER', 'NEWLINE',                    # indent, newline, etc
    'OPAREN', 'CPAREN', 'OBRACKET', 'CBRACKET', #'OCURLY', 'CCURLY',  # brackets.
    'STRING', 'INTEGER',                                            # literals
    'PLUS', 'MINUS', 'TIMES',                                       # operators
    'FLOORDIVIDE', 'DIVIDE', 'MOD', #'POWER',
    'LEFTSHIFT', 'RIGHTSHIFT', 'BITAND', 'BITOR', 'BITXOR', 'INVERT',

    'COMMA', 'DOT', 'COLON',

    'EQUALS', 'LESSEREQUAL', 'GREATEREQUAL',                         # comparison
    'NOTEQUAL', 'LESSER', 'GREATER',

    'AUGPLUS', 'AUGMINUS', 'AUGTIMES',                               # augmented operators
    'AUGFLOORDIVIDE', 'AUGDIVIDE', 'AUGMOD', #'AUGPOWER',
    'AUGLEFTSHIFT', 'AUGRIGHTSHIFT', 'AUGBITAND', 'AUGBITOR', 'AUGBITXOR',

    'ASSIGN'
] + list(reserved.values())


t_main_COMMA = r','
t_main_DOT = r'\.'

# operators
t_main_ASSIGN = r'='
t_main_PLUS = r'\+'
t_main_AUGPLUS = r'\+='
t_main_MINUS = r'\-'
t_main_AUGMINUS = r'\-='
t_main_TIMES = r'\*'
t_main_AUGTIMES = r'\*='
#t_main_POWER = r'\*\*'
#t_main_AUGPOWER = r'\*\*='
t_main_FLOORDIVIDE = r'//'
t_main_AUGFLOORDIVIDE = r'//='
t_main_DIVIDE = r'/'
t_main_AUGDIVIDE = r'/='
t_main_MOD = r'%'
t_main_AUGMOD = r'%='
t_main_LEFTSHIFT = r'<<'
t_main_AUGLEFTSHIFT = r'<<='
t_main_RIGHTSHIFT = r'>>'
t_main_AUGRIGHTSHIFT = r'>>='
t_main_BITAND = r'&'
t_main_AUGBITAND = r'&='
t_main_BITOR = r'\|'
t_main_AUGBITOR = r'\|='
t_main_BITXOR = r'\^'
t_main_AUGBITXOR = r'\^='
t_main_INVERT = r'~'
t_main_COLON = r':'

# comparison
t_main_EQUALS = r'=='
t_main_LESSEREQUAL = r'<='
t_main_GREATEREQUAL = r'>='
t_main_NOTEQUAL = r'(!=|<>)'
t_main_LESSER = r'<'
t_main_GREATER = r'>'

t_indent_ignore = ''
t_dedent_ignore = ''


def t_indent_error(t):
    logging.error("INDENT ERROR")


def t_dedent_error(t):
    pass


def t_comment_error(t):
    pass


def t_error(t):
    pass


def t_comment_skip(t):
    r"""[^\n]+"""
    pass


def t_ANY_NEWLINE(t):
    r"""\n+"""
    t.lexer.lineno += len(t.value)
    if len(t.lexer.paren_stack) == 0:
        t.lexer.curr_indent = 0
        if t.lexer.lexstate != 'indent':
            t.lexer.begin('indent')
            return t


def t_main_softnewline(t):
    r"""\\\n"""
    t.lexer.lineno += 1


def t_indent_comment(t):
    r"""\#"""
    t.lexer.begin('comment')


def char_val(c):
    if c == ' ':
        return 1
    elif c == '\t':
        return 8
    else:
        return 0


def t_indent_ws(t):
    r"""[ \t]+"""
    t.lexer.curr_indent += sum(map(char_val, t.value))


def process_indent(lexer):
    cnt = 0
    curr = lexer.curr_indent
    topi = lexer.indents.pop()
    while topi > curr:
        topi = lexer.indents.pop()
        cnt += 1
    lexer.indents.append(topi)
    if topi == curr:
        return -cnt
    if topi < curr:
        if cnt == 0:
            return 1
        # This should not be here but in t_indent_error...
        ie = IndentationError()
        ie.lineno = lexer.lineno
        ie.offset = lexer.lexpos
        ie.text = "dedent"
        raise ie
        #return -cnt


def t_indent_INDENT(t):
    r"""[^ \#\t\n]"""
    t.lexer.lexpos -= 1
    val = process_indent(t.lexer)
    if val == 0:
        t.lexer.begin('main')
    elif val < 0:
        t.lexer.begin('dedent')
        t.lexer.dedent_cnt = -val
    else:
        #ie = IndentationError()
        #ie.lineno = t.lexer.lineno
        #ie.offset = t.lexer.lexpos
        #ie.text = "indent"
        #raise ie
        t.lexer.indents.append(t.lexer.curr_indent)
        t.lexer.begin('main')
        return t


def t_dedent_DEDENT(t):
    r"""."""
    t.lexer.lexpos -= 1
    t.lexer.dedent_cnt -= 1
    if t.lexer.dedent_cnt == 0:
        t.lexer.begin('main')
    return t


invert_paren = {
    '(': ')',
    '[': ']',
    '{': '}'
}


paren_token_name = {
    '(': 'OPAREN',
    ')': 'CPAREN',
    '[': 'OBRACKET',
    ']': 'CBRACKET',
    '{': 'OCURLY',
    '}': 'CCURLY'
}


def t_main_open(t):
    r"""[\(\[\{]"""
    t.lexer.paren_stack.append(invert_paren[t.value])
    t.type = paren_token_name[t.value]
    return t


def t_main_close(t):
    r"""[\)\]\}]"""
    last = t.lexer.paren_stack.pop()
    if last != t.value:
        logging.error("Unmatched", t.value)
        t.lexer.paren_stack.append(last)
    t.type = paren_token_name[t.value]
    return t


def token_override(self):
    t = self.token_()
    if t is None:
        return process_eof(self)
    return t


def process_eof(lexer):
    if len(lexer.indents) == 1:
        return None
    lexer.indents.pop()

    tok = lex.LexToken()
    tok.value = ''
    tok.type = 'DEDENT'
    tok.lineno = lexer.lineno
    tok.lexpos = lexer.lexpos
    tok.lexer = lexer
    return tok


def process_string(string):
    # define me! (optional for P0, but need to do eventually)
    return string


def t_main_STRING_short_single(t):
    r"""(r|R|UR|Ur|U|uR|ur|u|br|bR|b|BR|Br|B)?'[^'\n]*'"""
    t.type = 'STRING'
    return process_string(t)


def t_main_STRING_short_double(t):
    r'''(r|R|UR|Ur|U|uR|ur|u|br|bR|b|BR|Br|B)?"[^"\n]*"'''
    t.type = 'STRING'
    return process_string(t)

literals = ".@,:`;~"

t_main_ignore = ' \t'

tokens += []


def t_main_INTEGER(t):
    r"""[0-9]+"""
    t.value = int(t.value)
    return t


def t_main_IDENTIFIER(t):
    r"""[_a-zA-Z][a-zA-Z_0-9]*"""
    t.type = reserved.get(t.value, 'IDENTIFIER')
    return t


def t_main_error(t):
    pass


def t_main_comment(t):
    """\#"""
    t.lexer.begin('comment')


def compyll_lex():
    lexer = lex.lex(errorlog=None)
    lexer.indents = []
    lexer.indents.append(0)
    lexer.paren_stack = []
    lexer.curr_indent = 0
    lexer.token_ = lexer.token
    lexer.token = (lambda: token_override(lexer))
    lexer.begin('indent')
    #lex.runmain(lexer)

if __name__ == '__main__':
    lexer = lex.lex(errorlog=None)
    lexer.indents = []
    lexer.indents.append(0)
    lexer.paren_stack = []
    lexer.curr_indent = 0
    lexer.token_ = lexer.token
    lexer.token = (lambda: token_override(lexer))
    lexer.begin('indent')
    lex.runmain(lexer)
