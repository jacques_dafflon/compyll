"""
.. module:: code_generator
   :platform: Unix, OSX
   :synopsis: Generate LLVM code.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

"""
import logging
import ast
from variable_generator import generate_register, generate_label
from variable_finder import find_variables
from string_finder import find_strings
from constants import *

__all__ = ["generate_code"]


def generate_code(tree):
    """
    Generates LLVM instructions from a flatten AST.

    Args:
        tree (ast.Module): The module from which CodeGenerator generate_code LLVM instructions.

    Returns:
        LLVM instructions as a multi-lines string.
    """
    code_gen = CodeGenerator()
    return code_gen.generate(tree)[1]


def generate_or_stmt(result, left, right):
    return result + ' = or i64 ' + str(left) + ', ' + str(right) + '\n'


def generate_and_stmt(result, left, right):
    return result + ' = and i64 ' + str(left) + ', ' + str(right) + '\n'


def generate_leftshift_stmt(result, left, right):
    return result + ' = shl i64 ' + str(left) + ', ' + str(right) + '\n'


def generate_rightshift_stmt(result, left, right):
    return result + ' = ashr i64 ' + str(left) + ', ' + str(right) + '\n'


class CodeGenerator():  # pylint: disable=R0904

    def __init__(self):
        # nodes to ignore in the discard
        self.ignore_nodes = [ast.Const, ast.String,  ast.Name, ast.Register, ast.UnarySub, ast.Invert]
        # nodes to wrap with 0-addition in the Assign
        self.simple_nodes = [ast.Const, ast.String, ast.Name, ast.Register, ast.Invert, ast.MakeClosure, ast.MakeObject, ast.List, ast.ObjRef]
        # nodes for which an extra icmp instruction is not necessary
        self.boolean_nodes = [ast.Equal, ast.GreaterEqual, ast.GreaterThan, ast.LessEqual, ast.LessThan, ast.NotEqual]
        self.current_label = "0"

    def generate(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def _generate_variable_load(self, tree, parameters):
        ret = ''
        variables = find_variables(tree, parameters)
        for var in variables:
            ret += var + ' = alloca i64, align 8\n'
        for var in variables:
            if var == '%True':
                ret += 'store i64 ' + str(TRUE_VALUE) + ', i64* %True, align 8\n'
            elif var == '%False':
                ret += 'store i64 ' + str(FALSE_VALUE) + ', i64* %False, align 8\n'
            elif var == '%input':
                tmp1 = generate_register()
                ret += tmp1 + ' = bitcast i64 (i8*)* @input to i8*\n'
                tmp2 = generate_register()
                ret += tmp2 + ' = call i64 @make_closure(i8* ' + tmp1 + ', i64 0)\n'
                ret += 'store i64 ' + tmp2 + ', i64* %input, align 8\n'
            elif var == '%len':
                tmp1 = generate_register()
                ret += tmp1 + ' = bitcast i64 (i64)* @len to i8*\n'
                tmp2 = generate_register()
                ret += tmp2 + ' = call i64 @make_closure(i8* ' + tmp1 + ', i64 1)\n'
                ret += 'store i64 ' + tmp2 + ', i64* %len, align 8\n'
            elif var == '%str':
                tmp1 = generate_register()
                ret += tmp1 + ' = bitcast i64 (i64)* @str to i8*\n'
                tmp2 = generate_register()
                ret += tmp2 + ' = call i64 @make_closure(i8* ' + tmp1 + ', i64 1)\n'
                ret += 'store i64 ' + tmp2 + ', i64* %str, align 8\n'
            else:
                ret += 'store i64 ' + str(UNDECLARED_VAR_VALUE) + ', i64* ' + var + ', align 8\n'
        return ret

    def Module(self, tree):  # pylint: disable=C0103
        ret = ""
        strings = find_strings(tree)
        for string in strings:
            ret += '@str_' + string.encode('hex_codec', 'strict') + \
                   ' = private unnamed_addr constant [' + \
                   str(len(string)+1) + ' x i8] c"' + string + '\\00", align 1\n'
        ret += '\n'

        useless, func_ret = self.generate(tree.funcs)
        ret += func_ret

        ret += 'define i64 @main() nounwind ssp uwtable {\n'
        ret += self._generate_variable_load(tree, ())

        ret += '\n'

        useless, tmp = self.generate(tree.node)
        assert type(tmp) is str, "%r is not a string!" % tmp
        ret += tmp

        ret += 'ret i64 0\n'
        ret += '}\n'
        ret += '\n'
        ret += 'declare void @assert_tag_func(i64, i64)\n'
        ret += '\n'
        ret += 'declare i64 @input(i8*)\n'
        ret += 'declare i64 @len(i64)\n'
        ret += 'declare i64 @str(i64)\n'
        ret += 'declare void @printnl(i64)\n'
        ret += 'declare void @print_raw(i64)\n'
        ret += 'declare i64 @get_elem(i64, i64)\n'
        ret += 'declare void @set_elem(i64, i64, i64)\n'
        ret += 'declare i64 @get_attr(i64, i8*)\n'
        ret += 'declare i64 @get_splice(i64, i64, i64)\n'
        ret += 'declare void @set_attr(i64, i8*, i64)\n'
        ret += '\n'
        ret += 'declare i64 @make_closure(i8*, i64)\n'
        ret += 'declare i64 @make_object()\n'
        ret += 'declare void @hashmap_insert(i8*, i8*, i64)\n'
        ret += 'declare i64 @hashmap_lookup(i8*, i8*)\n'
        ret += '\n'
        ret += 'declare i64 @list_create(i64)\n'
        ret += 'declare void @tlist_append(i8*, i64, i64)\n'
        ret += '\n'
        ret += 'declare i64 @add(i64, i64)\n'
        ret += 'declare i64 @sub(i64, i64)\n'
        ret += 'declare i64 @mul(i64, i64)\n'
        ret += 'declare i64 @_div(i64, i64)\n'
        ret += 'declare i64 @floordiv(i64, i64)\n'
        ret += 'declare i64 @mod(i64, i64)\n'
        ret += 'declare i64 @leftshift(i64, i64)\n'
        ret += 'declare i64 @rightshift(i64, i64)\n'
        ret += 'declare i64 @bitand(i64, i64)\n'
        ret += 'declare i64 @bitor(i64, i64)\n'
        ret += 'declare i64 @bitxor(i64, i64)\n'
        ret += 'declare i64 @equal(i64, i64)\n'
        ret += 'declare i64 @lesserequal(i64, i64)\n'
        ret += 'declare i64 @greaterequal(i64, i64)\n'
        ret += 'declare i64 @notequal(i64, i64)\n'
        ret += 'declare i64 @lessthan(i64, i64)\n'
        ret += 'declare i64 @greaterthan(i64, i64)\n'
        ret += 'declare i64 @not(i64)\n'
        ret += '\n'
        ret += 'declare i64 @get_string_pointer(i8*)\n'
        return '', ret

    def Stmt(self, tree):  # pylint: disable=C0103
        ret = str()
        for stmt in tree.nodes:
            ret += '; ' + str(stmt)
            ret += '\n'
            prev, statement = self.generate(stmt)
            assert type(prev) is str, "%r is not a string!" % prev
            assert type(statement) is str, "%r is not a string!" % statement
            ret += prev
            ret += statement
            ret += '\n'
        return '', ret

    def Discard(self, tree):  # pylint: disable=C0103
        prev, statement = self.generate(tree.exp)
        if tree.exp.__class__ in self.ignore_nodes:
            return prev, ''
        else:
            return prev, statement

    def String(self, tree):
        if type(tree.value) is str:
            hp = generate_register()
            string = 'getelementptr inbounds ([' + str(len(tree.value)+1) + ' x i8]* @str_' + tree.value.encode('hex_codec', 'strict') + ', i64 0, i64 0)'
            prev = hp + ' = call i64 @get_string_pointer(i8* ' + string + ')\n'
            return prev, hp
        else:
            logging.error("This is not a string, whatToDo")
            exit(1)

    def Const(self, tree):  # pylint: disable=C0103
        if type(tree.value) is int:
            return '', str(tree.value)
        else:
            logging.error("Unsupported value: only 64-bit integers are supported!")
            exit(1)

    def CallFunc(self, tree):  # pylint: disable=C0103
        func_type = type(tree.node)
        assert func_type in [ast.Name, ast.Register, ast.EnvRef, ast.ObjRef, ast.MakeClosure], "%r is not a Name!" % tree.node
        func_name = tree.node.func.name if func_type is ast.MakeClosure else tree.node.name
        assert type(func_name) is str, "%r is not a string!"

        prev, var = self.generate(tree.node)
        prev += 'call void @assert_tag_func(i64 ' + var + ', i64 ' + str(len(tree.args)) + ')\n'

        func_ptr_type = 'i64(i8*' + (len(tree.args)*', i64') + ')**'

        p_fp = generate_register()
        p_fp2 = generate_register()
        fp = generate_register()

        p_hp = generate_register()
        p_hp2 = generate_register()
        hp = generate_register()

        # Function pointer
        prev += p_fp + ' = sub i64 ' + var + ', 3\n'
        prev += p_fp2 + ' = inttoptr i64 ' + p_fp + ' to ' + func_ptr_type + '\n'
        prev += fp + ' = load ' + func_ptr_type + ' ' + p_fp2 + ', align 8\n'

        # Environment
        prev += p_hp + ' = add i64 ' + var + ', 5\n'
        prev += p_hp2 + ' = inttoptr i64 ' + p_hp + ' to i8**\n'
        prev += hp + ' = load i8** ' + p_hp2 + ', align 8\n'

        ret = 'call i64 ' + fp + '('
        ret += 'i8* ' + hp
        for param in tree.args:
            prev_arg, val_arg = self.generate(param)
            prev += prev_arg
            ret += ', i64 ' + val_arg

        ret += ') ; call function'

        return prev, ret

    def Return(self, tree):  # pylint: disable=C0103
        prev, var = self.generate(tree.exp)
        return prev, 'ret i64 ' + var

    def Function(self, tree):  # pylint: disable=C0103
        # Reseting labels to 0 for function scope
        label, self.current_label = self.current_label, str(0)
        generate_label.counter = int(self.current_label)

        # ==== Funcion Header ====
        env_tmp = generate_register()
        param_tmp = [generate_register() for a in tree.params]
        for arg in tree.params:
            assert type(arg) is ast.Name
        ret = 'define i64 @' + tree.name + '(i8* ' + env_tmp
        for t in param_tmp:
            ret += ', i64 ' + t
        ret += ') nounwind ssp uwtable {\n'

        # ==== Parameters alloca ====
        ret += '%.env = alloca i8*, align 8\n'
        for arg in tree.params:
            ret += '%' + arg.name + ' = alloca i64, align 8\n'

        # ==== Parameters store ====
        ret += 'store i8* ' + env_tmp + ', i8** %.env, align 8\n'
        for param in range(len(tree.params)):
            ret += 'store i64 ' + param_tmp[param] + ', i64* %' + tree.params[param].name + ', align 8\n'

        # ==== Local variables alloca ====
        ret += self._generate_variable_load(tree.body, map(lambda x: x.name, tree.params))

        # ==== Body ====
        useless, body = self.generate(tree.body)
        ret += body

        ret += '}\n'

        # Putting back current label
        self.current_label = label
        generate_label.counter = int(self.current_label)

        return '', ret

    def MakeClosure(self, tree):  # pylint: disable=C0103
        casted = generate_register()
        prev = casted + ' = bitcast i64 ('
        prev += 'i8*'
        prev += (', i64' * tree.nparams)
        prev += ')* @' + tree.func.name + ' to i8*\n'
        ptr = generate_register()
        prev += ptr + ' = call i64 @make_closure(i8* ' + casted + ', i64 ' + str(tree.nparams) + ')\n'

        if len(tree.env) > 0:
            p_hp = generate_register()
            p_hp2 = generate_register()
            hp = generate_register()

            prev += p_hp + ' = add i64 ' + ptr + ', 5 \n'
            prev += p_hp2 + ' = inttoptr i64 ' + p_hp + ' to i8** \n'
            prev += hp + ' = load i8** ' + p_hp2 + ', align 8 \n'

            for var in tree.env:
                if var.name == tree.assname:
                    value = ptr
                else:
                    prev_var, value = self.generate(var)
                    prev += prev_var
                string = 'getelementptr inbounds ([' + str(len(var.name)+1) + ' x i8]* @str_' + var.name.encode('hex_codec', 'strict') + ', i64 0, i64 0)'
                prev += 'call void @hashmap_insert(i8* ' + hp + ', i8* ' + string + ', i64 ' + value + ')\n'

        return prev, ptr

    def MakeObject(self, tree):
        tmp = generate_register()
        ret = tmp + ' = call i64 @make_object()\n'
        return ret, tmp

    def Assign(self, tree):  # pylint: disable=C0103
        assert type(tree.nodes) is list, "%r is not a list!" % tree.nodes
        assert len(tree.nodes) == 1, "list is greater than 1! Subtree of print not flattened!"

        prev_exp, val_exp = self.generate(tree.exp)
        assert type(prev_exp) is str, "%r is not a string!" % prev_exp
        assert type(val_exp) is str, "%r is not a string!" % val_exp

        if type(tree.nodes[0]) is ast.AssIndex:
            assert tree.exp.__class__ in self.simple_nodes
            ass_idx = tree.nodes[0]
            prev_ass_exp, ass_exp_var = self.generate(ass_idx.exp)
            assert type(prev_ass_exp) is str, "%r is not a string!" % prev_ass_exp
            prev_ass_idx, ass_idx_var = self.generate(ass_idx.index)
            assert type(prev_ass_idx) is str, "%r is not a string!" % prev_ass_idx

            return prev_exp + prev_ass_exp + prev_ass_idx, "call void @set_elem(i64 " \
                + ass_exp_var + ", i64 " + ass_idx_var + ", i64 " + val_exp + ")"

        if type(tree.nodes[0]) is ast.AssObjRef:
            assert tree.exp.__class__ in self.simple_nodes
            assobjref = tree.nodes[0]
            ass_exp_prev, ass_exp_var = self.generate(assobjref.exp)
            key = "i8* getelementptr inbounds ([" + str(len(assobjref.name)+1) + \
                  " x i8]* @str_" + assobjref.name.encode('hex_codec', 'strict') + ", i64 0, i64 0)"
            return prev_exp + ass_exp_prev, "call void @set_attr(i64 " + ass_exp_var + ", " + key + ", i64 " + val_exp + ")"

        val_nodes = self.generate(tree.nodes[0])[1]
        assert type(val_nodes) is str, "%r is not a string!" % val_nodes

        if type(tree.nodes[0]) is ast.AssName:
            assert tree.exp.__class__ in self.simple_nodes, "Found {} instead".format(tree)
            return '', prev_exp + 'store i64 ' + val_exp + ', i64* %' + val_nodes + ', align 8'

        else:
            assert type(tree.nodes[0]) is ast.AssRegister
            if tree.exp.__class__ in self.simple_nodes:  # Unary operations
                return '', prev_exp + val_nodes + ' = add nsw i64 ' + val_exp + ', 0'
            else:
                return '', prev_exp + val_nodes + ' = ' + val_exp

    def If(self, tree):  # pylint: disable=C0103
        assert type(tree.body) is ast.Stmt
        assert tree.other is None or type(tree.other) is ast.Stmt

        cond_prev, cond_var = self.generate(tree.cond)  # condition
        if_label = generate_label()

        current_label, self.current_label = self.current_label, if_label  # update current label for if body
        ignore, body = self.generate(tree.body)                           # if body
        self.current_label = current_label                                # revert current label

        untagged_cond = generate_register()
        cond_prev += generate_rightshift_stmt(untagged_cond, cond_var, TAG_SIZE)

        icmp = generate_register()
        cond_prev += icmp + ' = icmp ne i64 0, ' + untagged_cond + '\n'

        if tree.other:  # Check if there is an else body.
            else_label = generate_label()
            self.current_label = else_label  # reference for predecessors
            ignore, other = self.generate(tree.other)
            next_label = generate_label()       # end of if with an else
            else_code = "".join(["\n", else_label, ":\t\t\t\t; preds = %", current_label, "\n",
                                 other, "br label %", next_label])
            labels = "%{0}, %{1}".format(if_label, else_label)
        else:           # Otherwise just jump to the end.
            else_label = next_label = generate_label()  # end of if if no else
            else_code = ""
            labels = "%{0}, %{1}".format(current_label, if_label)

        code = [
            cond_prev,
            "br i1 ", icmp, ", label %", if_label, ", label %", else_label, "\n",
            "", if_label, ":\t\t\t\t; preds = %", current_label, "\n", body,
            "br label %", next_label,
            else_code,
            "\n", next_label, ":\t\t\t\t; preds = ", labels
        ]
        self.current_label = next_label  # update the current label
        return "", "".join(code)

    def While(self, tree):
        assert type(tree.cond) is ast.Stmt
        assert type(tree.body) is ast.Stmt
        assert type(tree.cond_var) in [ast.Const, ast.Name, ast.Register]

        while_label = generate_label()
        current_label, self.current_label = self.current_label, while_label
        ignore, cond = self.generate(tree.cond)  # Generating code for the condition
        cond_prev, cond_var = self.generate(tree.cond_var)

        untagged_cond = generate_register()
        cond_prev += generate_rightshift_stmt(untagged_cond, cond_var, TAG_SIZE)

        icmp = generate_register()

        body_label = generate_label()
        ignore, body = self.generate(tree.body)
        next_label = generate_label()

        code = ["br label %", while_label, "\n",
                "", while_label, ":\t\t\t\t; preds = %", current_label, ", %", body_label, "\n",
                cond, cond_prev, icmp, " = icmp ne i64 0, ", untagged_cond, "\n",
                "br i1 ", icmp, ", label %", body_label, ", label %", next_label, "\n",
                "", body_label, ":\t\t\t\t ; preds = %", while_label, "\n", body, "br label %", while_label,
                "\n", next_label, ":\t\t\t\t ; preds = %", while_label]
        self.current_label = next_label
        return "", "".join(code)

    def List(self, tree):
        ptr = generate_register()
        code = [ptr, " = call i64 @list_create(i64 ", str(len(tree.items)), ")\n"]
        prev = []
        for item in tree.items:
            prev_item, item_var = self.generate(item)
            prev.append(prev_item)
            code.extend(["call void @tlist_append(i8* null, i64 ", ptr, ", i64 ", item_var, ")\n"])
        prev.extend(code)
        return "".join(prev), ptr

    def Index(self, tree):
        prev_exp, exp_var = self.generate(tree.exp)
        prev_idx, idx_var = self.generate(tree.index)
        return prev_exp + prev_idx, "call i64 @get_elem(i64 " + exp_var + ", i64 " + idx_var + ")"

    def AssIndex(self, tree):
        logging.error("Invalid call: This should not be reachable.")
        exit(1)

    def Splice(self, tree):
        left_prev, left_var = self.generate(tree.left) if tree.left else ('', str(TAG_SIZE))      # NULL ptr
        right_prev, right_var = self.generate(tree.right) if tree.right else ('', str(TAG_SIZE))  # NULL ptr
        exp_prev, exp_var = self.generate(tree.exp)
        return left_prev + right_prev + exp_prev, \
            'call i64 @get_splice(i64 ' + exp_var + ', i64 ' + left_var + ', i64 ' + right_var + ')'

    def AssObjRef(self, tree):
        logging.error("Invalid call: This should not be reachable.")
        exit(1)

    def Printnl(self, tree):  # pylint: disable=C0103
        assert isinstance(tree.exp, ast.Node), "%r is not an ast.Node!" % tree.exp

        prev, val = self.generate(tree.exp)
        assert type(val) is str, "%r is not a string!" % val

        printstr = 'call void @printnl(i64 ' + val + ')'
        return '', prev + printstr

    def Name(self, tree):  # pylint: disable=C0103
        assert type(tree.name) is str, "%r is not a string!" % tree.name
        tmp_var = generate_register()
        load_string = tmp_var + ' = load i64* %' + tree.name + ', align 8\n'
        return load_string, tmp_var

    def EnvRef(self, tree):  # pylint: disable=C0103
        key = "i8* getelementptr inbounds ([" + str(len(tree.name)+1) + " x i8]* @str_" + tree.name.encode('hex_codec', 'strict') + ", i64 0, i64 0)"
        ptr = generate_register()
        prev = ptr + " = load i8** %" + tree.env + ", align 8\n"
        var = generate_register()
        prev += var + " = call i64 @hashmap_lookup(i8* " + ptr + ", " + key + ")\n"
        return prev, var

    def ObjRef(self, tree):
        prev, var = self.generate(tree.exp)
        key = "i8* getelementptr inbounds ([" + str(len(tree.name)+1) + " x i8]* @str_" + tree.name.encode('hex_codec', 'strict') + ", i64 0, i64 0)"
        tmp = generate_register()
        prev += tmp + " = call i64 @get_attr(i64 " + var + ", " + key + ")\n"
        return prev, tmp

    def Register(self, tree):
        assert type(tree.name) is str, "%r is not a string!" % tree.name
        return '', tree.name

    def AssName(self, tree):  # pylint: disable=C0103
        assert type(tree.name) is str, "%r is not a string!" % tree.name
        return '', tree.name

    def AssRegister(self, tree):
        assert type(tree.name) is str, "%r is not a string!" % tree.name
        return '', tree.name

    def _generate_binary_operation(self, tree, operation):
        left_prev, left_var = self.generate(tree.left)
        right_prev, right_var = self.generate(tree.right)
        return left_prev + right_prev, 'call i64 @' + operation + '(i64 ' + left_var + ', i64 ' + right_var + ')'

    def Add(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'add')

    def Sub(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'sub')

    def Mul(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'mul')

    def Div(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, '_div')

    def FloorDiv(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'floordiv')

    def Mod(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'mod')

    def Bitand(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'bitand')

    def Bitor(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'bitor')

    def Bitxor(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'bitxor')

    def LeftShift(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'leftshift')

    def RightShift(self, tree):  # pylint: disable=C0103
        return self._generate_binary_operation(tree, 'rightshift')

    def UnaryAdd(self, tree):  # pylint: disable=C0103
        prev, var = self.generate(tree.exp)
        return prev, 'call i64 @add(i64 ' + var + ', i64 0)'

    def UnarySub(self, tree):  # pylint: disable=C0103
        prev, var = self.generate(tree.exp)
        return prev, 'call i64 @sub(i64 0, i64 ' + var + ')'

    def Invert(self, tree):  # pylint: disable=C0103
        prev, var = self.generate(tree.exp)
        tmp = generate_register()
        prev += tmp + ' = call i64 @bitxor(i64 ' + var + ', i64 ' + str(-1 << TAG_SIZE) + ')\n'
        return prev, tmp

    def Not(self, tree):
        prev, var = self.generate(tree.exp)
        return prev, 'call i64 @not(i64 ' + var + ')'

    def Equal(self, tree):
        return self._generate_binary_operation(tree, 'equal')

    def LessEqual(self, tree):
        return self._generate_binary_operation(tree, 'lesserequal')

    def GreaterEqual(self, tree):
        return self._generate_binary_operation(tree, 'greaterequal')

    def NotEqual(self, tree):
        return self._generate_binary_operation(tree, 'notequal')

    def LessThan(self, tree):
        return self._generate_binary_operation(tree, 'lessthan')

    def GreaterThan(self, tree):
        return self._generate_binary_operation(tree, 'greaterthan')
