"""
.. module:: variable_finder
   :platform: Unix, OSX
   :synopsis: Look for valid Python varaibles

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

The variable finder module goes through an AST to look for and return valid Python variable names.
"""
import logging
import ast

__all__ = ["find_variables"]


def find_variables(tree, parameters):
    """Explore an ASt to find and retrieve valid Python variables.

    Args:
        tree (ast.Node): The root of the tree to search

    Returns:
        list: The list of valid Python variables.
    """
    var_finder = VariableFinder(parameters)

    # change list a bit
    var_finder.walk(tree)
    variables = var_finder.vars
    return map(lambda var: "%" + var, variables)

class VariableFinder():  # pylint: disable=R0904

    def __init__(self, parameters):
        self.vars = set()
        self.parameters = parameters
        self.builtins = ["True", "False", "input", "len", "str"]

    def walk(self, tree):
        return getattr(self, tree.__class__.__name__)(tree)

    def _walk_binary_node(self, node):
        self.walk(node.left)
        self.walk(node.right)

    def _walk_unary_node(self, node):
        self.walk(node.exp)

    def String(self, tree):
        pass

    def Return(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)

    def Module(self, tree):  # pylint: disable=C0103
        self.walk(tree.node)

    def Stmt(self, tree):  # pylint: disable=C0103
        for stmt in tree.nodes:
            self.walk(stmt)

    def Discard(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)

    def Const(self, tree):  # pylint: disable=C0103
        pass

    def CallFunc(self, tree):  # pylint: disable=C0103
        self.walk(tree.node)
        for arg in tree.args:
            self.walk(arg)

    def MakeClosure(self, tree):  # pylint: disable=C0103
        for free_variable in tree.env:
            self.walk(free_variable)

    def MakeObject(self, tree):
        pass

    def Function(self, tree):  # pylint: disable=C0103
        pass

    def Assign(self, tree):  # pylint: disable=C0103
        if type(tree.exp) is ast.MakeClosure:
            for stmt in tree.nodes:
                self.walk(stmt)
            self.walk(tree.exp)
        else:
            self.walk(tree.exp)
            for stmt in tree.nodes:
                self.walk(stmt)

    def List(self, tree):
        for item in tree.items:
            self.walk(item)

    def Index(self, tree):
        self.walk(tree.exp)
        self.walk(tree.index)

    def AssIndex(self, tree):
        self.walk(tree.exp)
        self.walk(tree.index)

    def Splice(self, tree):
        if tree.left:
            self.walk(tree.left)
        if tree.right:
            self.walk(tree.right)
        self.walk(tree.exp)

    def Printnl(self, tree):  # pylint: disable=C0103
        self.walk(tree.exp)

    def Name(self, tree):  # pylint: disable=C0103
        if tree.name not in self.vars and tree.name not in self.parameters:
            if tree.name in self.builtins:
                self.vars.add(tree.name)
            else:
                logging.error("Variable %s referenced before declaration!", tree.name)
                exit(1)

    def EnvRef(self, tree):  # pylint: disable=C0103
        pass

    def ObjRef(self, tree):
        pass

    def Register(self, tree):  # pylint: disable=C0103
        pass

    def AssName(self, tree):  # pylint: disable=C0103
        if tree.name not in self.parameters:
            self.vars.add(tree.name)

    def AssObjRef(self, tree):
        self.walk(tree.exp)

    def AssRegister(self, tree):  # pylint: disable=C0103
        pass

    def Add(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Sub(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Mul(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Div(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def UnaryAdd(self, tree):  # pylint: disable=C0103
        self._walk_unary_node(tree)

    def UnarySub(self, tree):  # pylint: disable=C0103
        self._walk_unary_node(tree)

    def FloorDiv(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Mod(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def If(self, tree):  # pylint: disable=C0103
        self.walk(tree.cond)
        self.walk(tree.body)
        if tree.other:
            self.walk(tree.other)

    def Invert(self, tree):  # pylint: disable=C0103
        self._walk_unary_node(tree)

    def Not(self, tree):
        self._walk_unary_node(tree)

    def Bitand(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Bitor(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Bitxor(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def LeftShift(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def RightShift(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def Equal(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def LessEqual(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def GreaterEqual(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def NotEqual(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def LessThan(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def GreaterThan(self, tree):  # pylint: disable=C0103
        self._walk_binary_node(tree)

    def While(self, tree):  # pylint: disable=C0103
        self.walk(tree.cond)
        self.walk(tree.cond_var)
        self.walk(tree.body)

