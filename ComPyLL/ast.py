"""
.. module:: ast
   :platform: Unix, OSX
   :synopsis: Python abstract syntax tree nodes definitions.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

The ast module contains the definitions of all the AST nodes used by the compiler.
"""

import logging

__all__ = ["Add", "And", "AssName", "AssRegister", "Assign", "AugAssign", "Bitand", "Bitor", "AssIndex", "AssObjRef", "Index",
           "Bitxor", "Bool", "CallFunc", "Class", "Const", "ConvertedLambda", "Discard", "Div", "Equal", "EnvRef", "FloorDiv",
           "Function", "GreaterEqual", "GreaterThan", "If", "Invert", "Lambda", "LeftShift", "LessEqual", "LessThan", "List",
           "MakeClosure", "Mod", "Module", "Mul", "Name", "NoneNode", "Not", "NotEqual", "ObjRef", "Or", "Pass", "Printnl",
           "Register", "Return", "RightShift", "Stmt", "String", "Sub", "Splice", "UnaryAdd", "UnarySub", "While"]

class Node(object):
    """Generic node in the AST

    This class is a super class and should be considered abstract.
    It is a generic and basic representation of a node from an AST.

    Attributes:
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, lineno):
        self._lineno = None
        self.lineno = lineno

    @property
    def lineno(self):  # pylint: disable=C0111
        return self._lineno

    @lineno.setter
    def lineno(self, number):  # pylint: disable=C0111
        assert number is None or type(number) is int
        self._lineno = number


class BinaryNode(Node):
    """Node representing an operation between two operands

    It holds the operation and both the left and right operand.

    Attributes:
        left (ast.Node): Left operand of the binary operation.
        right (ast.Node): Right operand of the binary operation.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno):
        self._left = None
        self._right = None
        self.left = left
        self.right = right
        super(BinaryNode, self).__init__(lineno)

    @property
    def left(self):  # pylint: disable=C0111
        return self._left

    @left.setter
    def left(self, node):  # pylint: disable=C0111
        assert isinstance(node, Node)
        self._left = node

    @property
    def right(self):  # pylint: disable=C0111
        return self._right

    @right.setter
    def right(self, node):  # pylint: disable=C0111
        assert isinstance(node, Node)
        self._right = node

    def print_operands(self):  # pylint: disable=C0111
        return "({0}, {1})".format(repr(self.left), repr(self.right))

    def __repr__(self):
        return self.__class__.__name__ + self.print_operands()


class ExpNode(Node):
    """Node representing a generic expression.

    It holds a generic expression.

    Attributes:
        exp (ast.Node): Expression represented by the node.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, exp, lineno):
        self._exp = None
        self.exp = exp
        super(ExpNode, self).__init__(lineno)

    @property
    def exp(self):  # pylint: disable=C0111
        return self._exp

    @exp.setter
    def exp(self, exp):  # pylint: disable=C0111
        assert isinstance(exp, Node), "Found {} instead: {}".format(type(exp), exp)
        self._exp = exp

    def print_exp(self):  # pylint: disable=C0111
        return "({0})".format(repr(self.exp))

    def __repr__(self):
        return self.__class__.__name__ + self.print_exp()


class Add(BinaryNode):
    """Add operation between two operands.

    Node representing an addition operation (``'+'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Add, self).__init__(left, right, lineno)


class And(BinaryNode):
    """And operation between two operands.

    Node representing an and (``'and'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(And, self).__init__(left, right, lineno)


class AssName(Node):
    """Name being assigned a value.

    Node representing a name (variable) whose getting a variable assigned. Usually part of an ``ast.Assign`` node.

    Attributes:
        name (str): Name of the variable.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, name, lineno=None):
        self._name = None
        self.name = name
        super(AssName, self).__init__(lineno)

    @property
    def name(self):  # pylint: disable=C0111
        return self._name

    @name.setter
    def name(self, name):  # pylint: disable=C0111
        assert type(name) is str
        self._name = name

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.name))


class AssObjRef(ExpNode):
    def __init__(self, exp, name, lineno=None):
        self._name = None
        self.name = name
        super(AssObjRef, self).__init__(exp, lineno)

    @property
    def name(self):  # pylint: disable=C0111
        return self._name

    @name.setter
    def name(self, name):  # pylint: disable=C0111
        assert type(name) is str
        self._name = name

    def __repr__(self):
        return self.__class__.__name__ + "({0}, {1})".format(repr(self.exp), repr(self.name))


class AssRegister(Node):
    """Name being assigned a value.

    Node representing a name (variable) whose getting a variable assigned. Usually part of an ``ast.Assign`` node.
    Eventhough it is not supported, the AssName node also handle the deletion of a name through the use of the ``flag``
    attribute which is either ``'OP_ASSIGN'`` for assignment or ``'OP_DELETE'`` for deletion.

    Example: In the following assignment: ``a=3``, the varaible ``a`` is represented by an ``AssName`` node such as
    ``AssName("a", 'OP_ASSIGN')`` where ``"a"`` is the ``name`` and ``'OP_ASSIGN'`` is the ``flag``.

    Attributes:
        name (str): Name of the variable.
        flag (str): Inidicate if the name is being assigned a value or if the name is being deleted
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, name, lineno=None):
        self._name = None
        self._flag = None
        self.name = name
        super(AssRegister, self).__init__(lineno)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is str
        self._name = name

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.name))


class AssIndex(ExpNode):
    def __init__(self, exp, index, lineno=None):
        self._index = None
        self.index = index
        super(AssIndex, self).__init__(exp, lineno)

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, index):
        assert isinstance(index, Node)
        self._index = index

    def __repr__(self):
        return self.__class__.__name__ + "({}, {})".format(repr(self.exp), repr(self.index))


class Assign(ExpNode):
    """Assignment to one or more varaibles.

    Node representing the assignment of an expression to one or more variables.

    Example: ``a=3`` is an assignment represented by this node, where the ``a` is an ``AssName`` node and the ``3`` is
    a ``Const`` node.

    Attributes:
        nodes(list of ast.Node): List of nodes to which the expression is assigned. Usually is ``ast.AssName``.
        exp (ast.Node): The expression to assign  to the nodes.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, nodes, exp, lineno=None):
        self._nodes = None
        self.nodes = nodes
        super(Assign, self).__init__(exp, lineno)

    @property
    def nodes(self):  # pylint: disable=C0111
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):  # pylint: disable=C0111
        assert type(nodes) is list
        for node in nodes:
            assert type(node) in [AssName, AssRegister, AssObjRef, AssIndex], \
                "Found {} instead: {}".format(node.__class__.__name__, node)
        self._nodes = nodes

    def __repr__(self):
        return self.__class__.__name__ + "({0}, {1})".format(repr(self.nodes), repr(self.exp))


class AugAssign(ExpNode):
    """Augmented assignment of a node by an expression.

    Node representing an augmented assignment (or compound assignment) of a node (usually ``ast.Name``) by an
    expression (``exp``). The type of augment (``opr``) is indicated as a string and should be among the following:
    ``+=``, ``-=``, ``*=``, ``/=``, ``//``, ``%=``, ``&=``, ``|=``, ``^=``, ``&=``, ``>>``, ``<<``.

    Example: ``a+=3`` represent an Augmented assign with ``node=Name('a')``, ``opr="+="`` and ``exp=Const('3')``

    Attributes:
        node (ast.Node): Node being augmented.
        opr (str): Operator to be used to augment the node.
        exp (ast.Node): Expression by which to augment the node.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, node, opr, exp, lineno=None):
        self._node = None
        self._opr = None
        self.node = node
        self.opr = opr
        super(AugAssign, self).__init__(exp, lineno)

    @property
    def node(self):  # pylint: disable=C0111
        return self._node

    @node.setter
    def node(self, node):  # pylint: disable=C0111
        assert type(node) in [Name, Index, ObjRef], "Found " + str(node)+ " instead"
        self._node = node

    @property
    def opr(self):  # pylint: disable=C0111
        return self._opr

    @opr.setter
    def opr(self, opr):  # pylint: disable=C0111
        assert type(opr) is str
        self._opr = opr

    def __repr__(self):
        return self.__class__.__name__ + "({0}, {1}, {2})".format(repr(self.node), repr(self.opr), repr(self.exp))


class Bitand(BinaryNode):
    """Bitwise AND operation between two operands.

    Node representing a bitwise AND operation (``'&'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Bitand, self).__init__(left, right, lineno)


class Bitor(BinaryNode):
    """Bitwise OR operation between two operands.

    Node representing a bitwise OR operation (``'|'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Bitor, self).__init__(left, right, lineno)


class Bitxor(BinaryNode):
    """Bitwise XOR operation between two operands.

    Node representing a bitwise XOR operation (``'^'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Bitxor, self).__init__(left, right, lineno)


class Bool(Node):
    """Representation of a boolean value.

    Node representing a boolean value (``True`` or ``False``).

    Attributes:
        value (bool): The boolean value of the node.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, value, lineno=None):
        self._value = None
        self.value = value
        super(Bool, self).__init__(lineno)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = bool(value)

    def __nonzero__(self):
        return self.value

    def __repr__(self):
        return repr(self.value)


class CallFunc(Node):
    """Node representing a function call.

    This node represents a function call to a generic function. For now, only the built-in function input is supported.

    Attributes:
        node (ast.Name): Holds the name of the function.
        args (list): List of the function arguments.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, node, args, lineno=None):
        self._node = None
        self._args = None
        self._star_args = None
        self._dstar_args = None
        self.node = node
        self.args = args
        super(CallFunc, self).__init__(lineno)

    @property
    def node(self):  # pylint: disable=C0111
        return self._node

    @node.setter
    def node(self, node):  # pylint: disable=C0111
        assert isinstance(node, Node)
        self._node = node

    @property
    def args(self):  # pylint: disable=C0111
        return self._args

    @args.setter
    def args(self, args):  # pylint: disable=C0111
        assert type(args) is list
        for arg in args:
            assert isinstance(arg, Node)
        self._args = args

    def __repr__(self):
        return self.__class__.__name__ + "({0}, {1})".format(self.node, repr(self.args))


class Class(Node):
    """
    Class
    """
    def __init__(self, name, superclass, body, lineno=None):
        super(Class, self).__init__(lineno)
        self._name = None
        self._superclass = None
        self._body = None
        self.name = name
        self.superclass = superclass
        self.body = body

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is Name
        self._name = name

    @property
    def superclass(self):
        return self._superclass

    @superclass.setter
    def superclass(self, superclass):
        assert superclass is None or type(superclass) is Name
        self._superclass = superclass

    @property
    def body(self):
        return self._body

    @body.setter
    def body(self, body):
        assert type(body) is Stmt
        self._body = body

    def __repr__(self):
        return self.__class__.__name__ + "({0}, {1}, {2})".format(self.name, self.superclass, self.body)


class Const(Node):
    """Constant or value of primitive type.

    Node representing a constant or value of a primitive type. So far, only integers are supported.

    Attributes:
        value (int): The value of the constant.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, value, lineno=None):
        self._value = None
        self.value = value
        super(Const, self).__init__(lineno)

    @property
    def value(self):  # pylint: disable=C0111
        return self._value

    @value.setter
    def value(self, value):  # pylint: disable=C0111
        assert type(value) is int
        self._value = value

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.value))


class ConvertedLambda(ExpNode):
    """Node representing a lambda converted for a closure.

    Node representing the conversion of a lambda with an enviornment, used in a closure.

    Attributes:
        name (str): name of the function.
        params ([ast.Name]): List of ``ast.Name`` nodes representing the parameters fo the lambda.
        exp (ast.Stmt): Body of the lambda.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, name, params, exp, lineno=None):
        super(ConvertedLambda, self).__init__(exp, lineno)
        self._name = None
        self._params = None
        self._name = name
        self.params = params

    def __repr__(self):
        return self.__class__.__name__ + "({}, {})".format(repr(self.params), repr(self.exp))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is str
        self._name = name

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, params):
        assert type(params) is list
        self._params = params


class Discard(ExpNode):
    """Node representing a discard statement.

    This node represents a discard statement, that is not an assignment.
    For example, ``x+2`` is a discard statement composed of an addition between a variable and a constant.

    Attributes:
        exp: (ast.Node): The expression composing the discard statement.
    """
    def __init__(self, exp, lineno=None):
        super(Discard, self).__init__(exp, lineno)


class Div(BinaryNode):
    """Division operation between two operands.

    Node representing a division operation (``'/'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Div, self).__init__(left, right, lineno)


class Equal(BinaryNode):
    """Equal operation between two operands.

    Node representing an equality comparison (``'=='``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Equal, self).__init__(left, right, lineno)


class EnvRef(Node):
    def __init__(self, env, name, lineno=None):
        super(EnvRef, self).__init__(lineno)
        self._env = None
        self._name = None
        self.env = env
        self.name = name

    def __repr__(self):
        return self.__class__.__name__ + "({}, {})".format(repr(self.env), repr(self.name))

    @property
    def env(self):
        return self._env

    @env.setter
    def env(self, env):
        assert type(env) is str
        self._env = env

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is str
        self._name = name


class FloorDiv(BinaryNode):
    """Floor division operation between two operands.

    Node representing a floor division operation (``'//'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(FloorDiv, self).__init__(left, right, lineno)


class Function(Node):
    """Function definitions.

    Node representing the defition of a function.

    Attributes:
        name (ast.Name): Name of the function.
        params (list): List of the name of arguments.
        body (ast.Stmt): Body of the funnction.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, name, params, body, lineno=None):
        super(Function, self).__init__(lineno)
        self._name = None
        self._params = None
        self._body = None
        self.name = name
        self.params = params
        self.body = body

    def __repr__(self):
        return self.__class__.__name__ + "({}, {}, {})".format(repr(self.name), repr(self.params), repr(self.body))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is str
        self._name = name

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, params):
        assert type(params) is list
        self._params = params

    @property
    def body(self):
        return self._body

    @body.setter
    def body(self, code):
        assert type(code) is Stmt
        self._body = code


class GreaterEqual(BinaryNode):
    """Greater than operation between two operands.

    Node representing a greater or equal comparison (``'>='``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(GreaterEqual, self).__init__(left, right, lineno)


class GreaterThan(BinaryNode):
    """Greater than operation between two operands.

    Node representing a less than comparison (``'>'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(GreaterThan, self).__init__(left, right, lineno)


class If(Node):
    """If block

    Node representing an if block with a condition, a body and an else clause.
    In case fo nested ifs or ``elif``, the second if is at the begining of the
    else clause.

    Attributes:
        cond(ast.exp): Expression for the condition of the if.
        body(ast.Stmt): Statment containing the body of the if.
        other(ast.Stmt): Statement containing the body of the else clause.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, cond, body, other, lineno=None):
        self._cond = None
        self._body = None
        self._other = None
        self.cond = cond
        self.body = body
        self.other = other
        super(If, self).__init__(lineno)
        
    def __repr__(self):
        return self.__class__.__name__ + "({}, {}, {})".format(repr(self.cond), repr(self.body), repr(self.other))

    @property
    def cond(self):
        return self._cond

    @cond.setter
    def cond(self, cond):
        assert isinstance(cond, Node)
        self._cond = cond

    @property
    def body(self):
        return self._body

    @body.setter
    def body(self, body):
        assert type(body) is Stmt
        self._body = body

    @property
    def other(self):
        return self._other

    @other.setter
    def other(self, other):
        assert other is None or type(other) is Stmt
        self._other = other


class Invert(ExpNode):
    """Bitwise inversion of an expresion.

    Node representing the bitwise inversion (``'~'``) of a node, only integers are supported. The operation is
    equivalent to ``-(x+1)``.

    Attributes:
        exp (ast.Node): Expression resulting in an integer value to be inverted.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, exp, lineno=None):
        super(Invert, self).__init__(exp, lineno)


class Lambda(ExpNode):
    def __init__(self, params, exp, lineno=None):
        super(Lambda, self).__init__(exp, lineno)
        self._params = None
        self.params = params

    def __repr__(self):
        return self.__class__.__name__ + "({}, {})".format(repr(self.params), repr(self.exp))

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, params):
        assert type(params) is list
        for param in params:
            assert type(param) is Name
        self._params = params


class LeftShift(BinaryNode):
    """Left shift operation between two operands.

    Node representing a left shift operation (``'<<'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(LeftShift, self).__init__(left, right, lineno)


class LessEqual(BinaryNode):
    """Less or equal operation between two operands.

    Node representing a less or equal comparison (``'<='``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(LessEqual, self).__init__(left, right, lineno)


class LessThan(BinaryNode):
    """Less than operation between two operands.

    Node representing a less than comparison (``'<'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(LessThan, self).__init__(left, right, lineno)


class List(Node):
    def __init__(self, items, lineno=None):
        super(List, self).__init__(lineno)
        self._items = None
        self.items = items

    def __repr__(self, ):
        return repr(self.items)

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, items):
        assert type(items) is list
        for item in items:
            assert isinstance(item, Node)
        self._items = items

class MakeClosure(Node):
    def __init__(self, func, nparams, env, assname, lineno=None):
        super(MakeClosure, self).__init__(lineno)
        self._func = None
        self._nparams = None
        self._env = None
        self._assname = None
        self.func = func
        self.nparams = nparams
        self.env = env
        self.assname = assname

    def __repr__(self):
        return self.__class__.__name__ + "({}, {}, {})".format(repr(self.func), str(self.nparams), repr(self.env))

    @property
    def func(self):
        return self._func

    @func.setter
    def func(self, fun):
        assert type(fun) in [ConvertedLambda, Name]
        self._func = fun

    @property
    def nparams(self):
        return self._nparams

    @nparams.setter
    def nparams(self, nparams):
        assert type(nparams) is int
        self._nparams = nparams

    @property
    def env(self):
        return self._env

    @env.setter
    def env(self, env):
        assert type(env) is list
        self._env = env

    @property
    def assname(self):
        return self._assname

    @assname.setter
    def assname(self, assname):
        assert type(assname) is str
        self._assname = assname


class MakeObject(Node):
    """
    Represents the act of calling runtime to get a new object or hashmap'
    """
    def __init__(self, lineno=None):
        super(MakeObject, self).__init__(lineno)

    def __repr__(self):
        return self.__class__.__name__


class Mod(BinaryNode):
    """Modullo operation between two operands.

    Node representing a modulo operation (``'%'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Mod, self).__init__(left, right, lineno)


class Module(Node):
    """Node representing a module.

    This node represent a module. This is generally the root of the AST. It contains the documentation and a statement
    node (ast.Stmt) which holds the statements of the module.

    Attributes:
        doc (str): documentation of the module.
        node (ast.Stmt): The statement node holding the list of statements in the module.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, node, funcs=None, lineno=None):
        self._node = None
        self._funcs = None
        self.node = node
        self.funcs = funcs
        super(Module, self).__init__(lineno)

    @property
    def node(self):  # pylint: disable=C0111
        return self._node

    @node.setter
    def node(self, node):  # pylint: disable=C0111
        assert type(node) is Stmt
        self._node = node

    @property
    def funcs(self):
        return self._funcs

    @funcs.setter
    def funcs(self, funcs):
        assert funcs is None or type(funcs) is Stmt
        self._funcs = funcs

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.node))


class Mul(BinaryNode):
    """Multiplication operation between two operands.

    Node representing a multiplication operation (``'*'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Mul, self).__init__(left, right, lineno)


class Name(Node):
    """Name of a variable.

    Node representing the name of a variable.

    Attributes:
        name (str): The name of the variable.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, name, lineno=None):
        self._name = None
        self.name = name
        super(Name, self).__init__(lineno)

    @property
    def name(self):  # pylint: disable=C0111
        return self._name

    @name.setter
    def name(self, name):  # pylint: disable=C0111
        assert type(name) is str
        self._name = name

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.name))


class NoneNode(Node):
    def __init__(self, lineno=None):
        super(NoneNode, self).__init__(lineno)

    def __repr__(self):
        return self.__class__.__name__


class Not(ExpNode):
    """Negation of an expresion.

    Node representing the negation of an expression (``'not'``).

    Attributes:
        exp (ast.exp): Expression being negated.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, exp, lineno=None):
        super(Not, self).__init__(exp, lineno)


class NotEqual(BinaryNode):
    """Not equal operation between two operands.

    Node representing a not equal comparison (``'!='``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(NotEqual, self).__init__(left, right, lineno)


class ObjRef(ExpNode):
    def __init__(self, exp, name, lineno=None):
        super(ObjRef, self).__init__(exp, lineno)
        self._name = None
        self.name = name

    def __repr__(self):
        return self.__class__.__name__ + "({}, {})".format(repr(self.exp), repr(self.name))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is str
        self._name = name


class Or(BinaryNode):
    """Or operation between two operands.

    Node representing an or (``'or'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Or, self).__init__(left, right, lineno)


class Pass(Node):
    """
    fuck documentation
    """
    def __init__(self, lineno=None):
        super(Pass, self).__init__(lineno)

    def __repr__(self):
        return self.__class__.__name__


class Printnl(ExpNode):
    """Node representing a print statement.

    This node represent a simple print statement which for now only support printing of a single node. (Unlike Python's
    default print statement.)

    Attributes:
        exp (ast.Node): The node to be printed.
        dest (str): The file where to print. Usually ``stdout``.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, exp, lineno=None):
        super(Printnl, self).__init__(exp, lineno)

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.exp))


class Register(Node):
    """Name of a temporary variable.

    Node representing the name of a variable.

    Attributes:
        name (str): The name of the variable.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, name, lineno=None):
        self._name = None
        self.name = name
        super(Register, self).__init__(lineno)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        assert type(name) is str
        self._name = name

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.name))


class Return(ExpNode):
    def __init__(self, exp, lineno=None):
        super(Return, self).__init__(exp, lineno)


class RightShift(BinaryNode):
    """Right shift operation between two operands.

    Node representing a right shift operation (``'>>'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(RightShift, self).__init__(left, right, lineno)


class Stmt(Node):
    """Statment node

    This node holds a list of statement. It is generally enclosed in a module and it contains a list of all the
    statements in siad module.

    Attributes:
        nodes (list): List of all the statements.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, nodes, lineno=None):
        self._nodes = None
        self.nodes = nodes
        super(Stmt, self).__init__(lineno)

    @property
    def nodes(self):  # pylint: disable=C0111
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):  # pylint: disable=C0111
        assert type(nodes) is list
        for node in nodes:
            assert isinstance(node, Node)
        self._nodes = nodes

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.nodes))


class String(Node):
    """A String. What a surprise.

    Node representing a  value of a string type.

    Attributes:
        value (str): The value of the string.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, value, lineno=None):
        self._value = None
        self.value = value
        super(String, self).__init__(lineno)

    @property
    def value(self):  # pylint: disable=C0111
        return self._value

    @value.setter
    def value(self, value):  # pylint: disable=C0111
        assert type(value) is str
        self._value = value

    def __repr__(self):
        return self.__class__.__name__ + "({0})".format(repr(self.value))



class Sub(BinaryNode):
    """Subtraction operation between two operands.

    Node representing a subtraction operation (``'-'``) between two operands.

    Attributes:
        left (ast.Node): Left operand.
        right (ast.Node): Right operand.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, left, right, lineno=None):
        super(Sub, self).__init__(left, right, lineno)


class Index(ExpNode):
    """
    Represents something like x[1]
    exp is the expression to be subscripted
    index is the index (expression)
    """
    def __init__(self, exp, index, lineno=None):
        super(Index, self).__init__(exp, lineno)
        self._index = None
        self.index = index

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, index):
        assert isinstance(index, Node)
        self._index = index

    def __repr__(self):
        return self.__class__.__name__ + "({}, {})".format(repr(self.exp), repr(self.index))


class Splice(ExpNode):
    """
    Represents soemthing like x[1:2] or x[:3] or x[2:] or x[:]
    exp is expression to be spliced
    left is the left part of the index
    right is the right part of the index
    """
    def __init__(self, exp, left, right, lineno=None):
        super(Splice, self).__init__(exp, lineno)
        self._left = None
        self._right = None
        self.left = left
        self.right = right

    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, left):
        assert left is None or isinstance(left, Node)
        self._left = left

    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, right):
        assert right is None or isinstance(right, Node)
        self._right = right

    def __repr__(self):
        return self.__class__.__name__ + "({}, [{}:{}])".format(repr(self.exp), repr(self.left), repr(self.right))


class UnaryAdd(ExpNode):
    """Unary addition of an expresion.

    Node representing the unary addition of a node (``'+node'``), not to be confuse with the traditional addition which
    supports two operands, the unary addition only supoprts a single operand to the right of the ``+`` symbol.

    Attributes:
        exp (ast.exp): Expression receiving the unary add.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, exp, lineno=None):
        super(UnaryAdd, self).__init__(exp, lineno)


class UnarySub(ExpNode):
    """Unary subtraction of an expresion.

    Node representing the unary subtraction of a node (``'-node'``), not to be confuse with the traditional subtraction
    which supports two operands, the unary subtraction only supoprts a single operand to the right of the ``-`` symbol.
    The unary subtraction has for effect to inverse the sign of the expression.

    Attributes:
        exp (ast.exp): Expression receiving the unary subtraction.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, exp, lineno=None):
        super(UnarySub, self).__init__(exp, lineno)


class While(Node):
    """While block

    Node representing a while block with a condition and a body.
    Note: The else clause of the while is not supported.

    Attributes:
        cond(ast.exp): Expression for the condition of the while.
        body(ast.Stmt): Statment containing the body of the while loop.
        lineno (int): The line in the source code matching this node in the AST.
    """
    def __init__(self, cond, body, lineno=None):
        self._cond = None
        self._body = None
        self.cond = cond
        self.body = body
        super(While, self).__init__(lineno)

    def __repr__(self):
        return self.__class__.__name__ + "({0}, {1})".format(repr(self.cond), repr(self.body))

    @property
    def cond(self):
        return self._cond

    @cond.setter
    def cond(self, cond):
        assert isinstance(cond, Node)
        self._cond = cond

    @property
    def body(self):
        return self._body

    @body.setter
    def body(self, body):
        assert type(body) is Stmt
        self._body = body
