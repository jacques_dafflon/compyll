#!/usr/bin/env python
"""
.. currentmodule:: compile
   :platform: Unix, OSX
   :synopsis: Compile Python into LLVM.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>
.. moduleauthor:: Giorgio Gori <gorig@usi.ch>
.. moduleauthor:: Giovanni Viviani <vivanig@usi.ch>

This module compile a subset of Python 2 code into LLVM.
"""
import logging
import sys
from compyll_parser import parse
from closure_converter import convert_closure
from lambda_lifter import lift_lambda
from optimize_local import optimize_local
from tagger import tag
from optimize_local import optimize_local
from closure_converter import convert_closure
from flattener import flatten
from code_generator import generate_code


__all__ = ["compile"]


def compile(code):
    """Compile Python 2 source code to LLVM.

    Transform a string representing valid Python code into LLVM and prints it.

    Args:
        code (str): String representing valid Python code.

    Returns:
        None
    """

    tree = parse(code)
    
    tree = convert_closure(tree)

    tree = lift_lambda(tree)

    tree = optimize_local(tree)

    tree = tag(tree)

    #tree = untag(tree)

    tree = flatten(tree)

    output = generate_code(tree)

    print(output)

    return None


def log_stuff(tree):
    if tree.funcs:
        for func in tree.funcs.nodes:
            logging.error(func)
    for stmt in tree.node.nodes:
        logging.error(stmt)


def main():
    logging.basicConfig(format='%(levelname)s[%(module)s->%(lineno)s]: %(message)s', level=logging.WARNING)
    if len(sys.argv) == 1:
        logging.error("Missing source file")
        exit(1)
    elif len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            compile(f.read())
        exit(0)
    elif len(sys.argv) > 2:
        logging.error("Too many arguments!")
        exit(1)

if __name__ == '__main__':
    main()
