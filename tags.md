## Compyll Tags System

- ```000```(0): integers
- ```001```(1): booleans
- ```010```(2): undeclared variable
- ```011```(3): pointer to function
- ```100```(4): pointer to object
- ```101```(5): pointer to string
- ```110```(6): pointer to list
- ```111```(7): _unused_
