# ComPyLL Report

## Design
Our compiler is made of two main components. The first one, written in Python is the core of the compiler. It reads a Python source code file and outputs an equivalent program in LLVM to the standard output. It uses PLY (the Python implementation of lex and yacc) to build an abstract syntax tree (AST) it then modifies the tree using a set of passes in order to output valid and equivalent LLM code.

The second part of our program is a set of functions written in C and compiled into a static library. These functions are used at runtime to perform operation which would be difficult to perform in LLVM. The library also include support for complex types such as strings, lists and objects.

## Supported Language
### Primitive types
Our compiler support integers and boolean as basic types All the usual operations
can be performed using those types (addition, multiplication, …) as well as compound assignments.

Note on boolean, similarly to Python, our compiler accepts assignment to ``True`` and ``False`` variables.

### Complex types
Our compiler supports the following complex types with limitations: strings, lists and objects.
(See the know bugs and limitations section below for the limitation of those types.)
#### Strings
For strings, both addition (concatenation) and multiplication by a basic type is supported.
Getting indices is supported
The following string methods are supported: 

* ``__add__(s)`` : concatenate ``s`` to the the string
* ``__getitem__(idx)``
* ``__len__()`` : length of the string
* ``__mul__(value)`` concatenate the string with itself ``value`` times.
* ``capitalize()``
* ``endswith(substring)`` : ``True`` if string ends with ``substring``. ``False`` otherwise.
* ``index(elem)``
* ``islower()`` : Returns ``True`` if the string does not contains upper case characters. 
* ``isupper()`` : Returns ``True`` if the string does not contains lower case characters.
* ``lower()`` : Returns the string with every upper case translated to lower case.
* ``upper()`` : Returns the string with every lower case translated to upper case.

#### Lists
For lists, both addition (concatenation) and multiplication by a basic type is supported.
Indices both for setting and getting are supported on lists.
Getting slices is supported.
The following list methods are supported.

* ``__add__(lst)``
* ``__getitem__(idx)``
* ``__len__()``
* ``__mul__(value)``
* ``__setitem__(idx, elem)``
* ``append(value)``
* ``count()``
* ``extend(lst)``
* ``index(elem)``
* ``insert(idx, elem)``
* ``pop(idx)``
* ``remove(elem)``
* ``reverse()``

#### Objects
We support simple objects definition as well as the ``__init__`` method.
Instance attributes are also supported.

#### Functions and lambdas
The compiler supports function definitions and calls, including definition of functions within functions.
Before function calls, the number of parameters given to the call is checked against the number of parameters expected by the function definition. If the number does not match, an error message is printed and the execution is terminated.

Moreover, the following built in functions are supported:

* ``len(seq)`` : Returns the length of a sequence where a sequence is a string or a list.
* ``str(val)`` : Returns the string representation of a value

Lambdas with and without parameters are supported.

### Statements
Discard, assign, multiple assign and print statements are supported.

### Control flow and comparisons
If/elif/else and while control flow are supported.
Comparisons are supported as well (``<``, ``<=``, ``==``, ``!=``, ``>=``, ``>``).

## Known bugs and limitations
When displaying syntax errors, the line of code containing the error is displayed with a marker indicating the column at which the error is. This marker is indicating the wrong position if the line contains an indentation.

### Primitive types
Methods are not supported on basic types.

### Complex types
* Comparison methods (``__lt__(other)``, …) are not supported.
* Objects do not support inheritance.
* Objects do not support magic methods. (Excepts for those mentioned for lists and strings )
* Lists do not support the ``sort()`` method
* Lists size is arbitrarily fixed regardless of the system or memory use.
* Not all methods are supported for strings. The list of supported methods can be found above.
* When calling a method with the wrong number of arguments, the error message indicates the correct number plus one. (It is including the object itself.)
* Decorators for objects are not supported.
* Old style classes are not supported.

#### Functions and lambdas
* Free variables in functions only have the value they have before the function's declaration.
* Recursive lambdas (such as ```x = lambda : x()```) are not supported.
* Free variables inside functions have the value they are assigned before the function definition and not the eventual values they get assigned between the function definition and the function call. For example the following code prints ``1`` and not ``2`` as it should in Python:

    ```
    a = 1
    def f():
        print a
    
    a = 2
    f()
    ```


## Implementation
Each pass is implemented in a class and each class is in a separate module (file).
Each class has a method whose name is descriptive of the pass. The method accepts the AST as single argument and traverses it using somewhat of a double dispatch.
For each node in the tree, a method having the same made is defined in the class. Using the name of the node, the corresponding function is being called.

Each pass modifies the tree and returns it for the next pass, besides the code generation pass which returns valid LLVM code as a string.

The different passes are explained below.

### Parsing
The parser just parses the code and generates the relative AST. To help us in the implementation we used yacc and lex. The nodes of the AST are implemented on the base of the compiler module of python, with some changes to better merge with the rest of our project. The list of tokens generated by lex is passed to yacc, to which we provide a list of rules and precedences that it uses to generate the AST.

### Closure converter
This pass is needed to convert all functions and lambdas. Each function and each lambda is split into a closure, containing a converted lambda and an environment. The closure is then assigned to a variable.

#### Free Variable Finder
This pass will do a top-down tree transversal of each function node. During the iteration it divides the variables into free variables and not free variables.

#### Free Variable Replacer
This pass will do a top-down tree transversal. Whenever it find a call to a free variable it replaces it with a call to the relative variable in the environment of the function.

### Lambda Lifting
This pass will do a bottom-up tree transversal. Whenever a Lambda is found a new top-level function is created with the parameters and body of the lambda, and it is identified with a new generated name. Instead of the lambda, a new Name with the generated function name is returned.

### Class Conversion
Classes are converted into functions. This conversion is done partially in the Closure Converter and partially in the Lambda Lifter. The conversion proceeds as follows.

First of all, in the __Closure Converter__ we call ```convert_closure``` on the body of the class, so that all methods are converted from ```ast.Function``` to ```ast.Assign(ast.Name(..), ast.MakeClosure(..))```. Then, the system will look for the ```__init__``` method, and it will create a default one it if it does not exist.

After that, in the __Lambda Lifter__, the system will convert the class into a top level function that will construct the object and it will return it. More specifically, the parameters are the same as the ones from the ```__init__``` (except for ```self```), and the body of the function is constructed as follows:

1. Assignment of a ```MakeObject``` call to ```self```. This will later call the c runtime and it will allocate the hashmap of the object.
2. The original class body, with the difference that simple assignments are converted into assignments to attributes of ```self```.
3. A call to ```__init__``` passing the parameters given to the function call.
4. ```Return self```. This will ensure that the return value of the function is the object itself.

Lastly, after the top-level function has been created, the original class declaration is replaced with an assignment of a function call of the created function to a variable, which name is the name of the class.

### Local Optimization
The local optimization pass executes constant folding and algebraic simplification. Originally it was also doing strength reduction, but that had to be removed with the introduction of list and strings, because those two types support multiplication, but not bit shifts, so multiplications cannot be converted into shifts.

### Tagging
Python is a dynamically typed language. To compile it to __LLVM__ we needed to find a way to represent the type of each variable. To achieve that we use Tagging.

The concept behind tagging is to dedicate some bits not used by the variable to store the type. Since we were all running 64bit machines, the three least significant bits of pointers are zeros, due to the fact that x64 is 8-aligned. We can then use those 3 bits to store 8 different types. In our implementation we stored seven different types, with one combination unused. Below we can see how the different types were encoded in three bits.

- ```000```(0): integers
- ```001```(1): booleans
- ```010```(2): undeclared variable
- ```011```(3): pointer to function
- ```100```(4): pointer to object
- ```101```(5): pointer to string
- ```110```(6): pointer to list
- ```111```(7): _unused_

The Integer type uses the default value of the tree bits since it is the type most used and the first one we implemented. The value two is not actually used for a type but to identify variable not yet defined.

### Flattening
Each method of the class implementing the flattening pass flattens a node of the tree.
All the methods return the two same arguments: a node containing the value of the flattened node (such as a register, a name or a constant); a list of nodes which contains the operations corresponding to the original node being flattened.
For example an add operation will return first a register which holds the result of the addition and second a list of operations which will flatten the two operators, add them and save them into a register.

### Code generation
The general contract for tree walk methods in the ```CodeGenerator``` class is that they take the AST node and return a tuple containing two strings. The first string zero, one or more llvm statements that have to be executed before, and the second string is the expression resulting from the node itself.

Before executing the code generator tree walk, two additional tree walks are done.

#### Variable Finder
The variable finder tree walk executed from the code generator will take a tree and it will traverse it bottom-up keeping track of the variables used and assigned. During this pass we detect if variables are always referenced before declaration (we stay on the safe side when traversing loops or if statements). Referenced built-in variables and functions such as ```True```, ```input``` or ```len``` are added to the set of variables.

At the end of the walk the __Variable Finder__ returns a list of the variables used. This list is used in the code generation to create the code to allocate the memory for each variable. Additionally, the default value of each variable is stored: for ```True``` and ```False``` we just store their tagged value, for built-in functions such as ```len``` and ```str``` we create the closure and store it, and for every other variable we store the value 2 that represents *undeclared variable*.

#### String Finder
The names finder will execute a tree walk looking for strings and keeping track of them in a set. Strings are not just string literals, but also object attributes and methods. 

At the end of the walk the __String Finder__ returns the set of strings to the code generator, which will generate lines such as ```'@str_74657374 = private unnamed_addr constant [5 x i8] c"test\00", align 1'``` at the top of the generated __LLVM__ file. Strings declared on top like this will be used later in the program for string literals, method invocations, and as keys for hashmap lookups and inserts.

## Closing Remarks
Building a compiler was a great experience and we learned a lot about the way they work.
Even if Python would not have been our first choice to build a project of this size, the outcome was successful and allowed us to progress not only in understanding how the language works but also in our Python programming skills.